
package app.controller;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.StudentYearDTO;
import app.model.StudentYear;
import app.service.StudentService;
import app.service.StudentYearService;


@Controller
@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RequestMapping(path = "/api/studentyear")
public class StudentYearController {
	@Autowired
	private StudentYearService service;
	@Autowired
	private StudentService studentService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<Page<StudentYearDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<StudentYearDTO>>(service.findAll(pageable).map(i -> new StudentYearDTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		StudentYear obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<StudentYearDTO>(new StudentYearDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> save(@RequestBody() StudentYear object) {
		Date date = new Date();
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		object.setDateofenrollment((java.sql.Date) sqlDate);
		object.getStudents().setActive(true);
		studentService.save(object.getStudents());
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> update(@RequestBody() StudentYear object, @PathVariable("id") Long id) {
		StudentYear obj = service.findOne(id);
		if (obj != null) {
			service.save(object);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> delete(@RequestBody() StudentYear object) {
		StudentYear obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}