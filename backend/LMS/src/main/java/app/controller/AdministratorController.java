
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.AdministratorDTO;
import app.model.Administrator;
import app.service.AdministratorService;

@Controller
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RequestMapping(path = "/api/administrator")
public class AdministratorController {
	@Autowired
	private AdministratorService service;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<AdministratorDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<AdministratorDTO>>(service.findAll(pageable).map(i -> new AdministratorDTO(i)),
				HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		Administrator obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<AdministratorDTO>(new AdministratorDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMIN"})
	public ResponseEntity<?> save(@RequestBody() Administrator object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMIN"})
	public ResponseEntity<?> update(@RequestBody() Administrator object, @PathVariable("id") Long id) {
		Administrator obj = service.findOne(id);
		if (obj != null) {
			obj.setEmail(object.getEmail());
			obj.setActive(object.getActive());
			service.save(object);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMIN"})
	public ResponseEntity<?> delete(@RequestBody() Administrator object) {
		Administrator obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}