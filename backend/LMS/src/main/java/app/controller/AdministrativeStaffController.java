
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.AdministrativeStaffDTO;
import app.model.AdministrativeStaff;
import app.service.AdministrativeStaffService;

@Controller
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RequestMapping(path = "/api/administrativestaff")
public class AdministrativeStaffController {
	@Autowired
	private AdministrativeStaffService service;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<AdministrativeStaffDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<AdministrativeStaffDTO>>(
				service.findAll(pageable).map(i -> new AdministrativeStaffDTO(i)), HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		AdministrativeStaff obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<AdministrativeStaffDTO>(new AdministrativeStaffDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "/get-by-email/{email}", method = RequestMethod.GET)
	public ResponseEntity<?> getByEmail(@PathVariable("email") String email) {
		AdministrativeStaff obj = service.findOne(email);
		if (obj != null) {
			return new ResponseEntity<AdministrativeStaffDTO>(new AdministrativeStaffDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<?> save(@RequestBody() AdministrativeStaff object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@RequestBody() AdministrativeStaff object, @PathVariable("id") Long id) {
		AdministrativeStaff obj = service.findOne(id);
		if (obj != null) {
			obj.setJmbg(object.getJmbg());
			obj.setLastname(object.getLastname());
			obj.setName(object.getName());
			service.save(obj);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestBody() AdministrativeStaff object) {
		AdministrativeStaff obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}