
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.TeacherDTO;
import app.model.Teacher;
import app.service.TeacherService;


@Controller
@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RequestMapping(path = "/api/teacher")
public class TeacherController {
	@Autowired
	private TeacherService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<Page<TeacherDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<TeacherDTO>>(service.findAll(pageable).map(i -> new TeacherDTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		Teacher obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<TeacherDTO>(new TeacherDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> save(@RequestBody() Teacher object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@RequestBody() Teacher object, @PathVariable("id") Long id) {
		Teacher obj = service.findOne(id);
		if (obj != null) {
			obj.setBiography(obj.getBiography());
			obj.setName(object.getName());
			obj.setLastname(object.getLastname());
			obj.setJmbg(object.getJmbg());
			service.save(obj);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> delete(@RequestBody() Teacher object) {
		Teacher obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}