
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.EntranceExamDTO;
import app.model.EntranceExam;
import app.model.Student;
import app.service.EntranceExamService;
import app.service.StudentService;



@Controller
@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RequestMapping(path = "/api/entranceexam")
public class EntranceExamController {
	@Autowired
	private EntranceExamService service;
	@Autowired StudentService studentService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_STUDENT"})
	public ResponseEntity<Page<EntranceExamDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<EntranceExamDTO>>(service.findAll(pageable).map(i -> new EntranceExamDTO(i)),
				HttpStatus.OK);
	}
	
	@RequestMapping(path = "/get-my-exam/{userId}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_STUDENT"})
	public ResponseEntity<Page<EntranceExamDTO>> getUserExams(Pageable pageable, @PathVariable("userId") Long userId) {
		Student student = studentService.findOne(userId);
		return new ResponseEntity<Page<EntranceExamDTO>>(service.getByStudent(student, pageable).map(i -> new EntranceExamDTO(i)),
				HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_STUDENT"})
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		EntranceExam obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<EntranceExamDTO>(new EntranceExamDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_STUDENT"})
	public ResponseEntity<?> save(@RequestBody() EntranceExam object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> update(@RequestBody() EntranceExam object) {
		EntranceExam obj = service.findOne(object.getId());
		if (obj != null) {
			service.save(object);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> delete(@RequestBody() EntranceExam object) {
		EntranceExam obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}