
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.MajorDTO;
import app.model.Major;
import app.service.MajorService;


@Controller
@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RequestMapping(path = "/api/major")
public class MajorController {
	@Autowired
	private MajorService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<MajorDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<MajorDTO>>(service.findAll(pageable).map(i -> new MajorDTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		Major obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<MajorDTO>(new MajorDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> save(@RequestBody() Major object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> update(@RequestBody() Major object) {
		Major obj = service.findOne(object.getId());
		if (obj != null) {
			service.save(object);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> delete(@RequestBody() Major object, @PathVariable("id") Long id) {
		Major obj = service.findOne(id);
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}