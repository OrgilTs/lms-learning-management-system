
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.UserNotificationDTO;
import app.model.UserNotification;
import app.service.UserNotificationService;


@Controller
@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RequestMapping(path = "/api/usernotification")
public class UserNotificationController {
	@Autowired
	private UserNotificationService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<Page<UserNotificationDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<UserNotificationDTO>>(service.findAll(pageable).map(i -> new UserNotificationDTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		UserNotification obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<UserNotificationDTO>(new UserNotificationDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> save(@RequestBody() UserNotification object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> update(@RequestBody() UserNotification object, @PathVariable("id") Long id) {
		UserNotification obj = service.findOne(object.getId());
		if (obj != null) {
			obj.setCreated(object.getCreated());
			obj.setMessage(object.getMessage());
			obj.setTitle(object.getTitle());
			service.save(obj);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> delete(@RequestBody() UserNotification object) {
		UserNotification obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}