
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.TeacherAtRealizationDTO;
import app.model.TeacherAtRealization;
import app.service.TeacherAtRealizationService;


@Controller
@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RequestMapping(path = "/api/teacheratrealization")
public class TeacherAtRealizationController {
	@Autowired
	private TeacherAtRealizationService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<Page<TeacherAtRealizationDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<TeacherAtRealizationDTO>>(service.findAll(pageable).map(i -> new TeacherAtRealizationDTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		TeacherAtRealization obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<TeacherAtRealizationDTO>(new TeacherAtRealizationDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> save(@RequestBody() TeacherAtRealization object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<?> update(@RequestBody() TeacherAtRealization object, @PathVariable("id") Long id) {
		TeacherAtRealization obj = service.findOne(id);
		if (obj != null) {
			service.save(object);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestBody() TeacherAtRealization object) {
		TeacherAtRealization obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}