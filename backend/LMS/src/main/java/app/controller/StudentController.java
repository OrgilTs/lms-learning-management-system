
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.StudentDTO;
import app.model.Student;
import app.model.User;
import app.service.StudentService;
import app.service.UserService;


@Controller
@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RequestMapping(path = "/api/student")
public class StudentController {
	@Autowired
	private StudentService service;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<Page<StudentDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<StudentDTO>>(service.findAll(pageable).map(i -> new StudentDTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	@Secured({"ROLE_STUDENT", "ROLE_TEACHER", "ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		Student obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<StudentDTO>(new StudentDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> save(@RequestBody() Student object) {
		User user = userService.findOne(object.getEmail());
		if(user==null) {
			object.setPassword(passwordEncoder.encode(object.getPassword()));
			service.save(object);
			User newUser = userService.findOne(object.getEmail());
			Student student = service.findOne(newUser.getId()); // izbegavam null error za dobavljanje povezanih stvari ..
			return new ResponseEntity<StudentDTO>(new StudentDTO(student),HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<Object>(HttpStatus.CONFLICT);
		}
	}
	

	@RequestMapping(path = "/registration", method = RequestMethod.POST)
	public ResponseEntity<?> registration(@RequestBody() Student object) {
		User user = userService.findOne(object.getEmail());
		if(user==null) {
			object.setActive(false);
			object.setPassword(passwordEncoder.encode(object.getPassword()));
			service.save(object);
			User newUser = userService.findOne(object.getEmail());
			Student student = service.findOne(newUser.getId()); // izbegavam null error za dobavljanje povezanih stvari ..
			return new ResponseEntity<StudentDTO>(new StudentDTO(student),HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<Object>(HttpStatus.CONFLICT);
		}
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> update(@RequestBody() Student object, @PathVariable("id") Long id) {
		Student obj = service.findOne(id);
		if (obj != null) {
			obj.setBirthdate(object.getBirthdate());
			obj.setName(object.getName());
			obj.setLastname(object.getLastname());
			obj.setJmbg(object.getJmbg());
			service.save(obj);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> delete(@RequestBody() Student object) {
		Student obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}