
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.AttendingSubjectDTO;
import app.model.AttendingSubject;
import app.service.AttendingSubjectService;


@Controller
@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RequestMapping(path = "/api/attendingsubject")
public class AttendingSubjectController {
	@Autowired
	private AttendingSubjectService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<AttendingSubjectDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<AttendingSubjectDTO>>(service.findAll(pageable).map(i -> new AttendingSubjectDTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		AttendingSubject obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<AttendingSubjectDTO>(new AttendingSubjectDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMIN", "ROLE_STUDENT"})
	public ResponseEntity<?> save(@RequestBody() AttendingSubject object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
	public ResponseEntity<?> update(@RequestBody() AttendingSubject object, @PathVariable("id") Long id) {
		AttendingSubject obj = service.findOne(id);
		if (obj != null) {
			service.save(object);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMIN"})
	public ResponseEntity<?> delete(@RequestBody() AttendingSubject object) {
		AttendingSubject obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}