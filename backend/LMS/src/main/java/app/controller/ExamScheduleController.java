
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.ExamScheduleDTO;
import app.model.ExamSchedule;
import app.service.ExamScheduleService;


@Controller
@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RequestMapping(path = "/api/examschedule")
public class ExamScheduleController {
	@Autowired
	private ExamScheduleService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<ExamScheduleDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<ExamScheduleDTO>>(service.findAll(pageable).map(i -> new ExamScheduleDTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		ExamSchedule obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<ExamScheduleDTO>(new ExamScheduleDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> save(@RequestBody() ExamSchedule object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "", method = RequestMethod.PUT)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> update(@RequestBody() ExamSchedule object) {
		ExamSchedule obj = service.findOne(object.getId());
		if (obj != null) {
			service.save(object);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	@Secured({"ROLE_ADMIN", "ROLE_STAFF"})
	public ResponseEntity<?> delete(@RequestBody() ExamSchedule object) {
		ExamSchedule obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}