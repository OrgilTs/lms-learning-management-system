
package app.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class ClassRealization  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @ManyToOne
    private Subject subject;
    
    @ManyToOne
    private TeacherAtRealization teacheratrealization;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "classrealization")
    private Set<AttendingSubject> attendingsubjects;
    

    public ClassRealization(Subject subject, TeacherAtRealization teacheratrealization, Set<AttendingSubject> attendingsubjects,  long id ) {
        
        this.id=id; 
        this.subject = subject;
        this.teacheratrealization = teacheratrealization;
        this.attendingsubjects = attendingsubjects;
    }

    public ClassRealization() {
    	super();
    }
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}
    
    public TeacherAtRealization getTeacheratrealization() {
		return this.teacheratrealization;
	}

	public void setTeacheratrealization(TeacherAtRealization teacheratrealization) {
		this.teacheratrealization = teacheratrealization;
	}
    
    public Set<AttendingSubject> getAttendingsubjects() {
		return this.attendingsubjects;
	}

	public void setAttendingsubjects(Set<AttendingSubject> attendingsubjects) {
		this.attendingsubjects = attendingsubjects;
	}
    
}