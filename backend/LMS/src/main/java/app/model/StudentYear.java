
package app.model;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class StudentYear {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String indexnumber;

	@Column(nullable = false)
	private Date dateofenrollment;

	@ManyToOne
	private Student student;

	@ManyToOne
	private StudyYear studyyear;

	public StudentYear(String indexnumber, Date dateofenrollment, Student student, long id, StudyYear studyYear) {

		this.id = id;
		this.indexnumber = indexnumber;
		this.dateofenrollment = dateofenrollment;
		this.student = student;
		this.studyyear = studyYear;
	}

	public StudentYear() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIndexnumber() {
		return this.indexnumber;
	}

	public StudyYear getStudyYear() {
		return studyyear;
	}

	public void setStudyYear(StudyYear studyYear) {
		this.studyyear = studyYear;
	}

	public void setIndexnumber(String indexnumber) {
		this.indexnumber = indexnumber;
	}

	public Date getDateofenrollment() {
		return this.dateofenrollment;
	}

	public void setDateofenrollment(Date dateofenrollment) {
		this.dateofenrollment = dateofenrollment;
	}

	public Student getStudents() {
		return this.student;
	}

	public void setStudents(Student student) {
		this.student = student;
	}

}