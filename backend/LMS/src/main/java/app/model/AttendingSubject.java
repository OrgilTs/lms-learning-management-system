
package app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AttendingSubject  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @Column(nullable = false)
    private int finalgrade;
    
    @ManyToOne
    private ClassRealization classrealization;
    
    @ManyToOne
    private Student student;
    

    public AttendingSubject(int finalgrade, ClassRealization classrealization, Student student,  long id ) {
        
        this.id=id; 
        this.finalgrade = finalgrade;
        this.classrealization = classrealization;
        this.student = student;
    }

    public AttendingSubject() {
    	super();
    }
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public int getFinalgrade() {
		return this.finalgrade;
	}

	public void setFinalgrade(int finalgrade) {
		this.finalgrade = finalgrade;
	}
    
    public ClassRealization getClassrealization() {
		return this.classrealization;
	}

	public void setClassrealization(ClassRealization classrealization) {
		this.classrealization = classrealization;
	}
    
    public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
    
}