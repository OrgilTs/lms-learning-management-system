
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Country  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @Column(nullable = false)
    private String name;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
    private Set<Place> places;
    

    public Country(String name, Set<Place> places,  long id ) {
        
        this.id=id; 
        this.name = name;
        this.places = places;
    }

    public Country() {
    	super();
    }
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public Set<Place> getPlaces() {
		return this.places;
	}

	public void setPlaces(Set<Place> places) {
		this.places = places;
	}
    
}