
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class classType  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @Column(nullable = false)
    private String name;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "classtype")
    private Set<TeacherAtRealization> teacherAtRealizations;
    

    public classType(String name, Set<TeacherAtRealization> teacherAtRealizations,  long id ) {
        
        this.id=id; 
        this.name = name;
        this.teacherAtRealizations = teacherAtRealizations;
    }

    public classType() {
    	super();
    }
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public Set<TeacherAtRealization> getNastavnicinarealizaciji() {
		return this.teacherAtRealizations;
	}

	public void setNastavnicinarealizaciji(Set<TeacherAtRealization> teacherAtRealizations) {
		this.teacherAtRealizations = teacherAtRealizations;
	}
    
}