
package app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Result  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @Column(nullable = false)
    private String description;
    
    @ManyToOne
    private Subject subject;
    

    public Result(String description, Subject subject,  long id ) {
        
        this.id=id; 
        this.description = description;
        this.subject = subject;
    }

    public Result() {
    	super();
    }
    
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
    
    public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}
    
}