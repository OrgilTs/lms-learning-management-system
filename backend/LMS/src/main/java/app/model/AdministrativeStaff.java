
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class AdministrativeStaff extends User  {
    
    @Column
    private String jmbg;
    
    @Column
    private String name;
    
    @Column
    private String lastname;
    
	@ManyToOne
	private Address address;
	
	@ManyToOne
	private University university;

    public AdministrativeStaff(String jmbg, String name, String lastname,  long id , String password, String email, boolean active, Address address, University university, Set<UserNotification> notifications) {
         super(password,email,active, id, notifications);
        this.jmbg = jmbg;
        this.name = name;
        this.lastname = lastname;
        this.address = address;
        this.university = university;
    }

    public AdministrativeStaff() {
    	super();
    }
    
    
    public String getJmbg() {
		return this.jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}
    
}