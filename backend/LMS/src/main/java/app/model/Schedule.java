
package app.model;

import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Schedule  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @Column(nullable = false)
    private LocalTime start;
    
    @Column(nullable = false)
    private LocalTime end;
    
    @ManyToOne
    private DayTitle daytitle;
    
    @ManyToOne
    private Subject subject;
    

    public Schedule(LocalTime start, LocalTime end, DayTitle daytitle, Subject subject,  long id ) {
        
        this.id=id; 
        this.start = start;
        this.end = end;
        this.daytitle = daytitle;
        this.subject = subject;
    }

    public Schedule() {
    	super();
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public LocalTime getStart() {
		return this.start;
	}

	public void setStart(LocalTime start) {
		this.start = start;
	}
    
    public LocalTime getEnd() {
		return this.end;
	}

	public void setEnd(LocalTime end) {
		this.end = end;
	}
    
    public DayTitle getDaytitle() {
		return this.daytitle;
	}

	public void setDaytitle(DayTitle daytitle) {
		this.daytitle = daytitle;
	}
    
    public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}
    
}