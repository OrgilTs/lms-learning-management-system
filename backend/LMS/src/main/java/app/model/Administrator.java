
package app.model;

import java.util.Set;

import javax.persistence.Entity;

@Entity
public class Administrator extends User {

	public Administrator(long id, String password, String email, boolean active, Set<UserNotification> notifications) {
		super(password, email, active, id, notifications);
	}

	public Administrator() {
		super();
	}
}