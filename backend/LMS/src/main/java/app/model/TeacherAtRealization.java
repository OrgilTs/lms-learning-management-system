
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class TeacherAtRealization  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @Column(nullable = false)
    private int numberofclasses;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacheratrealization")
    private Set<ClassRealization> classrealizations;
    
    @ManyToOne
    private classType classtype;
    

    public TeacherAtRealization(int numberofclasses, Set<ClassRealization> classrealizations, classType classtype,  long id ) {
        
        this.id=id; 
        this.numberofclasses = numberofclasses;
        this.classrealizations = classrealizations;
        this.classtype = classtype;
    }

    public TeacherAtRealization() {
    	super();
    }
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public int getNumberofclasses() {
		return this.numberofclasses;
	}

	public void setNumberofclasses(int numberofclasses) {
		this.numberofclasses = numberofclasses;
	}
    
    public Set<ClassRealization> getClassrealizations() {
		return this.classrealizations;
	}

	public void setClassrealizations(Set<ClassRealization> classrealizations) {
		this.classrealizations = classrealizations;
	}
    
    public classType getClasstype() {
		return this.classtype;
	}

	public void setClasstype(classType classtype) {
		this.classtype = classtype;
	}
    
}