
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ScientificField  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @Column(nullable = false)
    private String name;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "scientificfield")
    private Set<Title> titles;
    

    public ScientificField(String name, Set<Title> titles,  long id ) {
        
        this.id=id; 
        this.name = name;
        this.titles = titles;
    }
    
    public ScientificField() {
    	super();
    }
    

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public Set<Title> getTitles() {
		return this.titles;
	}

	public void setTitles(Set<Title> titles) {
		this.titles = titles;
	}
    
}