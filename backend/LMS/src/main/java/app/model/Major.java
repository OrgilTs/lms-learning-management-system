
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Major {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String name;

	@ManyToOne
	private Faculty faculty;

	@ManyToMany
	private Set<Teacher> teachers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "major")
	private Set<StudyYear> studyyears;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "major")
	private Set<EntranceExam> entranceexams;

	public Major(String name, Faculty faculty, Set<Teacher> teachers, Set<StudyYear> studyyears, long id,
			Set<EntranceExam> entranceexams) {
		this.id = id;
		this.name = name;
		this.faculty = faculty;
		this.teachers = teachers;
		this.studyyears = studyyears;
		this.entranceexams = entranceexams;
	}

	public Major() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Faculty getFaculty() {
		return this.faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public Set<Teacher> getTeachers() {
		return this.teachers;
	}

	public void setTeachers(Set<Teacher> teachers) {
		this.teachers = teachers;
	}

	public Set<StudyYear> getStudyyears() {
		return this.studyyears;
	}

	public void setStudyyears(Set<StudyYear> studyyears) {
		this.studyyears = studyyears;
	}

	public Set<EntranceExam> getEntranceexams() {
		return entranceexams;
	}

	public void setEntranceexams(Set<EntranceExam> entranceexams) {
		this.entranceexams = entranceexams;
	}

}