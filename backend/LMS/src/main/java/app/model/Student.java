
package app.model;

import java.sql.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Student extends User {
	@Column
	private String jmbg;

	@Column
	private String name;

	@Column
	private String lastname;

	@Column
	private Date birthdate;

	@ManyToOne
	private Address address;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "student")
	private Set<StudentYear> studentyears;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "student")
	private Set<AttendingSubject> attendingsubjects;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "student")
	private Set<EntranceExam> entranceexams;

	public Student(String jmbg, String name, String lastname, Date birthdate, Address address,
			Set<StudentYear> studentyears, Set<AttendingSubject> attendingsubjects, long id, String password,
			String email, boolean active, Set<EntranceExam> entranceexams, Set<UserNotification> notifications) {
		super(password, email, active, id, notifications);
		this.jmbg = jmbg;
		this.name = name;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.address = address;
		this.studentyears = studentyears;
		this.attendingsubjects = attendingsubjects;
		this.entranceexams = entranceexams;
	}

	public Student() {
		super();
	}

	public String getJmbg() {
		return this.jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthdate() {
		return this.birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<StudentYear> getStudentyear() {
		return this.studentyears;
	}

	public void setStudentyear(Set<StudentYear> studentyears) {
		this.studentyears = studentyears;
	}

	public Set<AttendingSubject> getAttendingsubjects() {
		return this.attendingsubjects;
	}

	public void setAttendingsubjects(Set<AttendingSubject> attendingsubjects) {
		this.attendingsubjects = attendingsubjects;
	}

	public Set<StudentYear> getStudentyears() {
		return studentyears;
	}

	public void setStudentyears(Set<StudentYear> studentyears) {
		this.studentyears = studentyears;
	}

	public Set<EntranceExam> getEntranceexams() {
		return entranceexams;
	}

	public void setEntranceexams(Set<EntranceExam> entranceexams) {
		this.entranceexams = entranceexams;
	}

}