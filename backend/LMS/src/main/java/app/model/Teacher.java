
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Teacher extends User {
	@Column
	private String jmbg;

	@Column
	private String name;

	@Column
	private String lastname;

	@Column
	private String biography;

	@ManyToOne
	private Address address;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher")
	private Set<Faculty> faculties;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rector")
	private Set<University> universitets;

	@ManyToMany
	private Set<Major> majors;

	public Teacher(String jmbg, String name, String lastname, String biography, Set<Faculty> faculties,
			Set<Major> majors, long id, String password, String email, boolean active, Address address, Set<UserNotification> notifications) {
		super(password, email, active, id, notifications);
		this.jmbg = jmbg;
		this.name = name;
		this.lastname = lastname;
		this.biography = biography;
		this.faculties = faculties;
		this.majors = majors;
		this.address = address;
	}

	public Teacher() {
		super();
	}

	public String getJmbg() {
		return this.jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return this.lastname;
	}

	public Set<University> getUniversitets() {
		return universitets;
	}

	public void setUniversitets(Set<University> universitets) {
		this.universitets = universitets;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getBiography() {
		return this.biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public Set<Faculty> getFaculty() {
		return this.faculties;
	}

	public void setFaculty(Set<Faculty> faculties) {
		this.faculties = faculties;
	}

	public Set<Major> getMajor() {
		return this.majors;
	}

	public void setMajor(Set<Major> majors) {
		this.majors = majors;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	
}