
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String street;

	@Column(nullable = false)
	private String number;

	@ManyToOne
	private Place place;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
	private Set<University> universities;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
	private Set<Faculty> faculties;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
	private Set<Teacher> teachers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
	private Set<Student> students;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
	private Set<AdministrativeStaff> administrativestaffs;

	public Address(String street, String number, Place place, Set<University> universities, Set<Faculty> faculties,
			long id, Set<Teacher> teachers, Set<AdministrativeStaff> administrativestaffs) {

		this.id = id;
		this.street = street;
		this.number = number;
		this.place = place;
		this.universities = universities;
		this.faculties = faculties;
		this.administrativestaffs = administrativestaffs;
	}

	public Address() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Place getPlace() {
		return this.place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public Set<University> getUniversities() {
		return this.universities;
	}

	public void setUniversities(Set<University> universities) {
		this.universities = universities;
	}

	public Set<Faculty> getFaculties() {
		return this.faculties;
	}

	public void setFaculties(Set<Faculty> faculties) {
		this.faculties = faculties;
	}

	public Set<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(Set<Teacher> teachers) {
		this.teachers = teachers;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<AdministrativeStaff> getAdministrativestaffs() {
		return administrativestaffs;
	}

	public void setAdministrativestaffs(Set<AdministrativeStaff> administrativestaffs) {
		this.administrativestaffs = administrativestaffs;
	}

}