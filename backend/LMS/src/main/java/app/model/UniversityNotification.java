
package app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class UniversityNotification  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @ManyToOne
    private University university;
    
    @Column(nullable = false)
    private LocalDateTime created;
    
    @Column(nullable = false)
    private String title;
    
    @Column(nullable = false)
    private String message;
    

    public UniversityNotification(University university, LocalDateTime created, String title, String message,  long id ) {
        
        this.id=id; 
        this.university = university;
        this.created = created;
        this.title = title;
        this.message = message;
    }

    public UniversityNotification() {
    	super();
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public University getUniversity() {
		return this.university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}
    
    public LocalDateTime getCreated() {
		return this.created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
    
    public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
    
    public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
    
}