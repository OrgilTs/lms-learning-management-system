
package app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class EntranceExam {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private LocalDateTime requireddatetime;

	@Column(nullable = false)
	private boolean accepted;

	@ManyToOne
	private Student student;
	
	@ManyToOne
	private Major major;

	public EntranceExam(Student student, LocalDateTime requireddatetime, boolean accepted, long id, Major major) {

		this.id = id;
		this.student = student;
		this.requireddatetime = requireddatetime;
		this.accepted = accepted;
		this.major = major;
	}

	public EntranceExam() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public LocalDateTime getRequireddatetime() {
		return this.requireddatetime;
	}

	public void setRequireddatetime(LocalDateTime requireddatetime) {
		this.requireddatetime = requireddatetime;
	}

	public boolean getAccepted() {
		return this.accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public Major getMajor() {
		return major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	
}