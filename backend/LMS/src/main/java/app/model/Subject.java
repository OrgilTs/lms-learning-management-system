
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Subject {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private int espb;

	@Column(nullable = false)
	private boolean required;

	@Column(nullable = false)
	private int numberoflectures;

	@Column(nullable = false)
	private int numberofpractices;

	@Column(nullable = false)
	private int otherformsofclass;

	@Column(nullable = false)
	private int researchwork;

	@Column(nullable = false)
	private int remainingclasses;

	@ManyToOne
	private StudyYear studyyear;

	@ManyToOne
	private Subject precondition;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subject")
	private Set<ClassRealization> classrealizations;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subject")
	private Set<Result> results;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subject")
	private Set<Schedule> schedules;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subject")
	private Set<ExamSchedule> examschedules;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "precondition")
	private Set<Subject> preconditions;

	public Subject(String name, int espb, boolean required, int numberoflectures, int numberofpractices,
			int otherformsofclass, int researchwork, int remainingclasses, StudyYear studyyear,
			Set<ClassRealization> classrealizations, Set<Result> results, long id, Subject precondition,
			Set<Schedule> schedules, Set<ExamSchedule> examschedules) {

		this.id = id;
		this.name = name;
		this.espb = espb;
		this.required = required;
		this.numberoflectures = numberoflectures;
		this.numberofpractices = numberofpractices;
		this.otherformsofclass = otherformsofclass;
		this.researchwork = researchwork;
		this.remainingclasses = remainingclasses;
		this.studyyear = studyyear;
		this.classrealizations = classrealizations;
		this.results = results;
		this.precondition = precondition;
		this.schedules = schedules;
		this.examschedules = examschedules;
	}

	public Subject() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEspb() {
		return this.espb;
	}

	public void setEspb(int espb) {
		this.espb = espb;
	}

	public boolean getRequired() {
		return this.required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public int getNumberoflectures() {
		return this.numberoflectures;
	}

	public void setNumberoflectures(int numberoflectures) {
		this.numberoflectures = numberoflectures;
	}

	public int getNumberofpractices() {
		return this.numberofpractices;
	}

	public void setNumberofpractices(int numberofpractices) {
		this.numberofpractices = numberofpractices;
	}

	public int getOtherformsofclass() {
		return this.otherformsofclass;
	}

	public void setOtherformsofclass(int otherformsofclass) {
		this.otherformsofclass = otherformsofclass;
	}

	public int getResearchwork() {
		return this.researchwork;
	}

	public void setResearchwork(int researchwork) {
		this.researchwork = researchwork;
	}

	public int getRemainingclasses() {
		return this.remainingclasses;
	}

	public void setRemainingclasses(int remainingclasses) {
		this.remainingclasses = remainingclasses;
	}

	public StudyYear getStudyyear() {
		return this.studyyear;
	}

	public void setStudyyear(StudyYear studyyear) {
		this.studyyear = studyyear;
	}

	public Set<ClassRealization> getClassrealizations() {
		return this.classrealizations;
	}

	public void setClassrealizations(Set<ClassRealization> classrealizations) {
		this.classrealizations = classrealizations;
	}

	public Set<Result> getResults() {
		return this.results;
	}

	public void setResults(Set<Result> results) {
		this.results = results;
	}

	public Subject getPrecondition() {
		return precondition;
	}

	public void setPrecondition(Subject precondition) {
		this.precondition = precondition;
	}

	public Set<Schedule> getSchedules() {
		return schedules;
	}

	public void setSchedules(Set<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Set<ExamSchedule> getExamschedules() {
		return examschedules;
	}

	public void setExamschedules(Set<ExamSchedule> examschedules) {
		this.examschedules = examschedules;
	}

	public Set<Subject> getPreconditions() {
		return preconditions;
	}

	public void setPreconditions(Set<Subject> preconditions) {
		this.preconditions = preconditions;
	}

}