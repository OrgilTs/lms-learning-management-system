
package app.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class StudyYear {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private int year;

	@ManyToOne
	private Major major;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studyyear")
	private Set<Subject> subjects;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studyyear")
	private Set<StudentYear> studentyear;

	public StudyYear(int year, Major major, Set<Subject> subjects, long id, Set<StudentYear> studentyear) {

		this.id = id;
		this.year = year;
		this.major = major;
		this.subjects = subjects;
		this.studentyear = studentyear;
	}

	public StudyYear() {
		super();
	}

	public long getId() {
		return id;
	}

	public Set<StudentYear> getStudentyear() {
		return studentyear;
	}

	public void setStudentyear(Set<StudentYear> studentyear) {
		this.studentyear = studentyear;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Major getMajor() {
		return this.major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	public Set<Subject> getSubjects() {
		return this.subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

}