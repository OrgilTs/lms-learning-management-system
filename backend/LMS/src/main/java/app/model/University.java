
package app.model;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class University {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private LocalDateTime dateofestablishment;

	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	private String phone;

	@ManyToOne
	private Address address;

	@ManyToOne
	private Teacher rector;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "university")
	private Set<Faculty> faculties;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "university")
	private Set<AdministrativeStaff> administrativestaffs;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "university")
	private Set<UniversityNotification> notifications;

	public University(String name, LocalDateTime dateofestablishment, Address address, Set<Faculty> faculties, long id,
			Set<AdministrativeStaff> administrativestaffs, String email, String phone,
			Set<UniversityNotification> notifications) {

		this.id = id;
		this.name = name;
		this.dateofestablishment = dateofestablishment;
		this.address = address;
		this.faculties = faculties;
		this.administrativestaffs = administrativestaffs;
		this.email = email;
		this.phone = phone;
	}

	public University() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDateofestablishment() {
		return this.dateofestablishment;
	}

	public void setDateofestablishment(LocalDateTime dateofestablishment) {
		this.dateofestablishment = dateofestablishment;
	}

	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<Faculty> getFaculties() {
		return this.faculties;
	}

	public void setFaculties(Set<Faculty> faculties) {
		this.faculties = faculties;
	}

	public Set<AdministrativeStaff> getAdministrativestaffs() {
		return administrativestaffs;
	}

	public void setAdministrativestaffs(Set<AdministrativeStaff> administrativestaffs) {
		this.administrativestaffs = administrativestaffs;
	}

	public Teacher getRector() {
		return rector;
	}

	public void setRector(Teacher rector) {
		this.rector = rector;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Set<UniversityNotification> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<UniversityNotification> notifications) {
		this.notifications = notifications;
	}

}