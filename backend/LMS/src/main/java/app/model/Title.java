
package app.model;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Title  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @Column(nullable = false)
    private Date datewhenchosed;
    
    @Column(nullable = false)
    private Date datewhenstopped;
    
    @ManyToOne
    private ScientificField scientificfield;
    
    @ManyToOne
    private TitleType titletype;
    

    public Title(Date datewhenchosed, Date datewhenstopped, ScientificField scientificfield, TitleType titletype,  long id ) {
        
        this.id=id; 
        this.datewhenchosed = datewhenchosed;
        this.datewhenstopped = datewhenstopped;
        this.scientificfield = scientificfield;
        this.titletype = titletype;
    }

    public Title() {
    	super();
    }
    
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public Date getDatewhenchosed() {
		return this.datewhenchosed;
	}

	public void setDatewhenchosed(Date datewhenchosed) {
		this.datewhenchosed = datewhenchosed;
	}
    
    public Date getDatewhenstopped() {
		return this.datewhenstopped;
	}

	public void setDatewhenstopped(Date datewhenstopped) {
		this.datewhenstopped = datewhenstopped;
	}
    
    public ScientificField getScientificfield() {
		return this.scientificfield;
	}

	public void setScientificfield(ScientificField scientificfield) {
		this.scientificfield = scientificfield;
	}
    
    public TitleType getTitletype() {
		return this.titletype;
	}

	public void setTitletype(TitleType titletype) {
		this.titletype = titletype;
	}
    
}