
package app.DTO;

import java.util.Set;

import app.DTO.UserDTO;
import app.model.Administrator;
import app.model.UserNotification;

public class AdministratorDTO extends UserDTO {

	public AdministratorDTO(long id, String password, String email, boolean active, Set<UserNotification> notifications) {
		super(password, email, active, id, notifications);
	}

	public AdministratorDTO(Administrator model) {
		super(null, model.getEmail(), model.getActive(), model.getId(), model.getNotifications());
	}

	public AdministratorDTO() {

	}

}