
package app.DTO;


 
import app.model.Result;
import app.model.Subject;

public class ResultDTO  {
	private long id;
    private String description;
    private SubjectDTO subject;
    

    public ResultDTO(String description, SubjectDTO subject,  long id ) {
        
        this.id=id; 
        this.description = description;
        this.subject = subject;
    }

    public ResultDTO(Result model){ 
        this.id = model.getId();  
        this.description = model.getDescription();  
        Subject su = model.getSubject();
        this.subject = new SubjectDTO(su.getName(), su.getEspb(), su.getRequired(), su.getNumberoflectures(), su.getNumberofpractices(), su.getOtherformsofclass(), su.getResearchwork(), su.getRemainingclasses(), su.getId(), su.getPrecondition()); 
    }
    
  public ResultDTO(String description, long id ) {
        this.id=id; 
        this.description = description;
    }

    public ResultDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
    
    public SubjectDTO getSubject() {
		return this.subject;
	}

	public void setSubject(SubjectDTO subject) {
		this.subject = subject;
	}
    
}