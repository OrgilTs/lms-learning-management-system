
package app.DTO;


 
import java.util.HashSet;
import java.util.Set;

import app.model.Address;
import app.model.Place;

public class PlaceDTO  {
	private long id;
    private String name;
    private CountryDTO country;
    private Set<AddressDTO> adresses = new HashSet<AddressDTO>();
    

    public PlaceDTO(String name, CountryDTO country, Set<AddressDTO> adresses,  long id ) {
        
        this.id=id; 
        this.name = name;
        this.country = country;
        this.adresses = adresses;
    }

    public PlaceDTO(Place model){ 
        this.id = model.getId();  
        this.name = model.getName();  
        this.country = new CountryDTO(model.getCountry().getName(), model.getCountry().getId());
        for(Address ad : model.getAdresses()) {
        	this.adresses.add(new AddressDTO(ad.getStreet(), ad.getNumber(), ad.getId()));
        }
    }
    
    
  public PlaceDTO(String name, long id ) {
        this.id=id; 
        this.name = name;
    }

    public PlaceDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public CountryDTO getCountry() {
		return this.country;
	}

	public void setCountry(CountryDTO country) {
		this.country = country;
	}
    
    public Set<AddressDTO> getAdresses() {
		return this.adresses;
	}

	public void setAdresses(Set<AddressDTO> adresses) {
		this.adresses = adresses;
	}
    
}