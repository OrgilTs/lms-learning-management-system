
package app.DTO;

import java.util.HashSet;
import java.util.Set;

import app.model.ClassRealization;
import app.model.ExamSchedule;
import app.model.Result;
import app.model.Schedule;
import app.model.Subject;

public class SubjectDTO {
	private long id;
	private String name;
	private int espb;
	private boolean required;
	private int numberoflectures;
	private int numberofpractices;
	private int otherformsofclass;
	private int researchwork;
	private int remainingclasses;
	private StudyYearDTO studyyear;
	private SubjectDTO precondition;
	private Set<ClassRealizationDTO> classrealizations = new HashSet<ClassRealizationDTO>();
	private Set<ResultDTO> results = new HashSet<ResultDTO>();
	private Set<ScheduleDTO> schedules = new HashSet<ScheduleDTO>();
	private Set<ExamScheduleDTO> examschedules = new HashSet<ExamScheduleDTO>();

	public SubjectDTO(String name, int espb, boolean required, int numberoflectures, int numberofpractices,
			int otherformsofclass, int researchwork, int remainingclasses, StudyYearDTO studyyear,
			Set<ClassRealizationDTO> classrealizations, Set<ResultDTO> results, long id, SubjectDTO precondition,
			Set<ScheduleDTO> schedules, Set<ExamScheduleDTO> examschedules) {

		this.id = id;
		this.name = name;
		this.espb = espb;
		this.required = required;
		this.numberoflectures = numberoflectures;
		this.numberofpractices = numberofpractices;
		this.otherformsofclass = otherformsofclass;
		this.researchwork = researchwork;
		this.remainingclasses = remainingclasses;
		this.studyyear = studyyear;
		this.classrealizations = classrealizations;
		this.results = results;
		this.precondition = precondition;
		this.schedules = schedules;
		this.examschedules = examschedules;
	}

	public SubjectDTO getPrecondition() {
		return precondition;
	}

	public void setPrecondition(SubjectDTO precondition) {
		this.precondition = precondition;
	}

	public SubjectDTO(Subject model) {
		this.id = model.getId();
		this.name = model.getName();
		this.espb = model.getEspb();
		this.required = model.getRequired();
		this.numberoflectures = model.getNumberoflectures();
		this.numberofpractices = model.getNumberofpractices();
		this.otherformsofclass = model.getOtherformsofclass();
		this.researchwork = model.getResearchwork();
		this.remainingclasses = model.getRemainingclasses();
		this.studyyear = new StudyYearDTO(model.getStudyyear().getYear(), model.getStudyyear().getId());

		if (model.getClassrealizations() != null) {
			for (ClassRealization cr : model.getClassrealizations()) {
				this.classrealizations.add(new ClassRealizationDTO(cr.getId()));
			}
		}
		
		if (model.getResults() != null) {
			for (Result re : model.getResults()) {
				this.results.add(new ResultDTO(re.getDescription(), re.getId()));
			}
		}

		if (model.getSchedules() != null) {
			for (Schedule sc : model.getSchedules()) {
				this.schedules.add(new ScheduleDTO(sc));
			}
		}

		if (model.getExamschedules() != null) {
			for (ExamSchedule sc : model.getExamschedules()) {
				this.examschedules.add(new ExamScheduleDTO(sc.getId(), sc.getCreated()));
			}
		}
	}

	public SubjectDTO(String name, int espb, boolean required, int numberoflectures, int numberofpractices,
			int otherformsofclass, int researchwork, int remainingclasses, long id, Subject precondition) {
		this.id = id;
		this.name = name;
		this.espb = espb;
		this.required = required;
		this.numberoflectures = numberoflectures;
		this.numberofpractices = numberofpractices;
		this.otherformsofclass = otherformsofclass;
		this.researchwork = researchwork;
		this.remainingclasses = remainingclasses;
		if (precondition != null) {
			this.precondition = new SubjectDTO(precondition.getName(), precondition.getId());
		}

	}

	public SubjectDTO(String name, long id) {
		this.id = id;
		this.name = name;
	}

	public SubjectDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEspb() {
		return this.espb;
	}

	public void setEspb(int espb) {
		this.espb = espb;
	}

	public boolean getRequired() {
		return this.required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public int getNumberoflectures() {
		return this.numberoflectures;
	}

	public void setNumberoflectures(int numberoflectures) {
		this.numberoflectures = numberoflectures;
	}

	public int getNumberofpractices() {
		return this.numberofpractices;
	}

	public void setNumberofpractices(int numberofpractices) {
		this.numberofpractices = numberofpractices;
	}

	public int getOtherformsofclass() {
		return this.otherformsofclass;
	}

	public void setOtherformsofclass(int otherformsofclass) {
		this.otherformsofclass = otherformsofclass;
	}

	public int getResearchwork() {
		return this.researchwork;
	}

	public void setResearchwork(int researchwork) {
		this.researchwork = researchwork;
	}

	public int getRemainingclasses() {
		return this.remainingclasses;
	}

	public void setRemainingclasses(int remainingclasses) {
		this.remainingclasses = remainingclasses;
	}

	public StudyYearDTO getStudyyear() {
		return this.studyyear;
	}

	public void setStudyyear(StudyYearDTO studyyear) {
		this.studyyear = studyyear;
	}

	public Set<ClassRealizationDTO> getClassrealizations() {
		return this.classrealizations;
	}

	public void setClassrealizations(Set<ClassRealizationDTO> classrealizations) {
		this.classrealizations = classrealizations;
	}

	public Set<ResultDTO> getResults() {
		return this.results;
	}

	public void setResults(Set<ResultDTO> results) {
		this.results = results;
	}

	public Set<ScheduleDTO> getSchedules() {
		return schedules;
	}

	public void setSchedules(Set<ScheduleDTO> schedules) {
		this.schedules = schedules;
	}

	public Set<ExamScheduleDTO> getExamschedules() {
		return examschedules;
	}

	public void setExamschedules(Set<ExamScheduleDTO> examschedules) {
		this.examschedules = examschedules;
	}

}