
package app.DTO;

import java.time.LocalDateTime;

import app.model.UniversityNotification;

public class UniversityNotificationDTO {
	private long id;
	private UniversityDTO university;
	private LocalDateTime created;
	private String title;
	private String message;

	public UniversityNotificationDTO(UniversityDTO university, LocalDateTime created, String title, String message,
			long id) {
		this.id = id;
		this.university = university;
		this.created = created;
		this.title = title;
		this.message = message;
	}

	public UniversityNotificationDTO(UniversityNotification model) {
		this.id = model.getId();
		if(model.getUniversity() != null) {
			this.university = new UniversityDTO(model.getUniversity().getName(),
					model.getUniversity().getDateofestablishment(), model.getUniversity().getId(),
					model.getUniversity().getEmail(), model.getUniversity().getPhone());
		}
		this.created = model.getCreated();
		this.title = model.getTitle();
		this.message = model.getMessage();
	}

	public UniversityNotificationDTO(long id, LocalDateTime created, String title, String message) {
		this.id = id;
		this.created = created;
		this.title = title;
		this.message = message;
	}
	
	public UniversityNotificationDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UniversityDTO getUniversity() {
		return this.university;
	}

	public void setUniversity(UniversityDTO university) {
		this.university = university;
	}

	public LocalDateTime getCreated() {
		return this.created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}