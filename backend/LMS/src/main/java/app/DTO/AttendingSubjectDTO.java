
package app.DTO;


import app.model.AttendingSubject;
import app.model.Student;

public class AttendingSubjectDTO  {
	private long id;
    private int finalgrade;
    private ClassRealizationDTO classrealization;
    private StudentDTO student;
    

    public AttendingSubjectDTO(int finalgrade, ClassRealizationDTO classrealization, StudentDTO student,  long id ) {
        
        this.id=id; 
        this.finalgrade = finalgrade;
        this.classrealization = classrealization;
        this.student = student;
    }

    public AttendingSubjectDTO(AttendingSubject model){ 
        this.id = model.getId();  
        this.finalgrade = model.getFinalgrade();  
        this.classrealization = new ClassRealizationDTO(model.getClassrealization().getId());  
        Student st = model.getStudent();
        this.student = new StudentDTO(st.getJmbg(), st.getName(), st.getLastname(), st.getBirthdate(), st.getId(), st.getEmail(), st.getActive()); 
    }

   public AttendingSubjectDTO(int finalgrade, long id ) {
        this.id=id; 
        this.finalgrade = finalgrade;
    }
   
    public AttendingSubjectDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public int getFinalgrade() {
		return this.finalgrade;
	}

	public void setFinalgrade(int finalgrade) {
		this.finalgrade = finalgrade;
	}
    
    public ClassRealizationDTO getClassrealization() {
		return this.classrealization;
	}

	public void setClassrealization(ClassRealizationDTO classrealization) {
		this.classrealization = classrealization;
	}
    
    public StudentDTO getStudent() {
		return this.student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}
    
}