
package app.DTO;

import java.util.HashSet;
import java.util.Set;

import app.model.StudentYear;
import app.model.StudyYear;
import app.model.Subject;

public class StudyYearDTO {
	private long id;
	private int year;
	private MajorDTO major;
	private Set<SubjectDTO> subjects = new HashSet<SubjectDTO>();
	private Set<StudentYearDTO> studentyear = new HashSet<StudentYearDTO>();

	public StudyYearDTO(int year, MajorDTO major, Set<SubjectDTO> subjects, long id) {

		this.id = id;
		this.year = year;
		this.major = major;
		this.subjects = subjects;
	}

	public StudyYearDTO(StudyYear model) {
		this.id = model.getId();
		this.year = model.getYear();
		this.major = new MajorDTO(model.getMajor().getName(), model.getMajor().getId());

		for (Subject su : model.getSubjects()) {
			this.subjects.add(new SubjectDTO(su));
		}

		for (StudentYear sy : model.getStudentyear()) {
			this.studentyear.add(new StudentYearDTO(sy.getIndexnumber(), sy.getDateofenrollment(), sy.getId()));
		}
	}

	public StudyYearDTO(int year, long id) {
		this.id = id;
		this.year = year;
	}

	public StudyYearDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public MajorDTO getMajor() {
		return this.major;
	}

	public void setMajor(MajorDTO major) {
		this.major = major;
	}

	public Set<SubjectDTO> getSubjects() {
		return this.subjects;
	}

	public void setSubjects(Set<SubjectDTO> subjects) {
		this.subjects = subjects;
	}

}