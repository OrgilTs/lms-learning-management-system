
package app.DTO;

import java.time.LocalDateTime;

import app.model.EntranceExam;

public class EntranceExamDTO {
	private long id;
	private StudentDTO student;
	private LocalDateTime requireddatetime;
	private boolean accepted;

	public EntranceExamDTO(StudentDTO student, LocalDateTime requireddatetime, boolean accepted, long id) {

		this.id = id;
		this.student = student;
		this.requireddatetime = requireddatetime;
		this.accepted = accepted;
	}

	public EntranceExamDTO(EntranceExam model) {
		this.id = model.getId();
		this.student = new StudentDTO(model.getStudent().getJmbg(), model.getStudent().getName(),
				model.getStudent().getLastname(), model.getStudent().getBirthdate(), model.getStudent().getId(),
				model.getStudent().getEmail(), model.getStudent().getActive());
		this.requireddatetime = model.getRequireddatetime();
		this.accepted = model.getAccepted();
	}

	public EntranceExamDTO() {

	}

	public EntranceExamDTO(long id, LocalDateTime requireddatetime, boolean accepted) {
		this.id = id;
		this.requireddatetime = requireddatetime;
		this.accepted = accepted;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public StudentDTO getStudent() {
		return this.student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public LocalDateTime getRequireddatetime() {
		return this.requireddatetime;
	}

	public void setRequireddatetime(LocalDateTime requireddatetime) {
		this.requireddatetime = requireddatetime;
	}

	public boolean getAccepted() {
		return this.accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

}