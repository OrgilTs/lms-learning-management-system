
package app.DTO;


 
import java.sql.Date;
import app.model.Title;

public class TitleDTO  {
	private long id;
    private Date datewhenchosed;
    private Date datewhenstopped;
    private ScientificFieldDTO scientificfield;
    private TitleTypeDTO titletype;
    

    public TitleDTO(Date datewhenchosed, Date datewhenstopped, ScientificFieldDTO scientificfield, TitleTypeDTO titletype,  long id ) {
        
        this.id=id; 
        this.datewhenchosed = datewhenchosed;
        this.datewhenstopped = datewhenstopped;
        this.scientificfield = scientificfield;
        this.titletype = titletype;
    }

    public TitleDTO(Title model){ 
        this.id = model.getId();  
        this.datewhenchosed = model.getDatewhenchosed();  
        this.datewhenstopped = model.getDatewhenstopped();  
        this.scientificfield = new ScientificFieldDTO(model.getScientificfield());  
        this.titletype = new TitleTypeDTO(model.getTitletype().getName(), model.getTitletype().getId()); 
    }


    public TitleDTO(Date datewhenchosed, Date datewhenstopped,  long id ) {
        this.id=id; 
        this.datewhenchosed = datewhenchosed;
        this.datewhenstopped = datewhenstopped;
    }
    
    public TitleDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public Date getDatewhenchosed() {
		return this.datewhenchosed;
	}

	public void setDatewhenchosed(Date datewhenchosed) {
		this.datewhenchosed = datewhenchosed;
	}
    
    public Date getDatewhenstopped() {
		return this.datewhenstopped;
	}

	public void setDatewhenstopped(Date datewhenstopped) {
		this.datewhenstopped = datewhenstopped;
	}
    
    public ScientificFieldDTO getScientificfield() {
		return this.scientificfield;
	}

	public void setScientificfield(ScientificFieldDTO scientificfield) {
		this.scientificfield = scientificfield;
	}
    
    public TitleTypeDTO getTitletype() {
		return this.titletype;
	}

	public void setTitletype(TitleTypeDTO titletype) {
		this.titletype = titletype;
	}
    
}