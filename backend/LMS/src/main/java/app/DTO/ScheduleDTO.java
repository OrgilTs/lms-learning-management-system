
package app.DTO;

import java.time.LocalTime;

import app.model.Schedule;

public class ScheduleDTO {
	private long id;
	private LocalTime start;
	private LocalTime end;
	private DayTitleDTO daytitle;
	private SubjectDTO subject;

	public ScheduleDTO(LocalTime start, LocalTime end, DayTitleDTO daytitle, SubjectDTO subject, long id) {

		this.id = id;
		this.start = start;
		this.end = end;
		this.daytitle = daytitle;
		this.subject = subject;
	}

	public ScheduleDTO(Schedule model) {
		this.id = model.getId();
		this.start = model.getStart();
		this.end = model.getEnd();
		this.daytitle = new DayTitleDTO(model.getDaytitle().getId(), model.getDaytitle().getName());
		this.subject = new SubjectDTO(model.getSubject().getName(), model.getSubject().getId());
	}

	public ScheduleDTO(long id, LocalTime start, LocalTime end) {
		this.id = id;
		this.start = start;
		this.end = end;
	}

	public ScheduleDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalTime getStart() {
		return this.start;
	}

	public void setStart(LocalTime start) {
		this.start = start;
	}

	public LocalTime getEnd() {
		return this.end;
	}

	public void setEnd(LocalTime end) {
		this.end = end;
	}

	public DayTitleDTO getDaytitle() {
		return this.daytitle;
	}

	public void setDaytitle(DayTitleDTO daytitle) {
		this.daytitle = daytitle;
	}

	public SubjectDTO getSubject() {
		return this.subject;
	}

	public void setSubject(SubjectDTO subject) {
		this.subject = subject;
	}

}