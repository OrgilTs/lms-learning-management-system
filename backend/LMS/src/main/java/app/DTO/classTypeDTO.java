
package app.DTO;


 
import java.util.HashSet;
import java.util.Set;

import app.model.TeacherAtRealization;
import app.model.classType;

public class classTypeDTO  {
	private long id;
    private String name;
    private Set<TeacherAtRealizationDTO> teacherAtRealizations = new HashSet<TeacherAtRealizationDTO>();
    

    public classTypeDTO(String name, Set<TeacherAtRealizationDTO> teacherAtRealizations,  long id ) {
        
        this.id=id; 
        this.name = name;
        this.teacherAtRealizations = teacherAtRealizations;
    }

    public classTypeDTO(classType model){ 
        this.id = model.getId();  
        this.name = model.getName();  
        for(TeacherAtRealization tr : model.getNastavnicinarealizaciji()) {
        	this.teacherAtRealizations.add(new TeacherAtRealizationDTO(tr.getNumberofclasses(), tr.getId()));
        }
    }
    
    public classTypeDTO(String name, long id ) {
        this.id=id; 
        this.name = name;
    }

    public classTypeDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public Set<TeacherAtRealizationDTO> getNastavnicinarealizaciji() {
		return this.teacherAtRealizations;
	}

	public void setNastavnicinarealizaciji(Set<TeacherAtRealizationDTO> teacherAtRealizations) {
		this.teacherAtRealizations = teacherAtRealizations;
	}
    
}