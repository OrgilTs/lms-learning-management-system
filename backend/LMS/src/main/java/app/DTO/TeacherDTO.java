
package app.DTO;

import java.util.HashSet;
import java.util.Set;

import app.DTO.UserDTO;
import app.model.Faculty;
import app.model.Major;
import app.model.Teacher;
import app.model.University;
import app.model.UserNotification;

public class TeacherDTO extends UserDTO {
	private String jmbg;
	private String name;
	private String lastname;
	private String biography;
	private AddressDTO address;
	private Set<FacultyDTO> faculties = new HashSet<FacultyDTO>();
	private Set<MajorDTO> majors = new HashSet<MajorDTO>();
	private Set<UniversityDTO> universitets = new HashSet<UniversityDTO>();

	public TeacherDTO(String jmbg, String name, String lastname, String biography, Set<FacultyDTO> faculties,
			Set<MajorDTO> majors, Set<UniversityDTO> universitets, long id, String password, String email,
			boolean active, AddressDTO address,  Set<UserNotification> notifications) {
		super(password, email, active, id, notifications);
		this.jmbg = jmbg;
		this.name = name;
		this.lastname = lastname;
		this.biography = biography;
		this.faculties = faculties;
		this.majors = majors;
		this.universitets = universitets;
		this.address = address;
	}

	public TeacherDTO(Teacher model) {
		super(null, model.getEmail(), model.getActive(), model.getId(), model.getNotifications());
		this.jmbg = model.getJmbg();
		this.name = model.getName();
		this.lastname = model.getLastname();
		this.biography = model.getBiography();

		this.address = new AddressDTO(model.getAddress().getStreet(), model.getAddress().getNumber(),
				model.getAddress().getId());

		for (Faculty fa : model.getFaculty()) {
			this.faculties.add(new FacultyDTO(fa.getName(), fa.getId(), fa.getEmail(), fa.getPhone()));
		}

		for (Major ma : model.getMajor()) {
			this.majors.add(new MajorDTO(ma.getName(), ma.getId()));
		}

		for (University un : model.getUniversitets()) {
			this.universitets.add(new UniversityDTO(un.getName(), un.getDateofestablishment(), un.getId(), un.getEmail(), un.getPhone()));
		}
	}

	public TeacherDTO(String jmbg, String name, String lastname, String biography, long id, String email,
			boolean active) {
		super(null, email, active, id, null);
		this.jmbg = jmbg;
		this.name = name;
		this.lastname = lastname;
		this.biography = biography;
	}

	public TeacherDTO() {

	}

	public String getJmbg() {
		return this.jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getBiography() {
		return this.biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public Set<FacultyDTO> getFaculty() {
		return this.faculties;
	}

	public void setFaculty(Set<FacultyDTO> faculties) {
		this.faculties = faculties;
	}

	public Set<MajorDTO> getMajor() {
		return this.majors;
	}

	public void setMajor(Set<MajorDTO> majors) {
		this.majors = majors;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

}