
package app.DTO;


 
import java.util.HashSet;
import java.util.Set;

import app.model.Country;
import app.model.Place;

public class CountryDTO  {
	private long id;
    private String name;
    private Set<PlaceDTO> places = new HashSet<PlaceDTO>();
    

    public CountryDTO(String name, Set<PlaceDTO> places,  long id ) {
        
        this.id=id; 
        this.name = name;
        this.places = places;
    }

    public CountryDTO(Country model){ 
        this.id = model.getId();  
        this.name = model.getName();  
        for(Place pl : model.getPlaces()) {
        	this.places.add(new PlaceDTO(pl.getName(), pl.getId()));
        }
    }

    public CountryDTO() {

    }
    
    public CountryDTO(String name,  long id) {
    	this.id = id;
    	this.name = name;
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public Set<PlaceDTO> getPlaces() {
		return this.places;
	}

	public void setPlaces(Set<PlaceDTO> places) {
		this.places = places;
	}
    
}