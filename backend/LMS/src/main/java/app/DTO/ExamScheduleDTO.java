
package app.DTO;

import java.time.LocalDateTime;

import app.model.ExamSchedule;

public class ExamScheduleDTO {
	private long id;
	private LocalDateTime created;
	private SubjectDTO subject;

	public ExamScheduleDTO(LocalDateTime created, SubjectDTO subject, long id) {

		this.id = id;
		this.created = created;
		this.subject = subject;
	}

	public ExamScheduleDTO(ExamSchedule model) {
		this.id = model.getId();
		this.created = model.getCreated();
		this.subject = new SubjectDTO(model.getSubject().getName(), model.getSubject().getId());
	}

	public ExamScheduleDTO(long id, LocalDateTime created) {
		this.id = id;
		this.created = created;
	}

	public ExamScheduleDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getCreated() {
		return this.created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public SubjectDTO getSubject() {
		return this.subject;
	}

	public void setSubject(SubjectDTO subject) {
		this.subject = subject;
	}

}