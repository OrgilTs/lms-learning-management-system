
package app.DTO;

import java.util.HashSet;
import java.util.Set;

import app.model.Title;
import app.model.TitleType;

public class TitleTypeDTO {
	private long id;
	private String name;
	private Set<TitleDTO> titles = new HashSet<TitleDTO>();;

	public TitleTypeDTO(String name, Set<TitleDTO> titles, long id) {

		this.id = id;
		this.name = name;
		this.titles = titles;
	}

	public TitleTypeDTO(TitleType model) {
		this.id = model.getId();
		this.name = model.getName();
		for (Title ti : model.getTitles()) {
			this.titles.add(new TitleDTO(ti.getDatewhenchosed(), ti.getDatewhenstopped(), ti.getId()));
		}
	}

	public TitleTypeDTO(String name, long id) {
		this.id = id;
		this.name = name;
	}

	public TitleTypeDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<TitleDTO> getTitles() {
		return this.titles;
	}

	public void setTitles(Set<TitleDTO> titles) {
		this.titles = titles;
	}

}