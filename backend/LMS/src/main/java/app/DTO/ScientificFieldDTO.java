
package app.DTO;

import java.util.HashSet;
import java.util.Set;

import app.model.ScientificField;
import app.model.Title;

public class ScientificFieldDTO {
	private long id;
	private String name;
	private Set<TitleDTO> titles = new HashSet<TitleDTO>();

	public ScientificFieldDTO(String name, Set<TitleDTO> titles, long id) {

		this.id = id;
		this.name = name;
		this.titles = titles;
	}

	public ScientificFieldDTO(ScientificField model) {
		this.id = model.getId();
		this.name = model.getName();
		for (Title ti : model.getTitles()) {
			this.titles.add(new TitleDTO(ti.getDatewhenchosed(), ti.getDatewhenstopped(), ti.getId()));
		}
	}

	public ScientificFieldDTO(String name, long id) {
		this.id = id;
		this.name = name;
	}

	public ScientificFieldDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<TitleDTO> getTitles() {
		return this.titles;
	}

	public void setTitles(Set<TitleDTO> titles) {
		this.titles = titles;
	}

}