
package app.DTO;

import java.util.HashSet;
import java.util.Set;

import app.model.User;
import app.model.UserNotification;

public class UserDTO {
	private long id;
	private String password;
	private String email;
	private boolean active;
	private Set<UserNotificationDTO> notifications = new HashSet<UserNotificationDTO>();

	public UserDTO(String password, String email, boolean active, long id, Set<UserNotification> notifications) {
		this.id = id;
		this.password = password;
		this.email = email;
		this.active = active;
		if (notifications != null) {
			for (UserNotification un : notifications) {
				this.notifications
						.add(new UserNotificationDTO(un.getCreated(), un.getTitle(), un.getMessage(), un.getId()));
			}
		}
	}

	public UserDTO(User model) {
		this.id = model.getId();
		this.password = model.getPassword();
		this.email = model.getEmail();
		this.active = model.getActive();
		for (UserNotification un : model.getNotifications()) {
			this.notifications
					.add(new UserNotificationDTO(un.getCreated(), un.getTitle(), un.getMessage(), un.getId()));
		}
	}

	public UserDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Set<UserNotificationDTO> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<UserNotificationDTO> notifications) {
		this.notifications = notifications;
	}
}