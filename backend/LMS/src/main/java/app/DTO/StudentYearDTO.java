
package app.DTO;


 
import java.sql.Date;
import app.model.StudentYear;

public class StudentYearDTO  {
	private long id;
    private String indexnumber;
    private Date dateofenrollment;
    private StudentDTO student;
    private StudyYearDTO studyyear;
    

    public StudentYearDTO(String indexnumber, Date dateofenrollment, StudentDTO student,  long id,  StudyYearDTO studyyear) {
        
        this.id=id; 
        this.indexnumber = indexnumber;
        this.dateofenrollment = dateofenrollment;
        this.student = student;
        this.studyyear=studyyear;
    }

    public StudentYearDTO(StudentYear model){ 
        this.id = model.getId();  
        this.indexnumber = model.getIndexnumber();  
        this.dateofenrollment = model.getDateofenrollment(); 
        this.student = new StudentDTO(model.getStudents().getJmbg(), model.getStudents().getName(), model.getStudents().getLastname(), model.getStudents().getBirthdate(), model.getStudents().getId(), model.getStudents().getEmail(), model.getStudents().getActive());
        this.studyyear = new StudyYearDTO(model.getStudyYear().getYear(), model.getStudyYear().getId());
    }
    
    public StudentYearDTO(String indexnumber, Date dateofenrollment,  long id ) {
        this.id=id; 
        this.indexnumber = indexnumber;
        this.dateofenrollment = dateofenrollment;
    }

    public StudentYearDTO() {

    }

    public StudyYearDTO getStudyyear() {
		return studyyear;
	}

	public void setStudyyear(StudyYearDTO studyyear) {
		this.studyyear = studyyear;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getIndexnumber() {
		return this.indexnumber;
	}

	public void setIndexnumber(String indexnumber) {
		this.indexnumber = indexnumber;
	}
    
    public Date getDateofenrollment() {
		return this.dateofenrollment;
	}

	public void setDateofenrollment(Date dateofenrollment) {
		this.dateofenrollment = dateofenrollment;
	}
    
    public StudentDTO getStudent() {
		return this.student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}
    
}