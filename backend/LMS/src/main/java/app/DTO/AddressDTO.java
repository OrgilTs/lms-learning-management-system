
package app.DTO;

import java.util.HashSet;
import java.util.Set;

import app.model.Address;
import app.model.AdministrativeStaff;
import app.model.Faculty;
import app.model.Student;
import app.model.Teacher;
import app.model.University;

public class AddressDTO {
	private long id;
	private String street;
	private String number;
	private PlaceDTO place;
	private Set<UniversityDTO> universities = new HashSet<UniversityDTO>();
	private Set<FacultyDTO> faculties = new HashSet<FacultyDTO>();
	private Set<TeacherDTO> teachers = new HashSet<TeacherDTO>();
	private Set<StudentDTO> students = new HashSet<StudentDTO>();
	private Set<AdministrativeStaffDTO> administrativestaffs = new HashSet<AdministrativeStaffDTO>();

	public AddressDTO(String street, String number, PlaceDTO place, Set<UniversityDTO> universities,
			Set<FacultyDTO> faculties, long id, Set<TeacherDTO> teachers, Set<StudentDTO> students,
			Set<AdministrativeStaffDTO> administrativestaffs) {
		this.id = id;
		this.street = street;
		this.number = number;
		this.place = place;
		this.universities = universities;
		this.faculties = faculties;
		this.teachers = teachers;
		this.students = students;
		this.administrativestaffs = administrativestaffs;
	}

	public AddressDTO(Address model) {
		this.id = model.getId();
		this.street = model.getStreet();
		this.number = model.getNumber();
		this.place = new PlaceDTO(model.getPlace().getName(), model.getPlace().getId());

		for (University un : model.getUniversities()) {
			this.universities.add(new UniversityDTO(un.getName(), un.getDateofestablishment(), un.getId(),
					un.getEmail(), un.getPhone()));
		}

		for (Faculty fa : model.getFaculties()) {
			this.faculties.add(new FacultyDTO(fa.getName(), fa.getId(), fa.getEmail(), fa.getPhone()));
		}

		if (model.getTeachers() != null) {
			for (Teacher ti : model.getTeachers()) {
				if (ti.getClass().getName() == "Teacher") {
					this.teachers.add(new TeacherDTO(ti.getJmbg(), ti.getName(), ti.getLastname(), ti.getBiography(),
							ti.getId(), ti.getEmail(), ti.getActive()));
				}

			}
		}
		if (model.getStudents() != null) {
			for (Student st : model.getStudents()) {
				if (st.getClass().getName() == "Student") {
					this.students.add(new StudentDTO(st.getJmbg(), st.getName(), st.getLastname(), st.getBirthdate(),
							st.getId(), st.getEmail(), st.getActive()));
				}
			}
		}
		if (model.getAdministrativestaffs() != null) {
			for (AdministrativeStaff as : model.getAdministrativestaffs()) {
				if (as.getClass().getName() == "AdministrativeStaff") {
					this.administrativestaffs.add(new AdministrativeStaffDTO(as.getJmbg(), as.getName(),
							as.getLastname(), as.getId(), as.getEmail(), as.getActive(), as.getNotifications()));
				}
			}
		}
	}

	public AddressDTO(String street, String number, long id) {
		this.id = id;
		this.street = street;
		this.number = number;
	}

	public AddressDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public PlaceDTO getPlace() {
		return this.place;
	}

	public void setPlace(PlaceDTO place) {
		this.place = place;
	}

	public Set<UniversityDTO> getUniversities() {
		return this.universities;
	}

	public void setUniversities(Set<UniversityDTO> universities) {
		this.universities = universities;
	}

	public Set<FacultyDTO> getFaculties() {
		return this.faculties;
	}

	public void setFaculties(Set<FacultyDTO> faculties) {
		this.faculties = faculties;
	}

	public Set<TeacherDTO> getTeachers() {
		return teachers;
	}

	public void setTeachers(Set<TeacherDTO> teachers) {
		this.teachers = teachers;
	}

	public Set<StudentDTO> getStudents() {
		return students;
	}

	public void setStudents(Set<StudentDTO> students) {
		this.students = students;
	}

	public Set<AdministrativeStaffDTO> getAdministrativestaffs() {
		return administrativestaffs;
	}

	public void setAdministrativestaffs(Set<AdministrativeStaffDTO> administrativestaffs) {
		this.administrativestaffs = administrativestaffs;
	}

}