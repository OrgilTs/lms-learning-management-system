
package app.DTO;

import java.util.HashSet;
import java.util.Set;

import app.model.DayTitle;
import app.model.Schedule;

public class DayTitleDTO {
	private long id;
	private String name;
	private Set<ScheduleDTO> schedules = new HashSet<ScheduleDTO>();

	public DayTitleDTO(String name, Set<ScheduleDTO> schedules, long id) {

		this.id = id;
		this.name = name;
		this.schedules = schedules;
	}

	public DayTitleDTO(DayTitle model) {
		this.id = model.getId();
		this.name = model.getName();
		for (Schedule sc : model.getSchedules()) {
			this.schedules.add(new ScheduleDTO(sc.getId(), sc.getStart(), sc.getEnd()));
		}
	}

	public DayTitleDTO(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public DayTitleDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ScheduleDTO> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(Set<ScheduleDTO> schedules) {
		this.schedules = schedules;
	}

}