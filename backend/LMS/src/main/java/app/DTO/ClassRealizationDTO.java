
package app.DTO;


 
import java.util.HashSet;
import java.util.Set;

import app.model.AttendingSubject;
import app.model.ClassRealization;

public class ClassRealizationDTO  {
	private long id;
    private SubjectDTO subject;
    private TeacherAtRealizationDTO teacheratrealization;
    private Set<AttendingSubjectDTO> attendingsubjects = new HashSet<AttendingSubjectDTO>();
    

    public ClassRealizationDTO(SubjectDTO subject, TeacherAtRealizationDTO teacheratrealization, Set<AttendingSubjectDTO> attendingsubjects,  long id ) {
        
        this.id=id; 
        this.subject = subject;
        this.teacheratrealization = teacheratrealization;
        this.attendingsubjects = attendingsubjects;
    }

    public ClassRealizationDTO(ClassRealization model){ 
        this.id = model.getId();  
        this.subject = new SubjectDTO(model.getSubject());
        this.teacheratrealization = new  TeacherAtRealizationDTO(model.getTeacheratrealization().getNumberofclasses(), model.getTeacheratrealization().getId());  
        for(AttendingSubject as : model.getAttendingsubjects()) {
        	this.attendingsubjects.add(new AttendingSubjectDTO(as.getFinalgrade(), as.getId()));
        }
    }
    
    public ClassRealizationDTO(long id ) {
        this.id=id; 
    }

    public ClassRealizationDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public SubjectDTO getSubject() {
		return this.subject;
	}

	public void setSubject(SubjectDTO subject) {
		this.subject = subject;
	}
    
    public TeacherAtRealizationDTO getTeacheratrealization() {
		return this.teacheratrealization;
	}

	public void setTeacheratrealization(TeacherAtRealizationDTO teacheratrealization) {
		this.teacheratrealization = teacheratrealization;
	}
    
    public Set<AttendingSubjectDTO> getAttendingsubjects() {
		return this.attendingsubjects;
	}

	public void setAttendingsubjects(Set<AttendingSubjectDTO> attendingsubjects) {
		this.attendingsubjects = attendingsubjects;
	}
    
}