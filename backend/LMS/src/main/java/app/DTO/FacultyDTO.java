
package app.DTO;

import java.util.HashSet;
import java.util.Set;

import app.model.Faculty;
import app.model.Major;
import app.model.Teacher;

public class FacultyDTO {
	private long id;
	private String name;
	private String email;
	private String phone;
	private UniversityDTO university;
	private AddressDTO address;
	private TeacherDTO deandata; // teacher 
	private Set<MajorDTO> majors = new HashSet<MajorDTO>();

	public FacultyDTO(String name, UniversityDTO university, AddressDTO address, TeacherDTO teacher,
			Set<MajorDTO> majors, long id, String email, String phone) {

		this.id = id;
		this.name = name;
		this.university = university;
		this.address = address;
		this.deandata = teacher;
		this.majors = majors;
		this.email = email;
		this.phone = phone;
	}

	public FacultyDTO(Faculty model) {
		this.id = model.getId();
		this.name = model.getName();
		this.university = new UniversityDTO(model.getUniversity().getName(),
				model.getUniversity().getDateofestablishment(), model.getUniversity().getId(),
				model.getUniversity().getEmail(), model.getUniversity().getPhone());
		this.address = new AddressDTO(model.getAddress().getStreet(), model.getAddress().getNumber(),
				model.getAddress().getId());

		Teacher ti = model.getTeacher();
		this.deandata = new TeacherDTO(ti.getJmbg(), ti.getName(), ti.getLastname(), ti.getBiography(), ti.getId(),
				ti.getEmail(), ti.getActive());

		for (Major ma : model.getMajors()) {
			this.majors.add(new MajorDTO(ma.getName(), ma.getId()));
		}
	}

	public FacultyDTO(String name, long id, String email, String phone) {
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.email = email;
	}

	public FacultyDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UniversityDTO getUniversity() {
		return this.university;
	}

	public void setUniversity(UniversityDTO university) {
		this.university = university;
	}

	public AddressDTO getAddress() {
		return this.address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public TeacherDTO getDeandata() {
		return this.deandata;
	}

	public void setDeandata(TeacherDTO deandata) {
		this.deandata = deandata;
	}

	public Set<MajorDTO> getMajors() {
		return this.majors;
	}

	public void setMajors(Set<MajorDTO> majors) {
		this.majors = majors;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}