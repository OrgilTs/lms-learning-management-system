
package app.DTO;

import java.time.LocalDateTime;
import app.model.UserNotification;

public class UserNotificationDTO {
	private long id;
	private UserDTO user;
	private LocalDateTime created;
	private String title;
	private String message;

	public UserNotificationDTO(UserDTO user, LocalDateTime created, String title, String message, long id) {
		this.id = id;
		this.user = user;
		this.created = created;
		this.title = title;
		this.message = message;
	}

	public UserNotificationDTO(LocalDateTime created, String title, String message, long id) {
		this.id = id;
		this.created = created;
		this.title = title;
		this.message = message;
	}

	public UserNotificationDTO(UserNotification model) {
		this.id = model.getId();
		this.user = new UserDTO(null, model.getUser().getEmail(), model.getUser().getActive(), model.getUser().getId(), null);
		this.created = model.getCreated();
		this.title = model.getTitle();
		this.message = model.getMessage();
	}

	public UserNotificationDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserDTO getUser() {
		return this.user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public LocalDateTime getCreated() {
		return this.created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}