
package app.DTO;

import java.util.Set;

import app.DTO.UserDTO;
import app.model.AdministrativeStaff;
import app.model.UserNotification;

public class AdministrativeStaffDTO extends UserDTO {
	private String jmbg;
	private String name;
	private String lastname;
	private AddressDTO address;
	private UniversityDTO university;

	public AdministrativeStaffDTO(String jmbg, String name, String lastname, long id, String password, String email,
			boolean active, AddressDTO address, UniversityDTO university, Set<UserNotification> notifications) {
		super(password, email, active, id, notifications);
		this.jmbg = jmbg;
		this.name = name;
		this.lastname = lastname;
		this.address = address;
		this.university = university;
	}

	public AdministrativeStaffDTO(AdministrativeStaff model) {
		super(null, model.getEmail(), model.getActive(), model.getId(), model.getNotifications());
		this.jmbg = model.getJmbg();
		this.name = model.getName();
		this.lastname = model.getLastname();
		if (model.getAddress() != null) {
			this.address = new AddressDTO(model.getAddress().getStreet(), model.getAddress().getNumber(),
					model.getAddress().getId());
		}
		
		if (model.getUniversity() != null) {
			this.university = new UniversityDTO(model.getUniversity().getName(),
					model.getUniversity().getDateofestablishment(), model.getUniversity().getId(),
					model.getUniversity().getEmail(), model.getUniversity().getPhone());
		}
	}

	public AdministrativeStaffDTO(String jmbg, String name, String lastname, long id, String email, boolean active,
			Set<UserNotification> notifications) {
		super(null, email, active, id, notifications);
		this.jmbg = jmbg;
		this.name = name;
		this.lastname = lastname;
	}

	public AdministrativeStaffDTO() {

	}

	public String getJmbg() {
		return this.jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public UniversityDTO getUniversity() {
		return university;
	}

	public void setUniversity(UniversityDTO university) {
		this.university = university;
	}

}