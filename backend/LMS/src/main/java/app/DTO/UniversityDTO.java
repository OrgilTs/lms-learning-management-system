
package app.DTO;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import app.model.AdministrativeStaff;
import app.model.Faculty;
import app.model.University;
import app.model.UniversityNotification;

public class UniversityDTO {
	private long id;
	private String name;
	private LocalDateTime dateofestablishment;
	private AddressDTO address;
	private TeacherDTO rector;
	private Set<FacultyDTO> faculties = new HashSet<FacultyDTO>();
	private Set<AdministrativeStaffDTO> administrativestaffs = new HashSet<AdministrativeStaffDTO>();
	private String email;
	private String phone;
	private Set<UniversityNotificationDTO> notifications = new HashSet<UniversityNotificationDTO>();

	public TeacherDTO getRector() {
		return rector;
	}

	public void setRector(TeacherDTO rector) {
		this.rector = rector;
	}

	public UniversityDTO(String name, LocalDateTime dateofestablishment, AddressDTO address, Set<FacultyDTO> faculties,
			long id, TeacherDTO rector, Set<AdministrativeStaffDTO> administrativestaffs, String email, String phone,
			Set<UniversityNotificationDTO> notifications) {
		this.id = id;
		this.name = name;
		this.dateofestablishment = dateofestablishment;
		this.address = address;
		this.faculties = faculties;
		this.rector = rector;
		this.administrativestaffs = administrativestaffs;
		this.email = email;
		this.phone = phone;
		this.notifications = notifications;
	}

	public UniversityDTO(University model) {
		this.id = model.getId();
		this.name = model.getName();
		this.email = model.getEmail();
		this.phone = model.getPhone();
		this.dateofestablishment = model.getDateofestablishment();
		this.address = new AddressDTO(model.getAddress().getStreet(), model.getAddress().getNumber(),
				model.getAddress().getId());

		for (Faculty fa : model.getFaculties()) {
			this.faculties.add(new FacultyDTO(fa.getName(), fa.getId(), fa.getEmail(), fa.getPhone()));
		}

		this.rector = new TeacherDTO(model.getRector().getJmbg(), model.getRector().getName(),
				model.getRector().getLastname(), model.getRector().getBiography(), model.getRector().getId(),
				model.getRector().getEmail(), model.getRector().getActive());

		for (AdministrativeStaff as : model.getAdministrativestaffs()) {
			this.administrativestaffs.add(new AdministrativeStaffDTO(as.getJmbg(), as.getName(), as.getLastname(),
					as.getId(), as.getEmail(), as.getActive(), as.getNotifications()));
		}

		for (UniversityNotification no : model.getNotifications()) {
			this.notifications
					.add(new UniversityNotificationDTO(no.getId(), no.getCreated(), no.getTitle(), no.getMessage()));
		}
	}

	public UniversityDTO(String name, LocalDateTime dateofestablishment, long id, String email, String phone) {
		this.id = id;
		this.name = name;
		this.dateofestablishment = dateofestablishment;
		this.email = email;
		this.phone = phone;
	}

	public UniversityDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDateofestablishment() {
		return this.dateofestablishment;
	}

	public void setDateofestablishment(LocalDateTime dateofestablishment) {
		this.dateofestablishment = dateofestablishment;
	}

	public AddressDTO getAddress() {
		return this.address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public Set<FacultyDTO> getFaculties() {
		return this.faculties;
	}

	public void setFaculties(Set<FacultyDTO> faculties) {
		this.faculties = faculties;
	}

	public Set<AdministrativeStaffDTO> getAdministrativestaffs() {
		return administrativestaffs;
	}

	public void setAdministrativestaffs(Set<AdministrativeStaffDTO> administrativestaffs) {
		this.administrativestaffs = administrativestaffs;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Set<UniversityNotificationDTO> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<UniversityNotificationDTO> notifications) {
		this.notifications = notifications;
	}

}