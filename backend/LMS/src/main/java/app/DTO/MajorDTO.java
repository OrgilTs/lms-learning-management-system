
package app.DTO;


 
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import app.model.EntranceExam;
import app.model.Major;
import app.model.StudyYear;
import app.model.Teacher;

public class MajorDTO  {
	private long id;
    private String name;
    private FacultyDTO faculty;
    private Set<TeacherDTO> teachers = new HashSet<TeacherDTO>();
    private ArrayList<StudyYearDTO> studyyears =  new ArrayList<StudyYearDTO>();
    private Set<EntranceExam> entranceexams;
    
    public MajorDTO(String name, FacultyDTO faculty, Set<TeacherDTO> teachers, ArrayList<StudyYearDTO> studyyears,  long id ) {
        this.id=id; 
        this.name = name;
        this.faculty = faculty;
        this.teachers = teachers;
        this.studyyears = studyyears;
    }


    public MajorDTO(Major model){ 
        this.id = model.getId();  
        this.name = model.getName();  
        this.faculty = new  FacultyDTO(model.getFaculty().getName(), model.getFaculty().getId(), model.getFaculty().getEmail(), model.getFaculty().getPhone());  
        for(Teacher ti : model.getTeachers()) {
        	this.teachers.add(new TeacherDTO(ti.getJmbg(), ti.getName(), ti.getLastname(), ti.getBiography(), ti.getId(), ti.getEmail(), ti.getActive()));
        }
        for(StudyYear sy : model.getStudyyears()) {
        	this.studyyears.add(new StudyYearDTO(sy.getYear(), sy.getId()));
        }

        this.studyyears.sort(Comparator.comparing(StudyYearDTO::getId));
    }

    public MajorDTO(String name, long id ) {
        this.id=id; 
        this.name = name;
    }
  
    public MajorDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public FacultyDTO getFaculty() {
		return this.faculty;
	}

	public void setFaculty(FacultyDTO faculty) {
		this.faculty = faculty;
	}
    
    public Set<TeacherDTO> getTeachers() {
		return this.teachers;
	}

	public void setTeachers(Set<TeacherDTO> teachers) {
		this.teachers = teachers;
	}
    
    public ArrayList<StudyYearDTO> getStudyyears() {
		return this.studyyears;
	}

	public void setStudyyears(ArrayList<StudyYearDTO> studyyears) {
		this.studyyears = studyyears;
	}
    
}