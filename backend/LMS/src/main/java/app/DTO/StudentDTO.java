
package app.DTO;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import app.DTO.UserDTO;
import app.model.AttendingSubject;
import app.model.EntranceExam;
import app.model.Student;
import app.model.StudentYear;
import app.model.UserNotification;

public class StudentDTO extends UserDTO {
	private String jmbg;
	private String name;
	private String lastname;
	private Date birthdate;
	private AddressDTO address;
	private Set<StudentYearDTO> studentyears = new HashSet<StudentYearDTO>();
	private Set<AttendingSubjectDTO> attendingsubjects = new HashSet<AttendingSubjectDTO>();
	private Set<EntranceExamDTO> entranceexams = new HashSet<EntranceExamDTO>();

	public StudentDTO(String jmbg, String name, String lastname, Date birthdate, AddressDTO address,
			Set<StudentYearDTO> studentyears, Set<AttendingSubjectDTO> attendingsubjects, long id, String password,
			String email, boolean active, Set<EntranceExamDTO> entranceexams, Set<UserNotification> notifications) {
		super(password, email, active, id, notifications);
		this.jmbg = jmbg;
		this.name = name;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.address = address;
		this.studentyears = studentyears;
		this.attendingsubjects = attendingsubjects;
		this.entranceexams = entranceexams;
	}

	public StudentDTO(Student model) {
		super(null, model.getEmail(), model.getActive(), model.getId(), model.getNotifications());
		this.jmbg = model.getJmbg();
		this.name = model.getName();
		this.lastname = model.getLastname();
		this.birthdate = model.getBirthdate();
		this.address = new AddressDTO(model.getAddress().getStreet(), model.getAddress().getNumber(),
				model.getAddress().getId());

		if (model.getStudentyear() != null) {
			for (StudentYear sy : model.getStudentyear()) {
				this.studentyears.add(new StudentYearDTO(sy.getIndexnumber(), sy.getDateofenrollment(), sy.getId()));
			}
		}

		if (model.getAttendingsubjects() != null) {
			for (AttendingSubject as : model.getAttendingsubjects()) {
				this.attendingsubjects.add(new AttendingSubjectDTO(as.getFinalgrade(), as.getId()));
			}
		}

		if (model.getEntranceexams() != null) {
			for (EntranceExam as : model.getEntranceexams()) {
				this.entranceexams.add(new EntranceExamDTO(as.getId(), as.getRequireddatetime(), as.getAccepted()));
			}
		}
	}

	public StudentDTO(String jmbg, String name, String lastname, Date birthdate, long id, String email,
			boolean active) {
		super(null, email, active, id, null);
		this.jmbg = jmbg;
		this.name = name;
		this.lastname = lastname;
		this.birthdate = birthdate;
	}

	public StudentDTO() {

	}

	public String getJmbg() {
		return this.jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthdate() {
		return this.birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public Set<StudentYearDTO> getStudentyears() {
		return this.studentyears;
	}

	public void setStudentyears(Set<StudentYearDTO> studentyears) {
		this.studentyears = studentyears;
	}

	public Set<AttendingSubjectDTO> getAttendingsubjects() {
		return this.attendingsubjects;
	}

	public void setAttendingsubjects(Set<AttendingSubjectDTO> attendingsubjects) {
		this.attendingsubjects = attendingsubjects;
	}

	public Set<EntranceExamDTO> getEntranceexams() {
		return entranceexams;
	}

	public void setEntranceexams(Set<EntranceExamDTO> entranceexams) {
		this.entranceexams = entranceexams;
	}

}