
package app.DTO;


 
import java.util.HashSet;
import java.util.Set;

import app.model.ClassRealization;
import app.model.TeacherAtRealization;

public class TeacherAtRealizationDTO  {
	private long id;
    private int numberofclasses;
    private Set<ClassRealizationDTO> classrealizations = new HashSet<ClassRealizationDTO>();
    private classTypeDTO classtype;
    

    public TeacherAtRealizationDTO(int numberofclasses, Set<ClassRealizationDTO> classrealizations, classTypeDTO classtype,  long id ) {
        
        this.id=id; 
        this.numberofclasses = numberofclasses;
        this.classrealizations = classrealizations;
        this.classtype = classtype;
    }

    public TeacherAtRealizationDTO(TeacherAtRealization model){ 
        this.id = model.getId();  
        this.numberofclasses = model.getNumberofclasses();  
        for(ClassRealization cr : model.getClassrealizations()) {
        	this.classrealizations.add(new ClassRealizationDTO(cr.getId()));
        }
        this.classtype = new classTypeDTO(model.getClasstype().getName(), model.getClasstype().getId()); 
    }

    public TeacherAtRealizationDTO(int numberofclasses, long id) {
        this.id=id; 
        this.numberofclasses = numberofclasses;
    }
    
    public TeacherAtRealizationDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public int getNumberofclasses() {
		return this.numberofclasses;
	}

	public void setNumberofclasses(int numberofclasses) {
		this.numberofclasses = numberofclasses;
	}
    
    public Set<ClassRealizationDTO> getClassrealizations() {
		return this.classrealizations;
	}

	public void setClassrealizations(Set<ClassRealizationDTO> classrealizations) {
		this.classrealizations = classrealizations;
	}
    
    public classTypeDTO getClasstype() {
		return this.classtype;
	}

	public void setClasstype(classTypeDTO classtype) {
		this.classtype = classtype;
	}
    
}