
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Result;

@Repository
public interface ResultRepository extends PagingAndSortingRepository<Result, Long> {

}
