
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Faculty;

@Repository
public interface FacultyRepository extends PagingAndSortingRepository<Faculty, Long> {

}
