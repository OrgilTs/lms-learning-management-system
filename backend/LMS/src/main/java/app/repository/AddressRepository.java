
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Address;

@Repository
public interface AddressRepository extends PagingAndSortingRepository<Address, Long> {

}
