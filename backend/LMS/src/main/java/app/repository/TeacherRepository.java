
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Teacher;

@Repository
public interface TeacherRepository extends PagingAndSortingRepository<Teacher, Long> {

}
