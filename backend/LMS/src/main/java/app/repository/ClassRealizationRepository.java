
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.ClassRealization;

@Repository
public interface ClassRealizationRepository extends PagingAndSortingRepository<ClassRealization, Long> {

}
