
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.StudyYear;

@Repository
public interface StudyYearRepository extends PagingAndSortingRepository<StudyYear, Long> {

}
