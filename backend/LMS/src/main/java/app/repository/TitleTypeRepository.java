
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.TitleType;

@Repository
public interface TitleTypeRepository extends PagingAndSortingRepository<TitleType, Long> {

}
