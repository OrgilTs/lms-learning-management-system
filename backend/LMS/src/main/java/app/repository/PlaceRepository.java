
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Place;

@Repository
public interface PlaceRepository extends PagingAndSortingRepository<Place, Long> {

}
