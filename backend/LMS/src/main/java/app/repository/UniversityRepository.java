
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.University;

@Repository
public interface UniversityRepository extends PagingAndSortingRepository<University, Long> {

}
