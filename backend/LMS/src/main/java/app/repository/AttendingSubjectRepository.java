
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.AttendingSubject;

@Repository
public interface AttendingSubjectRepository extends PagingAndSortingRepository<AttendingSubject, Long> {

}
