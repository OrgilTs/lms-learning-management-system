
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Major;

@Repository
public interface MajorRepository extends PagingAndSortingRepository<Major, Long> {

}
