
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Student;

@Repository
public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {

}
