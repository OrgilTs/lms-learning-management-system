
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.StudentYear;

@Repository
public interface StudentYearRepository extends PagingAndSortingRepository<StudentYear, Long> {

}
