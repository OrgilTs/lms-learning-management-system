
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.AdministrativeStaff;

@Repository
public interface AdministrativeStaffRepository extends PagingAndSortingRepository<AdministrativeStaff, Long> {
	AdministrativeStaff getByEmail(String email);
}
