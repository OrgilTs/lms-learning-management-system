
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.ScientificField;

@Repository
public interface ScientificFieldRepository extends PagingAndSortingRepository<ScientificField, Long> {

}
