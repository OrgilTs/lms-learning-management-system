
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.UniversityNotification;

@Repository
public interface UniversityNotificationRepository extends PagingAndSortingRepository<UniversityNotification, Long> {

}
