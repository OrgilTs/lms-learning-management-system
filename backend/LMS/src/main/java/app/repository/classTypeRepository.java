
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.classType;

@Repository
public interface classTypeRepository extends PagingAndSortingRepository<classType, Long> {

}
