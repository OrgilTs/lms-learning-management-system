
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Country;

@Repository
public interface CountryRepository extends PagingAndSortingRepository<Country, Long> {

}
