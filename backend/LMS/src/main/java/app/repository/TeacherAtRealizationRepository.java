
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.TeacherAtRealization;

@Repository
public interface TeacherAtRealizationRepository extends PagingAndSortingRepository<TeacherAtRealization, Long> {

}
