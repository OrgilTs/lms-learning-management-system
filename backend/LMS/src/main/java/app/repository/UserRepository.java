
package app.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	User getByEmail(String email);
	Optional<User> getByEmailAndPassword(String email, String password);
}
