
package app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.EntranceExam;
import app.model.Student;

@Repository
public interface EntranceExamRepository extends PagingAndSortingRepository<EntranceExam, Long> {
	Page<EntranceExam> getByStudent(Student studdent, Pageable pageable);
}
