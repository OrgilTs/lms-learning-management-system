
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Title;

@Repository
public interface TitleRepository extends PagingAndSortingRepository<Title, Long> {

}
