package app.service;


import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.model.User;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UserService userService;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userService.findOne(username);
		if(user != null) {
			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		
			if(user.getClass().getName() ==  "app.model.Administrator") {
				grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			}
			else if(user.getClass().getName() ==  "app.model.AdministrativeStaff") {
				grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_STAFF"));
			}
			else if(user.getClass().getName() ==  "app.model.Teacher") {
				grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_TEACHER"));
			}
			else if(user.getClass().getName() ==  "app.model.Student") {
				grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_STUDENT"));
			}
			else {
				grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_GUEST"));
			}
			
			return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), grantedAuthorities);
		}
		
		return null;
	}

}
