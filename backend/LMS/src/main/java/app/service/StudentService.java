
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Student;
import app.repository.StudentRepository;

@Service
public class StudentService {
	@Autowired
	private StudentRepository repository;

	public Page<Student> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Student findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Student object) {
		repository.delete(object);
	}

	public void save(Student object) {
		repository.save(object);
	}
}