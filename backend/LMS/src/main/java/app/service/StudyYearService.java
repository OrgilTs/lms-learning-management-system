
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.StudyYear;
import app.repository.StudyYearRepository;

@Service
public class StudyYearService {
	@Autowired
	private StudyYearRepository repository;

	public Page<StudyYear> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public StudyYear findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(StudyYear object) {
		repository.delete(object);
	}

	public void save(StudyYear object) {
		repository.save(object);
	}
}