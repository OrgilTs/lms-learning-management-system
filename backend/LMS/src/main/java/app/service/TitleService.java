
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Title;
import app.repository.TitleRepository;

@Service
public class TitleService {
	@Autowired
	private TitleRepository repository;

	public Page<Title> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Title findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Title object) {
		repository.delete(object);
	}

	public void save(Title object) {
		repository.save(object);
	}
}