
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.AttendingSubject;
import app.repository.AttendingSubjectRepository;

@Service
public class AttendingSubjectService {
	@Autowired
	private AttendingSubjectRepository repository;

	public Page<AttendingSubject> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public AttendingSubject findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(AttendingSubject object) {
		repository.delete(object);
	}

	public void save(AttendingSubject object) {
		repository.save(object);
	}
}