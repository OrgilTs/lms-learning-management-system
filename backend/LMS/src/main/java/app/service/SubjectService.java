
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Subject;
import app.repository.SubjectRepository;

@Service
public class SubjectService {
	@Autowired
	private SubjectRepository repository;

	public Page<Subject> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Subject findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Subject object) {
		repository.delete(object);
	}

	public void save(Subject object) {
		repository.save(object);
	}
}