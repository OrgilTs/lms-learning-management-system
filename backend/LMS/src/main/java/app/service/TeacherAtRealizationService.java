
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.TeacherAtRealization;
import app.repository.TeacherAtRealizationRepository;

@Service
public class TeacherAtRealizationService {
	@Autowired
	private TeacherAtRealizationRepository repository;

	public Page<TeacherAtRealization> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public TeacherAtRealization findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(TeacherAtRealization object) {
		repository.delete(object);
	}

	public void save(TeacherAtRealization object) {
		repository.save(object);
	}
}