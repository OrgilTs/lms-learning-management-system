
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Result;
import app.repository.ResultRepository;

@Service
public class ResultService {
	@Autowired
	private ResultRepository repository;

	public Page<Result> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Result findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Result object) {
		repository.delete(object);
	}

	public void save(Result object) {
		repository.save(object);
	}
}