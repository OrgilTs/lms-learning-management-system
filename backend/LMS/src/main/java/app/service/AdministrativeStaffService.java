
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.AdministrativeStaff;
import app.repository.AdministrativeStaffRepository;

@Service
public class AdministrativeStaffService {
	@Autowired
	private AdministrativeStaffRepository repository;

	public Page<AdministrativeStaff> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public AdministrativeStaff findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public AdministrativeStaff findOne(String email) {
		return  repository.getByEmail(email);
	}
	
	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(AdministrativeStaff object) {
		repository.delete(object);
	}

	public void save(AdministrativeStaff object) {
		repository.save(object);
	}
}