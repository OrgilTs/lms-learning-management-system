
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Schedule;
import app.repository.ScheduleRepository;

@Service
public class ScheduleService {
	@Autowired
	private ScheduleRepository repository;

	public Page<Schedule> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Schedule findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Schedule object) {
		repository.delete(object);
	}

	public void save(Schedule object) {
		repository.save(object);
	}
}