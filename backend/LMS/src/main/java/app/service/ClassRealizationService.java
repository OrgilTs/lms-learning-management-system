
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.ClassRealization;
import app.repository.ClassRealizationRepository;

@Service
public class ClassRealizationService {
	@Autowired
	private ClassRealizationRepository repository;

	public Page<ClassRealization> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public ClassRealization findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(ClassRealization object) {
		repository.delete(object);
	}

	public void save(ClassRealization object) {
		repository.save(object);
	}
}