
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.TitleType;
import app.repository.TitleTypeRepository;

@Service
public class TitleTypeService {
	@Autowired
	private TitleTypeRepository repository;

	public Page<TitleType> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public TitleType findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(TitleType object) {
		repository.delete(object);
	}

	public void save(TitleType object) {
		repository.save(object);
	}
}