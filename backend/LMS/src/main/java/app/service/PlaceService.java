
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Place;
import app.repository.PlaceRepository;

@Service
public class PlaceService {
	@Autowired
	private PlaceRepository repository;

	public Page<Place> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Place findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Place object) {
		repository.delete(object);
	}

	public void save(Place object) {
		repository.save(object);
	}
}