
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.ScientificField;
import app.repository.ScientificFieldRepository;

@Service
public class ScientificFieldService {
	@Autowired
	private ScientificFieldRepository repository;

	public Page<ScientificField> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public ScientificField findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(ScientificField object) {
		repository.delete(object);
	}

	public void save(ScientificField object) {
		repository.save(object);
	}
}