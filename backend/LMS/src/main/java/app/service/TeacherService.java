
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Teacher;
import app.repository.TeacherRepository;

@Service
public class TeacherService {
	@Autowired
	private TeacherRepository repository;

	public Page<Teacher> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Teacher findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Teacher object) {
		repository.delete(object);
	}

	public void save(Teacher object) {
		repository.save(object);
	}
}