
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.University;
import app.repository.UniversityRepository;

@Service
public class UniversityService {
	@Autowired
	private UniversityRepository repository;

	public Page<University> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public University findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(University object) {
		repository.delete(object);
	}

	public void save(University object) {
		repository.save(object);
	}
}