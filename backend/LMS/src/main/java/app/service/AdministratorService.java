
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Administrator;
import app.repository.AdministratorRepository;

@Service
public class AdministratorService {
	@Autowired
	private AdministratorRepository repository;

	public Page<Administrator> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Administrator findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Administrator object) {
		repository.delete(object);
	}

	public void save(Administrator object) {
		repository.save(object);
	}
}