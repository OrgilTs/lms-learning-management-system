
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Major;
import app.repository.MajorRepository;

@Service
public class MajorService {
	@Autowired
	private MajorRepository repository;

	public Page<Major> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Major findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Major object) {
		repository.delete(object);
	}

	public void save(Major object) {
		repository.save(object);
	}
}