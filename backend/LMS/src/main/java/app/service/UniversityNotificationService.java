
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.UniversityNotification;
import app.repository.UniversityNotificationRepository;

@Service
public class UniversityNotificationService {
	@Autowired
	private UniversityNotificationRepository repository;

	public Page<UniversityNotification> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public UniversityNotification findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(UniversityNotification object) {
		repository.delete(object);
	}

	public void save(UniversityNotification object) {
		repository.save(object);
	}
}