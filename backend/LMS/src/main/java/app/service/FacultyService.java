
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Faculty;
import app.repository.FacultyRepository;

@Service
public class FacultyService {
	@Autowired
	private FacultyRepository repository;

	public Page<Faculty> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Faculty findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Faculty object) {
		repository.delete(object);
	}

	public void save(Faculty object) {
		repository.save(object);
	}
}