
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.Address;
import app.repository.AddressRepository;

@Service
public class AddressService {
	@Autowired
	private AddressRepository repository;

	public Page<Address> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Address findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(Address object) {
		repository.delete(object);
	}

	public void save(Address object) {
		repository.save(object);
	}
}