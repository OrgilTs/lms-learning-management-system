
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.classType;
import app.repository.classTypeRepository;

@Service
public class classTypeService {
	@Autowired
	private classTypeRepository repository;

	public Page<classType> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public classType findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(classType object) {
		repository.delete(object);
	}

	public void save(classType object) {
		repository.save(object);
	}
}