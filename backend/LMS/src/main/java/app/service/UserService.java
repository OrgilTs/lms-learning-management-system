
package app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.User;
import app.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository repository;

	public Page<User> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public User findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(User object) {
		repository.delete(object);
	}

	public void save(User object) {
		repository.save(object);
	}

	public User findOne(String email) {
		return repository.getByEmail(email);
	}

	public Optional<User> findOne(String email, String password) {
		return repository.getByEmailAndPassword(email, password);
	}

}