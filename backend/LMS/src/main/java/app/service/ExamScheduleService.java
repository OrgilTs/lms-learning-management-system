
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.ExamSchedule;
import app.repository.ExamScheduleRepository;

@Service
public class ExamScheduleService {
	@Autowired
	private ExamScheduleRepository repository;

	public Page<ExamSchedule> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public ExamSchedule findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(ExamSchedule object) {
		repository.delete(object);
	}

	public void save(ExamSchedule object) {
		repository.save(object);
	}
}