package app.utils;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import app.model.User;
import app.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenUtils {
	@Value("${token.secret}")
	private String secret;
	
	@Value("${token.expiration}")
	private Long expiration;
	
	@Autowired
	private UserService userService;
	
	private Claims getClaims(String token) {
		Claims claims;
		
		try {
			claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
		}
		catch(Exception e) {
			claims = null;
		}
		
		return claims;
	}
	
	private boolean isExpired(String token) {
		final Date expiration = this.getExpirationDate(token);
		return expiration.before(new Date(System.currentTimeMillis()));
	}
	
	public String getUsername(String token) {
		String username;
		
		try {
			Claims claims = this.getClaims(token);
			username = claims.getSubject();
		}
		catch(Exception e) {
			username = null;
		}
		
		return username;
	}
	
	public Date getExpirationDate(String token) {
		Date expiration;
		
		try {
			final Claims claims = this.getClaims(token);
			expiration = claims.getExpiration();
		}
		catch(Exception e) {
			expiration = null;
		}
		
		return expiration;
	}
	
	public boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsername(token);
		return (username.equals(userDetails.getUsername()) && !isExpired(token));
	}
	
	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<String, Object>();
		claims.put("sub", userDetails.getUsername());
		claims.put("created", new Date(System.currentTimeMillis()));
		
		User user = userService.findOne(userDetails.getUsername());
		if(user.getClass().getName() ==  "app.model.Administrator") {
			claims.put("role", "ROLE_ADMIN");
		}
		else if(user.getClass().getName() ==  "app.model.AdministrativeStaff") {
			claims.put("role", "ROLE_STAFF");
		}
		else if(user.getClass().getName() ==  "app.model.Teacher") {
			claims.put("role", "ROLE_TEACHER");
		}
		else if(user.getClass().getName() ==  "app.model.Student") {
			claims.put("role", "ROLE_STUDENT");
		}
		else {
			claims.put("role", "ROLE_GUEST");
		}
		claims.put("active", user.getActive());
		claims.put("id", user.getId());
		return Jwts.builder().setClaims(claims).setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
				.signWith(SignatureAlgorithm.HS512, this.secret).compact();
	}
}
