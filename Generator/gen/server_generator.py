from jinja2 import Template

class ServerGenerator:
    def __init__(self, project, package, output_path):
        self.project = project
        self.output_path = output_path
        self.package = package
        self.tmpl = Template("""
package {{package.lower()}};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class {{package.title()}} extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run({{package.title()}}.class, args);
	}
}

""")

    def render(self):
        try:
            self.tmpl.stream(package=self.package).dump("{}/{}.java".format(self.output_path, self.package.title()))
            print("[{0}]: {1}.java generated successfully...".format(self.project, self.package.title()))
        except Exception as er:
            print("\033[91m Error >>> [{0}]: {1}.java: \n {2} \033[0m".format(self.project, self.package.title(), er))
        