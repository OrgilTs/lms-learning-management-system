from jinja2 import Template

class ControllerGenerator:
    def __init__(self, clas, project, package, output_path):
        self.clas = clas
        self.project = project
        self.output_path = output_path
        self.package = package
        self.tmpl = Template("""
package {{package.lower()}}.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import {{package.lower()}}.DTO.{{clas.name}}DTO;
import {{package.lower()}}.model.{{clas.name}};
import {{package.lower()}}.service.{{clas.name}}Service;


@Controller
@RequestMapping(path = "/api/{{clas.name.lower()}}")
public class {{clas.name}}Controller {
	@Autowired
	private {{clas.name}}Service service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<{{clas.name}}DTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<{{clas.name}}DTO>>(service.findAll(pageable).map(i -> new {{clas.name}}DTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		{{clas.name}} obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<{{clas.name}}DTO>(new {{clas.name}}DTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<?> save(@RequestBody() {{clas.name}} object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@RequestBody() {{clas.name}} object) {
		{{clas.name}} obj = service.findOne(object.getId());
		if (obj != null) {
			service.save(object);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestBody() {{clas.name}} object) {
		{{clas.name}} obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}
""")

    def render(self):
        try:
            self.tmpl.stream(clas=self.clas, package=self.package).dump("{}/{}Controller.java".format(self.output_path, self.clas["name"]))
            print("[{0}]: {1}Controller.java generated successfully...".format(self.project, self.clas["name"]))
        except Exception as er:
            print("\033[91m Error >>> [{0}]: {1}.java: \n {2} \033[0m".format(self.project, self.clas["name"], er))
        