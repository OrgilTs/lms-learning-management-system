from jinja2 import Template

class ModelDtoGenerator:
    def __init__(self, clas, project, package, output_path):
        self.clas = clas
        self.project = project
        self.output_path = output_path
        self.package = package
        self.tmpl = Template("""
package {{package.lower()}}.DTO;


{%if clas.parent != None%}import {{package.lower()}}.DTO.{{clas.parent.name}}DTO; {%endif%} 
import {{package.lower()}}.model.{{clas.name}};

public class {{clas.name}}DTO {%if clas.parent != None%}extends {{clas.parent.name}}DTO {% endif %} {
	private long id;
    {% for attribute in clas.attributes %}private {{ attribute.type }} {{ attribute.name.lower() }};
    {% endfor %}

    public {{clas.name}}DTO({% for attribute in clas.attributes %}{{ attribute.type }} {{ attribute.name.lower()}}, {% endfor %} long id {%if clas.parent != None%}{%for a in clas.parent.attributes%}, {{ a.type }} {{ a.name.lower()}}{%endfor%}{% endif %}) {
        {%if clas.parent != None%} super({%for a in clas.parent.attributes%}{%if loop.index >1%},{%endif%}{{a.name.lower()}}{%endfor%}, id);{% endif %}
        this.id=id; {% for attribute in clas.attributes %}
        this.{{attribute.name.lower()}} = {{attribute.name.lower()}};{% endfor %}
    }

    public {{clas.name}}DTO({{clas.name}} model){ 
        this.id = model.getId(); {% for attribute in clas.attributes %} 
        this.{{attribute.name.lower()}} = model.get{{attribute.name.title()}}(); {% endfor %}
    }

    public {{clas.name}}DTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    {% for attribute in clas.attributes %}
    public {{ attribute.type }} get{{attribute.name.title()}}() {
		return this.{{attribute.name.lower()}};
	}

	public void set{{attribute.name.title()}}({{attribute.type}} {{attribute.name.lower()}}) {
		this.{{attribute.name.lower()}} = {{attribute.name.lower()}};
	}
    {% endfor %}
}
""")

    def render(self):
        try:
            self.tmpl.stream(clas=self.clas, package=self.package).dump("{}/{}DTO.java".format(self.output_path, self.clas["name"]))
            print("[{0}]: {1}DTO.java generated successfully...".format(self.project, self.clas["name"]))
        except Exception as er:
            print("\033[91m Error >>> [{0}]: {1}.java: \n {2} \033[0m".format(self.project, self.clas["name"], er))
        