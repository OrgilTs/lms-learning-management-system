import os, shutil, json
from model_generator import ModelGenerator
from DTO_generator import ModelDtoGenerator
from service_generator import ServiceGenerator
from repository_generator import RepositoryGenerator
from controller_generator import ControllerGenerator
from server_generator import ServerGenerator

def getClass(index, name):
    for clas in data[index]["classes"]:
        if clas["name"] == name:
            return clas

data = None

try:
    with open("./project.json") as f:
        data = json.load(f)
except Exception as e:
    print("[ERROR]: {}".format(e))
    exit()

for i in range(0, len(data)):
    print("[{}]: Preparing please wait...".format(data[i]["projectName"]))
    classes = []
    for clas in data[i]["classes"]:
        if clas["parent"] is not None:
            parent = getClass(i, clas["parent"])
            if parent == None:
                print("Error: class {} does not exist in the project".format(clas["parent"]))
                cont = ""
                while cont != "y" or cont != "n":
                    input("Continue y/n: ")
                    if cont == "n":
                        exit()
            else:
                clas["parent"] = parent
        classes.append(clas)

    if os.path.exists('./myProjects') is False:
        os.mkdir("./myProjects")

    if os.path.exists('./myProjects/{}'.format(data[i]["projectName"])) is True:
        shutil.rmtree('./myProjects/{0}'.format(data[i]["projectName"]))
    
    os.mkdir("./myProjects/{}".format(data[i]["projectName"]))
    os.mkdir("./myProjects/{}/{}".format(data[i]["projectName"], data[i]["package"]))
    os.mkdir("./myProjects/{}/{}/model".format(data[i]["projectName"], data[i]["package"]))
    os.mkdir("./myProjects/{}/{}/repository".format(data[i]["projectName"], data[i]["package"]))
    os.mkdir("./myProjects/{}/{}/DTO".format(data[i]["projectName"], data[i]["package"]))
    os.mkdir("./myProjects/{}/{}/service".format(data[i]["projectName"], data[i]["package"]))
    os.mkdir("./myProjects/{}/{}/controller".format(data[i]["projectName"], data[i]["package"]))
    
    print("[{}]: Start generating...".format(data[i]["projectName"]))
    server_generator = ServerGenerator(data[i]["projectName"],  data[i]["package"], "myProjects/{}/{}/".format(data[i]["projectName"], data[i]["package"]))
    server_generator.render()

    for c in classes:
        model = ModelGenerator(c, data[i]["projectName"],  data[i]["package"], "myProjects/{}/{}/model".format(data[i]["projectName"], data[i]["package"]))
        model.render()
        model_dto = ModelDtoGenerator(c, data[i]["projectName"],  data[i]["package"], "myProjects/{}/{}/DTO".format(data[i]["projectName"], data[i]["package"]))
        model_dto.render()
        service = ServiceGenerator(c, data[i]["projectName"],  data[i]["package"], "myProjects/{}/{}/service".format(data[i]["projectName"], data[i]["package"]))
        service.render()
        repository = RepositoryGenerator(c, data[i]["projectName"],  data[i]["package"], "myProjects/{}/{}/repository".format(data[i]["projectName"], data[i]["package"]))
        repository.render()
        controller = ControllerGenerator(c, data[i]["projectName"],  data[i]["package"], "myProjects/{}/{}/controller".format(data[i]["projectName"], data[i]["package"]))
        controller.render()
    print("*** GENERATED ***")