from jinja2 import Template

class RepositoryGenerator:
    def __init__(self, clas, project, package, output_path):
        self.clas = clas
        self.project = project
        self.output_path = output_path
        self.package = package
        self.tmpl = Template("""
package {{package.lower()}}.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.{{clas.name}};

@Repository
public interface {{clas.name}}Repository extends PagingAndSortingRepository<{{clas.name}}, Long> {

}

""")

    def render(self):
        try:
            self.tmpl.stream(clas=self.clas, package=self.package).dump("{}/{}Repository.java".format(self.output_path, self.clas["name"]))
            print("[{0}]: {1}Repository.java generated successfully...".format(self.project, self.clas["name"]))
        except Exception as er:
            print("\033[91m Error >>> [{0}]: {1}.java: \n {2} \033[0m".format(self.project, self.clas["name"], er))
        