
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.Schedule;

@Repository
public interface ScheduleRepository extends PagingAndSortingRepository<Schedule, Long> {

}
