
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.EntranceExam;

@Repository
public interface EntranceExamRepository extends PagingAndSortingRepository<EntranceExam, Long> {

}
