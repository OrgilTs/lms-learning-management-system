
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.DayTitle;

@Repository
public interface DayTitleRepository extends PagingAndSortingRepository<DayTitle, Long> {

}
