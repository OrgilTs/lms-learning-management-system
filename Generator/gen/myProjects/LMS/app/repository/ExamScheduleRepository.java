
package app.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import app.model.ExamSchedule;

@Repository
public interface ExamScheduleRepository extends PagingAndSortingRepository<ExamSchedule, Long> {

}
