
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.UserNotification;
import app.repository.UserNotificationRepository;

@Service
public class UserNotificationService {
	@Autowired
	private UserNotificationRepository repository;

	public Page<UserNotification> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public UserNotification findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(UserNotification object) {
		repository.delete(object);
	}

	public void save(UserNotification object) {
		repository.save(object);
	}
}