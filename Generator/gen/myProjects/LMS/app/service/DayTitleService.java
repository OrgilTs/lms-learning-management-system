
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.DayTitle;
import app.repository.DayTitleRepository;

@Service
public class DayTitleService {
	@Autowired
	private DayTitleRepository repository;

	public Page<DayTitle> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public DayTitle findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(DayTitle object) {
		repository.delete(object);
	}

	public void save(DayTitle object) {
		repository.save(object);
	}
}