
package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.model.EntranceExam;
import app.repository.EntranceExamRepository;

@Service
public class EntranceExamService {
	@Autowired
	private EntranceExamRepository repository;

	public Page<EntranceExam> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public EntranceExam findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete(EntranceExam object) {
		repository.delete(object);
	}

	public void save(EntranceExam object) {
		repository.save(object);
	}
}