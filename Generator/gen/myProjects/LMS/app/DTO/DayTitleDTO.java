
package app.DTO;


 
import app.model.DayTitle;

public class DayTitleDTO  {
	private long id;
    private String name;
    private Schedule schedules;
    

    public DayTitleDTO(String name, Schedule schedules,  long id ) {
        
        this.id=id; 
        this.name = name;
        this.schedules = schedules;
    }

    public DayTitleDTO(DayTitle model){ 
        this.id = model.getId();  
        this.name = model.getName();  
        this.schedules = model.getSchedules(); 
    }

    public DayTitleDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public Schedule getSchedules() {
		return this.schedules;
	}

	public void setSchedules(Schedule schedules) {
		this.schedules = schedules;
	}
    
}