
package app.DTO;


 
import app.model.UniversityNotification;

public class UniversityNotificationDTO  {
	private long id;
    private University university;
    private LocalDateTime created;
    private String title;
    private String message;
    

    public UniversityNotificationDTO(University university, LocalDateTime created, String title, String message,  long id ) {
        
        this.id=id; 
        this.university = university;
        this.created = created;
        this.title = title;
        this.message = message;
    }

    public UniversityNotificationDTO(UniversityNotification model){ 
        this.id = model.getId();  
        this.university = model.getUniversity();  
        this.created = model.getCreated();  
        this.title = model.getTitle();  
        this.message = model.getMessage(); 
    }

    public UniversityNotificationDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public University getUniversity() {
		return this.university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}
    
    public LocalDateTime getCreated() {
		return this.created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
    
    public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
    
    public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
    
}