
package app.DTO;


 
import app.model.EntranceExam;

public class EntranceExamDTO  {
	private long id;
    private Student student;
    private LocalDateTime requireddatetime;
    private boolean accepted;
    

    public EntranceExamDTO(Student student, LocalDateTime requireddatetime, boolean accepted,  long id ) {
        
        this.id=id; 
        this.student = student;
        this.requireddatetime = requireddatetime;
        this.accepted = accepted;
    }

    public EntranceExamDTO(EntranceExam model){ 
        this.id = model.getId();  
        this.student = model.getStudent();  
        this.requireddatetime = model.getRequireddatetime();  
        this.accepted = model.getAccepted(); 
    }

    public EntranceExamDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
    
    public LocalDateTime getRequireddatetime() {
		return this.requireddatetime;
	}

	public void setRequireddatetime(LocalDateTime requireddatetime) {
		this.requireddatetime = requireddatetime;
	}
    
    public boolean getAccepted() {
		return this.accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
    
}