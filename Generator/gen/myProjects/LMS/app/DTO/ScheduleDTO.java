
package app.DTO;


 
import app.model.Schedule;

public class ScheduleDTO  {
	private long id;
    private LocalTime start;
    private LocalTime end;
    private DayTitle daytitle;
    private Subject subject;
    

    public ScheduleDTO(LocalTime start, LocalTime end, DayTitle daytitle, Subject subject,  long id ) {
        
        this.id=id; 
        this.start = start;
        this.end = end;
        this.daytitle = daytitle;
        this.subject = subject;
    }

    public ScheduleDTO(Schedule model){ 
        this.id = model.getId();  
        this.start = model.getStart();  
        this.end = model.getEnd();  
        this.daytitle = model.getDaytitle();  
        this.subject = model.getSubject(); 
    }

    public ScheduleDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public LocalTime getStart() {
		return this.start;
	}

	public void setStart(LocalTime start) {
		this.start = start;
	}
    
    public LocalTime getEnd() {
		return this.end;
	}

	public void setEnd(LocalTime end) {
		this.end = end;
	}
    
    public DayTitle getDaytitle() {
		return this.daytitle;
	}

	public void setDaytitle(DayTitle daytitle) {
		this.daytitle = daytitle;
	}
    
    public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}
    
}