
package app.DTO;


 
import app.model.ExamSchedule;

public class ExamScheduleDTO  {
	private long id;
    private LocalDateTime created;
    private Subject subject;
    

    public ExamScheduleDTO(LocalDateTime created, Subject subject,  long id ) {
        
        this.id=id; 
        this.created = created;
        this.subject = subject;
    }

    public ExamScheduleDTO(ExamSchedule model){ 
        this.id = model.getId();  
        this.created = model.getCreated();  
        this.subject = model.getSubject(); 
    }

    public ExamScheduleDTO() {

    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public LocalDateTime getCreated() {
		return this.created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
    
    public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}
    
}