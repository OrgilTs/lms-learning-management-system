
package app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UserNotification  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @ManyToOne
    private User user;
    
    @Column(nullable = false)
    private LocalDateTime created;
    
    @Column(nullable = false)
    private String title;
    
    @Column(nullable = false)
    private String message;
    

    public UserNotification(User user, LocalDateTime created, String title, String message,  long id ) {
        
        this.id=id; 
        this.user = user;
        this.created = created;
        this.title = title;
        this.message = message;
    }

    public UserNotification() {
    	super();
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
    
    public LocalDateTime getCreated() {
		return this.created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
    
    public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
    
    public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
    
}