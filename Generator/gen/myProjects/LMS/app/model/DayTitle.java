
package app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DayTitle  {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    
    @Column(nullable = false)
    private String name;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = 'daytitle')
    private Schedule schedules;
    

    public DayTitle(String name, Schedule schedules,  long id ) {
        
        this.id=id; 
        this.name = name;
        this.schedules = schedules;
    }

    public DayTitle() {
    	super();
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
    
    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public Schedule getSchedules() {
		return this.schedules;
	}

	public void setSchedules(Schedule schedules) {
		this.schedules = schedules;
	}
    
}