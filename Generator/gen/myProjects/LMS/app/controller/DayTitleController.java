
package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.DTO.DayTitleDTO;
import app.model.DayTitle;
import app.service.DayTitleService;


@Controller
@RequestMapping(path = "/api/daytitle")
public class DayTitleController {
	@Autowired
	private DayTitleService service;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<DayTitleDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<Page<DayTitleDTO>>(service.findAll(pageable).map(i -> new DayTitleDTO(i)),
				HttpStatus.OK);
	}
	

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(@PathVariable("id") Long id) {
		DayTitle obj = service.findOne(id);
		if (obj != null) {
			return new ResponseEntity<DayTitleDTO>(new DayTitleDTO(obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<?> save(@RequestBody() DayTitle object) {
		service.save(object);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<?> update(@RequestBody() DayTitle object) {
		DayTitle obj = service.findOne(object.getId());
		if (obj != null) {
			service.save(object);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestBody() DayTitle object) {
		DayTitle obj = service.findOne(object.getId());
		if (obj != null) {
			service.delete(object.getId());
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
	}
}