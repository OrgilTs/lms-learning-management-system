from jinja2 import Template

class ServiceGenerator:
    def __init__(self, clas, project, package, output_path):
        self.clas = clas
        self.project = project
        self.output_path = output_path
        self.package = package
        self.tmpl = Template("""
package {{package.lower()}}.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import {{package.lower()}}.model.{{clas.name}};
import {{package.lower()}}.repository.{{clas.name}}Repository;

@Service
public class {{clas.name}}Service {
	@Autowired
	private {{clas.name}}Repository repository;

	public Page<{{clas.name}}> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public {{clas.name}} findOne(long id) {
		return repository.findById(id).orElse(null);
	}

	public void delete(long id) {
		repository.deleteById(id);
	}

	public void delete({{clas.name}} object) {
		repository.delete(object);
	}

	public void save({{clas.name}} object) {
		repository.save(object);
	}
}
""")

    def render(self):
        try:
            self.tmpl.stream(clas=self.clas, package=self.package).dump("{}/{}Service.java".format(self.output_path, self.clas["name"]))
            print("[{0}]: {1}Service.java generated successfully...".format(self.project, self.clas["name"]))
        except Exception as er:
            print("\033[91m Error >>> [{0}]: {1}.java: \n {2} \033[0m".format(self.project, self.clas["name"], er))
        