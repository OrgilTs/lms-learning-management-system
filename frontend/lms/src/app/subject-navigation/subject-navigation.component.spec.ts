import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectNavigationComponent } from './subject-navigation.component';

describe('SubjectNavigationComponent', () => {
  let component: SubjectNavigationComponent;
  let fixture: ComponentFixture<SubjectNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
