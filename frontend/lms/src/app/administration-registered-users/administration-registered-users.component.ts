import { Component, OnInit } from '@angular/core';
import { TeacherService } from '../service/teacher.service';
import { AdministrativeStaffService } from '../service/administrative-staff.service';

@Component({
  selector: 'app-administration-registered-users',
  templateUrl: './administration-registered-users.component.html',
  styleUrls: ['./administration-registered-users.component.css']
})
export class AdministrationRegisteredUsersComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
