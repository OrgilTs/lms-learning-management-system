import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationRegisteredUsersComponent } from './administration-registered-users.component';

describe('AdministrationRegisteredUsersComponent', () => {
  let component: AdministrationRegisteredUsersComponent;
  let fixture: ComponentFixture<AdministrationRegisteredUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationRegisteredUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationRegisteredUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
