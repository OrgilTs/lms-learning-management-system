import { Component, OnInit } from '@angular/core';
import { TeacherService } from '../service/teacher.service';
import { Teacher } from '../model/Teacher';


@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {

  teacher: Teacher={ id: null,active: null,university:null, address: null, email: null, password: null, jmbg:null, name: null, lastname: null, biography:null,faculty: null,major:null};
  teachers: Teacher[]=[];
  
  constructor(private ts: TeacherService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.ts.getAll("").subscribe(response => this.teachers = response["content"]);
  }

  submit() {
    this.ts.add(this.teacher).subscribe(() => this.getAll());
  }

}
