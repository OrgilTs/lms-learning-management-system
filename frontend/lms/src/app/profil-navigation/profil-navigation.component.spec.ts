import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilNavigationComponent } from './profil-navigation.component';

describe('ProfilNavigationComponent', () => {
  let component: ProfilNavigationComponent;
  let fixture: ComponentFixture<ProfilNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
