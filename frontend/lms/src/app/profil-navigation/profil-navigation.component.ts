import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-profil-navigation',
  templateUrl: './profil-navigation.component.html',
  styleUrls: ['./profil-navigation.component.css']
})
export class ProfilNavigationComponent implements OnInit {
  role;

  constructor(private ls: LoginService) { }

  ngOnInit(): void {
    this.role = this.ls.getRole();
  }

}
