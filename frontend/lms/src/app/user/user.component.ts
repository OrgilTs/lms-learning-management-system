import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user = {
    id:null,
    email: null,
    password: null,
    active:null
  }

  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
  }

}
