import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationCodebookComponent } from './administration-codebook.component';

describe('AdministrationCodebookComponent', () => {
  let component: AdministrationCodebookComponent;
  let fixture: ComponentFixture<AdministrationCodebookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationCodebookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationCodebookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
