import { Component, OnInit } from '@angular/core';
import { User } from '../model/User';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  user = {
    id:null,
    email: null,
    password: null,
    active:null
  }
  users: User[]=[];

  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
  }

}
