import { Component, OnInit } from '@angular/core';
import { Usernotification } from '../model/Usernotification';
import { User } from '../model/User';
import { UserNotificationService } from '../service/user-notification.service';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-notifications-user-add',
  templateUrl: './notifications-user-add.component.html',
  styleUrls: ['./notifications-user-add.component.css']
})
export class NotificationsUserAddComponent implements OnInit {

  user: User = { id:null, active :true,email :null, password :null, notifications:null}
  users: User[]=[];
  userNotification: Usernotification  = {id:null, created: null, title: null, user: { id:null, active :true,email :null, password :null, notifications:null}, message: null}

  constructor(private nus: UserNotificationService, private us:UserService, private router: Router) { }

  ngOnInit(): void {
    this.us.getAll("").subscribe(response => this.users = response["content"]);
  }

  submit() {
    this.nus.add(this.userNotification).subscribe(
      result => {
        this.router.navigate(["obavestenja"]);
      },
      error => {
        console.log(error);
      }
    );
  }

}
