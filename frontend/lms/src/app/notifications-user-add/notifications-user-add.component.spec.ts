import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsUserAddComponent } from './notifications-user-add.component';

describe('NotificationsUserAddComponent', () => {
  let component: NotificationsUserAddComponent;
  let fixture: ComponentFixture<NotificationsUserAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsUserAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsUserAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
