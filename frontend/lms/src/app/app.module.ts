import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { ProfilComponent } from './profil/profil.component';
import { ProfilNavigationComponent } from './profil-navigation/profil-navigation.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { SubjectComponent } from './subject/subject.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { CoursesComponent } from './courses/courses.component';
import { FacultiesComponent } from './faculties/faculties.component';
import { ExamsComponent } from './exams/exams.component';
import { ExamApplicationComponent } from './exam-application/exam-application.component';
import { ExamsPassedComponent } from './exams-passed/exams-passed.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UserComponent } from './user/user.component';
import { UsersComponent } from './users/users.component';
import { AdministrationComponent } from './administration/administration.component';
import { CourseComponent } from './course/course.component';
import { FacultyComponent } from './faculty/faculty.component';
import { SubjectsCurrentComponent } from './subjects-current/subjects-current.component';
import { AdministrativeStaffComponent } from './administrative-staff/administrative-staff.component';
import { StudentEnrollmentComponent } from './student-enrollment/student-enrollment.component';
import { AcademicCalendarComponent } from './academic-calendar/academic-calendar.component';
import { ExamsScheduleComponent } from './exams-schedule/exams-schedule.component';
import { TeachingScheduleComponent } from './teaching-schedule/teaching-schedule.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { ColloquiumComponent } from './colloquium/colloquium.component';
import { ColloquiumsComponent } from './colloquiums/colloquiums.component';
import { ColloquiumsScheduleComponent } from './colloquiums-schedule/colloquiums-schedule.component';
import { RectorInformationComponent } from './rector-information/rector-information.component';
import { FinalWorkComponent } from './final-work/final-work.component';
import { SubjectInformationComponent } from './subject-information/subject-information.component';
import { SyllabusComponent } from './syllabus/syllabus.component';
import { SyllabusUpdateComponent } from './syllabus-update/syllabus-update.component';
import { ExamsResultsComponent } from './exams-results/exams-results.component';
import { ColloquiumsResultsComponent } from './colloquiums-results/colloquiums-results.component';
import { SubjectNavigationComponent } from './subject-navigation/subject-navigation.component';
import { IssueComponent } from './issue/issue.component';
import { StudentListComponent } from './student-list/student-list.component';
import { ThemeComponent } from './theme/theme.component';
import { TestComponent } from './test/test.component';
import { ProjectComponent } from './project/project.component';
import { ThemesComponent } from './themes/themes.component';
import { AdministrativeStaffAddComponent } from './administrative-staff-add/administrative-staff-add.component';
import { NotificationsAddComponent } from './notifications-add/notifications-add.component';
import { TeachersAddComponent } from './teachers-add/teachers-add.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpConfigInterceptor } from './http-config-interceptor.service';
import { EntryExamComponent } from './entry-exam/entry-exam.component';
import { EntryExamApplicationComponent } from './entry-exam-application/entry-exam-application.component';
import { SubjectsTeacherComponent } from './subjects-teacher/subjects-teacher.component';
import { AdministrationRegisteredUsersComponent } from './administration-registered-users/administration-registered-users.component';
import { AdministrationOrganizationComponent } from './administration-organization/administration-organization.component';
import { AdministrationCoursesComponent } from './administration-courses/administration-courses.component';
import { AcademicCalendarUpdateComponent } from './academic-calendar-update/academic-calendar-update.component';
import { AdministrationCodebookComponent } from './administration-codebook/administration-codebook.component';
import { StudentComponent } from './student/student.component';
import { EditStudentComponent } from './edit-student/edit-student.component';
import { StudentsComponent } from './students/students.component';
import { EditTeacherComponent } from './edit-teacher/edit-teacher.component';
import { EditAdministrativeStaffComponent } from './edit-administrative-staff/edit-administrative-staff.component';
import { TeacherComponent } from './teacher/teacher.component';
import { TeachersComponent } from './teachers/teachers.component';
import { AdministrativeStaffsComponent } from './administrative-staffs/administrative-staffs.component';
import { EditCourseComponent } from './edit-course/edit-course.component';
import { UniversityComponent } from './university/university.component';
import { UniversitiesComponent } from './universities/universities.component';
import { NotificationsEditComponent } from './notifications-edit/notifications-edit.component';
import { ExamsScheduleEditComponent } from './exams-schedule-edit/exams-schedule-edit.component';
import { ColloquiumsScheduleEditComponent } from './colloquiums-schedule-edit/colloquiums-schedule-edit.component';
import { TeachingScheduleEditComponent } from './teaching-schedule-edit/teaching-schedule-edit.component';
import { NotificationsUniversityComponent } from './notifications-university/notifications-university.component';
import { NotificationUniversityComponent } from './notification-university/notification-university.component';
import { NotificationsUserAddComponent } from './notifications-user-add/notifications-user-add.component';
import { NotificationsUserEditComponent } from './notifications-user-edit/notifications-user-edit.component';
import { SubjectsAdministrationComponent } from './subjects-administration/subjects-administration.component';
import { ScheduleAddComponent } from './schedule-add/schedule-add.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegistrationComponent,
    ProfilComponent,
    ProfilNavigationComponent,
    NavigationComponent,
    FooterComponent,
    ContactComponent,
    AddUserComponent,
    EditUserComponent,
    SubjectComponent,
    SubjectsComponent,
    CoursesComponent,
    FacultiesComponent,
    ExamsComponent,
    ExamApplicationComponent,
    ExamsPassedComponent,
    NotificationsComponent,
    UserComponent,
    UsersComponent,
    AdministrationComponent,
    CourseComponent,
    FacultyComponent,
    SubjectsCurrentComponent,
    AdministrativeStaffComponent,
    StudentEnrollmentComponent,
    AcademicCalendarComponent,
    ExamsScheduleComponent,
    TeachingScheduleComponent,
    SchedulesComponent,
    ColloquiumComponent,
    ColloquiumsComponent,
    ColloquiumsScheduleComponent,
    RectorInformationComponent,
    FinalWorkComponent,
    SubjectInformationComponent,
    SyllabusComponent,
    SyllabusUpdateComponent,
    ExamsResultsComponent,
    ColloquiumsResultsComponent,
    SubjectNavigationComponent,
    IssueComponent,
    StudentListComponent,
    ThemeComponent,
    TestComponent,
    ProjectComponent,
    ThemesComponent,
    AdministrativeStaffAddComponent,
    NotificationsAddComponent,
    TeachersAddComponent,
    EntryExamComponent,
    EntryExamApplicationComponent,
    SubjectsTeacherComponent,
    AdministrationRegisteredUsersComponent,
    AdministrationOrganizationComponent,
    AdministrationCoursesComponent,
    AcademicCalendarUpdateComponent,
    AdministrationCodebookComponent,
    StudentComponent,
    EditStudentComponent,
    StudentsComponent,
    EditTeacherComponent,
    EditAdministrativeStaffComponent,
    TeacherComponent,
    TeachersComponent,
    AdministrativeStaffsComponent,
    EditCourseComponent,
    UniversityComponent,
    UniversitiesComponent,
    NotificationsEditComponent,
    ExamsScheduleEditComponent,
    ColloquiumsScheduleEditComponent,
    TeachingScheduleEditComponent,
    NotificationsUniversityComponent,
    NotificationUniversityComponent,
    NotificationsUserAddComponent,
    NotificationsUserEditComponent,
    SubjectsAdministrationComponent,
    ScheduleAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule

  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
