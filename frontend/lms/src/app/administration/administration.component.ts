import { Component, OnInit } from '@angular/core';
import { Student } from '../model/Student';
import { StudentService } from '../service/student.service';
import { LoginService } from '../service/login.service';
import { StudentYearService } from '../service/student-year.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Studentyear } from '../model/Studentyear';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {

  student: Student={id:null,active: true, email: null, password: null,jmbg:null,name:null,lastname:null,birthdate:null,address:null,studentyear:null,attendingsubjects:null};
  studentYear: Studentyear={id:null,indexnumber :null,dateofenrollment :null, students :null, student: null, studyyear: null }
  students: Student[]=[];

  constructor(private ss: StudentService, private ls: LoginService, private sy: StudentYearService) { }

  ngOnInit(): void {
    this.ss.getOne(this.ls.getPayload()["id"]).subscribe(result => this.student = result);
    this.sy.getOne(this.ls.getPayload()["id"]).subscribe(result => this.studentYear = result);
  }

}
