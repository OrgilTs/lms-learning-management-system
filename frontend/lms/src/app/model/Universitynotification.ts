import { University } from './University';

export interface Universitynotification {
    id:number;
    university :University;
    created: Date;
    title: string;
    message: string;
}
