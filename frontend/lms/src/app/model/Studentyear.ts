import { Student } from './Student';
import { Studyyear } from './Studyyear';

export interface Studentyear {     
     id:number;
     indexnumber :string;
     dateofenrollment :Date;
     students :Student[];
     student: Student;
     studyyear: Studyyear; 
}
