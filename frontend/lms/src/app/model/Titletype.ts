import { Title } from './Title';

export interface Titletype {   
     id:number;
     indexnumber :string;
     titles :Title[];  
}
