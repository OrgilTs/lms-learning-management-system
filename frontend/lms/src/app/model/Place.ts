import { Country } from './Country';
import { Address } from './Address';

export interface Place {   
     id:number;
     name :string;
     country :Country;
     adresses :Address[];    
}
