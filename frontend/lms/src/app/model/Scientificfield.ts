import { Title } from './Title';

export interface Scientificfield {
     id:number;
     name :string;
     titles :Title[];   
}
