import { Time } from '@angular/common';
import { Daytitle } from './DayTitle';
import { Subject } from './Subject';


export interface Schedule {
     id:number;
     start :Time;
     end :Time; 
     daytitle:Daytitle;
     subject:Subject;
}