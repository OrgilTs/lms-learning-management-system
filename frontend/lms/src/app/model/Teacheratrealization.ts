import { Classrealization } from './Classrealization';
import { Classtype } from './Classtype';

export interface Teacheratrealization {     
     id:number;   
     brojcasova :number;
     classrealizations :Classrealization[];
     classtype :Classtype;    
}
