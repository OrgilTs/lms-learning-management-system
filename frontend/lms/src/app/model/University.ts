import { Faculty } from './Faculty';
import { Address } from './Address';
import { User } from './User';
import { Teacher } from './Teacher';
import { Universitynotification } from './Universitynotification';

export interface University {
     id:number;
     name :string;
     description: string;
     phone: string;
     email: string;
     rector: Teacher;
     dateofestablishment :Date;
     address :Address;
     faculties :Faculty[];
     notifications: Universitynotification;
}
