import { University } from './University';
import { Address } from './Address';

export interface Administrativestaff {
     id:number;
     active: boolean;
     email: string;
     password: string;
     jmbg :string;
     name :string;
     lastname :string;
     university: University;
     address: Address;
}
