import { NumberValueAccessor } from '@angular/forms';

export interface User {
     id:number;
     active :boolean;
     email :string;
     password :string;
     notifications:[];
}
