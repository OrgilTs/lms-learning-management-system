import { Student } from './Student';
import { Major } from './Major';

export interface Entranceexam {
    id:number; 
	student: Student;
	requireddatetime: Date;
	accepted: boolean;
	major: Major;
}