import { Place } from './Place';
import { University } from './University';
import { Faculty } from './Faculty';

export interface Address {    
     id:number;
     street :string;
     number :string;
     place :Place;
     universities :University[];
     faculties :Faculty[];
}
