import { Studyyear } from './Studyyear';
import { Classrealization } from './Classrealization';
import { Result } from './Result';
import { Schedule } from './Schedule';

export interface Subject {
      
     id:number; 
     name :string;
     espb :number;
     required :boolean;
     numberoflectures :number;
     numberofpractices :number;
     otherFormsOfClass :number;
     researchwork :number;
     remainingclasses :number;
     studyyear :Studyyear;
     classrealizations :Classrealization[];
     results :Result[];
     schedules:Schedule[]; 
}
