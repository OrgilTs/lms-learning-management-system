import { User } from './User';

export interface Usernotification {
    id:number;
    created :Date;
    title :string; 
    message:string;
    user: User;
   
}