import { Studentyear } from './Studentyear';
import { Attendingsubject } from './Attendingsubject';
import { Address } from './Address';


export interface Student {   
     id:number;
     active: boolean;
     email: string;
     password: string;
     name :string;
     lastname :string;
     jmbg :string;
     birthdate :Date;
     address :Address;
     studentyear :Studentyear;
     attendingsubjects :Attendingsubject[];   
}
