import { Place } from './Place';

export interface Country { 
     id:number;  
     name :string;
     places :Place[];    
}
