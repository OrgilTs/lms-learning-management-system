import { Subject } from './Subject';
import { Major } from './Major';

export interface Studyyear {
     id:number;
     year :number;
     major :Major;
     subjects :Subject[];  
}
