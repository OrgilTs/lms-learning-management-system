import { Faculty } from './Faculty';
import { Major } from './Major';
import { Address } from './Address';
import { University } from './University';

export interface Teacher {  
     id:number;
     active: boolean;
     email: string;
     password: string;
     jmbg :string;
     name :string;
     lastname :string;
     biography :string;
     faculty :Faculty;
     major :Major; 
     address: Address;
     university: University;
}
