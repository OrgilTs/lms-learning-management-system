import { Subject } from './Subject';

export interface Result {
     id:number;
     description :string;
     subject :Subject; 
}
