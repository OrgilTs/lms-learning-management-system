import { Major } from './Major';
import { Scientificfield } from './Scientificfield';
import { Titletype } from './Titletype';

export interface Title {    
     id:number;
     datewhenchosed :Date;
     datewhenstopped :Date;
     majors :Major[];
     scientificfield :Scientificfield;
     titletype :Titletype;  
}
