import { Classrealization } from './Classrealization';
import { Student } from './Student';

export interface Attendingsubject {    
     id:number;    
     finalgrade :number;
     classrealization :Classrealization;
     student :Student; 
}
