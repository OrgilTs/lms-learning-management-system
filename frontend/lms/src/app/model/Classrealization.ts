import { Teacheratrealization } from './Teacheratrealization';
import { Attendingsubject } from './Attendingsubject';
import { Subject } from './Subject';

export interface Classrealization {
     id:number;
     subject :Subject;
     teacheratrealization :Teacheratrealization;
     attendingsubjects :Attendingsubject[]; 
}
