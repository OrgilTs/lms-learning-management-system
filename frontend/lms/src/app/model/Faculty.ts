import { University } from './University';
import { Teacher } from './Teacher';
import { Major } from './Major';
import { Address } from './Address';

export interface Faculty {
     id:number;
     name :string;
     university :University;
     phone: string;
     email: string;
     address :Address;
     description: string;
     deandata: Teacher;
     teachers :Teacher[];
     majors :Major[];  
}
