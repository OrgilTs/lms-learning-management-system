import { Faculty } from './Faculty';
import { Teacher } from './Teacher';
import { Studyyear } from './Studyyear';





export interface Major {
     id: number;
     name: string;
     faculty: Faculty;
     teachers: Teacher[];
     studyyears: Studyyear[];
}
