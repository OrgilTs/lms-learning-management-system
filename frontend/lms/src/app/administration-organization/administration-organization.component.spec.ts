import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationOrganizationComponent } from './administration-organization.component';

describe('AdministrationOrganizationComponent', () => {
  let component: AdministrationOrganizationComponent;
  let fixture: ComponentFixture<AdministrationOrganizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationOrganizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationOrganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
