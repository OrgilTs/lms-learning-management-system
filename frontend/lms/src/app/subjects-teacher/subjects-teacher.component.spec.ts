import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectsTeacherComponent } from './subjects-teacher.component';

describe('SubjectsTeacherComponent', () => {
  let component: SubjectsTeacherComponent;
  let fixture: ComponentFixture<SubjectsTeacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectsTeacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectsTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
