import { Component, OnInit } from '@angular/core';
import { University } from '../model/University';
import { UniversityService } from '../service/university.service';
import { Universitynotification } from '../model/Universitynotification';
import { UniversityNotificationService } from '../service/university-notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications-add',
  templateUrl: './notifications-add.component.html',
  styleUrls: ['./notifications-add.component.css']
})
export class NotificationsAddComponent implements OnInit {

  universities: University[]=[];
  universityNotification: Universitynotification = {id:null, university :{ id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null}, created: null, title: null, message: null}

  constructor(private us: UniversityService, private uns: UniversityNotificationService, private router: Router) { }

  ngOnInit(): void {
    this.us.getAll("").subscribe(response => this.universities = response["content"]);
  }

  submit() {
    this.uns.add(this.universityNotification).subscribe(
      result => {
        this.router.navigate(["universities"]);
      },
      error => {
        console.log(error);
      }
    );
  }

}
