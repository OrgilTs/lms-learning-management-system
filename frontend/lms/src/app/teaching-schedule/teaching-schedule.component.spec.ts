import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachingScheduleComponent } from './teaching-schedule.component';

describe('TeachingScheduleComponent', () => {
  let component: TeachingScheduleComponent;
  let fixture: ComponentFixture<TeachingScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachingScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachingScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
