import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrativeStaffAddComponent } from './administrative-staff-add.component';

describe('AdministrativeStaffAddComponent', () => {
  let component: AdministrativeStaffAddComponent;
  let fixture: ComponentFixture<AdministrativeStaffAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrativeStaffAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrativeStaffAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
