import { Component, OnInit } from '@angular/core';
import { Faculty } from '../model/Faculty';
import { University } from '../model/University';
import { Administrativestaff } from '../model/Administrativestaff';
import { UniversityService } from '../service/university.service';
import { FacultyService } from '../service/faculty.service';
import { AdministrativeStaffService } from '../service/administrative-staff.service';
import { LoginService } from '../service/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Major } from '../model/Major';

@Component({
  selector: 'app-administrative-staff-add',
  templateUrl: './administrative-staff-add.component.html',
  styleUrls: ['./administrative-staff-add.component.css']
})
export class AdministrativeStaffAddComponent implements OnInit {
  selectedUniversity = 0;
  
  universities: University[]=[];
  administrativeStaff: Administrativestaff={ id: null, address: null, active: true, email: null, university:{id: null, name:null, address: null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null}, password: null, jmbg:null, name: null, lastname: null};
  
  constructor(private us: UniversityService, private as: AdministrativeStaffService, private ls: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.us.getAll("").subscribe(response => this.universities = response["content"]);
  }

  submit() {
    console.log(this.administrativeStaff)
    this.as.add(this.administrativeStaff).subscribe(
      result => {
        this.router.navigate(["administrativestaffs"]);
      },
      error => {
        console.log(error);
      }
    );
  }

}
