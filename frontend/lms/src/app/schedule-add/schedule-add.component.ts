import { Component, OnInit } from '@angular/core';
import { ScheduleService } from '../service/schedule.service';
import { Schedule } from '../model/Schedule';
import { Router } from '@angular/router';
import { SubjectService } from '../service/subject.service';
import { Subject } from '../model/Subject';
import { Daytitle } from '../model/DayTitle';
import { DaytitleService } from '../service/daytitle.service';

@Component({
  selector: 'app-schedule-add',
  templateUrl: './schedule-add.component.html',
  styleUrls: ['./schedule-add.component.css']
})
export class ScheduleAddComponent implements OnInit {

  schedule:Schedule={id:null,start:null,end:null,daytitle:{id:null,name:null},subject:{id:null,name:null,espb:null,required:true,numberoflectures:null,numberofpractices:null,otherFormsOfClass:1,researchwork:1,remainingclasses:40,studyyear:{ id: null, year: null, major: null, subjects:null},classrealizations:null,results:null,schedules:null}};
  schedules:Schedule[];
  subjects:Subject[];
  daytitles:Daytitle[];

  constructor(private ss:ScheduleService,private router: Router,private sus:SubjectService,private ds:DaytitleService) { }

  ngOnInit(): void {
    this.getAllSchedules();
    this.getAllSubjects();
    this.getAllDaytitles();
  }
  getAllSchedules(){
    this.ss.getAll("").subscribe(response => this.schedules = response["content"]);
  }
  getAllSubjects(){
    this.sus.getAll("").subscribe(response => this.subjects = response["content"]);
  }
  getAllDaytitles(){
    this.ds.getAll("").subscribe(response => this.daytitles = response["content"]);
  }
  submit() {
    this.ss.add(this.schedule).subscribe(
      result => {
        this.router.navigate(["administracija-satnica"]);
      },
      error => {
        console.log(error);
        
      }
    );
  }

}
