import { Component, OnInit } from '@angular/core';
import { AdministrativeStaffService } from '../service/administrative-staff.service';
import { Administrativestaff } from '../model/Administrativestaff';

@Component({
  selector: 'app-administrative-staffs',
  templateUrl: './administrative-staffs.component.html',
  styleUrls: ['./administrative-staffs.component.css']
})
export class AdministrativeStaffsComponent implements OnInit {


  administrativeStaff: Administrativestaff = { id: null, address: null, active: null, email: null, university: null, password: null, jmbg: null, name: null, lastname: null };
  administrativeStaffs: Administrativestaff[] = [];

  constructor(private as: AdministrativeStaffService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.as.getAll("").subscribe(response => this.administrativeStaffs = response["content"]);
  }

  submit() {
    this.as.add(this.administrativeStaff).subscribe(() => this.getAll());
  }

}
