import { Component, OnInit } from '@angular/core';
import { FacultyService } from '../service/faculty.service';
import { Faculty } from '../model/Faculty';


@Component({
  selector: 'app-faculties',
  templateUrl: './faculties.component.html',
  styleUrls: ['./faculties.component.css']
})
export class FacultiesComponent implements OnInit {

  faculty: Faculty={ id: null, name:null, university:null, address:null, phone:null, email:null, description:null, deandata:null, teachers:null,majors:null};
 
  faculties: Faculty[]=[];
  
  constructor(private fs: FacultyService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.fs.getAll("").subscribe(response => this.faculties = response["content"]);
  }

  submit() {
    this.fs.add(this.faculty).subscribe(() => this.getAll());
  }

}
