import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '../service/registration.service';
import { Router } from '@angular/router';
import { Student } from '../model/Student';
import { User } from '../model/User';
import { StudentService } from '../service/student.service';
import { University } from '../model/University';
import { Administrativestaff } from '../model/Administrativestaff';
import { UniversityService } from '../service/university.service';
import { FacultyService } from '../service/faculty.service';
import { Faculty } from '../model/Faculty';
import { Major } from '../model/Major';
import { MajorService } from '../service/major.service';
import { Studentyear } from '../model/Studentyear';
import { StudentYearService } from '../service/student-year.service';
import { Studyyear } from '../model/Studyyear';
import { StudyyearService } from '../service/studyyear.service';

// moze se zvati i student service,mozda je bolje

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  student: Student = {
    id: null, active: false, email: null, password: null, jmbg: null, name: null, lastname: null, birthdate: null, address: {
      id: 3, street: null,
      number: null, place: null, universities: null, faculties: null
    }, studentyear: null, attendingsubjects: null
  };


  constructor(private ss: StudentService, private router: Router) { }

  ngOnInit(): void {
  }

  submit() {
    this.ss.registration(this.student).subscribe(() => this.router.navigate(["/prijava"]));
  }

}


