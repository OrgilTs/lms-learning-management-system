import { Component, OnInit } from '@angular/core';

import { Major } from '../model/Major';
import { MajorService } from '../service/major.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  // course: Course={ id: null, name: null, description:null, leaderdata:null};
  // courses: Course[]=[];
  
  major:Major={"id":null,"name":null,"faculty":null,"teachers":null,"studyyears":null};
  majors:Major[];
  constructor(private ms: MajorService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.ms.getAll("").subscribe(response => this.majors = response["content"]);
  }

  submit() {
    this.ms.add(this.major).subscribe(() => this.getAll());
  }

}
