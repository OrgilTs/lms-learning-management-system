import { Component, OnInit } from '@angular/core';
import { UniversityService } from '../service/university.service';
import { UniversityNotificationService } from '../service/university-notification.service';
import { Router } from '@angular/router';
import { Universitynotification } from '../model/Universitynotification';
import { University } from '../model/University';

@Component({
  selector: 'app-notifications-edit',
  templateUrl: './notifications-edit.component.html',
  styleUrls: ['./notifications-edit.component.css']
})
export class NotificationsEditComponent implements OnInit {

  selectedUniversity = 0;
  university: University={ id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null};
  universities: University[]=[];
  universityNotification: Universitynotification = {id:null, university :null, created: null, title: null, message: null}
  universityNotifications: Universitynotification[]=[];

  constructor(private us: UniversityService, private uns: UniversityNotificationService, private router: Router) { }

  ngOnInit(): void {
    this.us.getAll("").subscribe(response => this.universities = response["content"]);
    this.uns.getAll("").subscribe(response => this.universityNotifications = response["content"]);
  }

  submit() {
    this.uns.edit(this.universityNotification.id, this.universityNotification).subscribe(() => this.router.navigate(["/obavestenjau", "update", this.universityNotification.id]));
  }

  onUniversityChange(){
    this.university= { id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null};
    if (this.selectedUniversity > 0) {
      this.us.getOne(this.selectedUniversity).subscribe(response => this.university = response);
    }
    }

}
