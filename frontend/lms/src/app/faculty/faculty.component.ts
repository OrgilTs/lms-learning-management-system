import { Component, OnInit } from '@angular/core';
import { FacultyService } from '../service/faculty.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Faculty } from '../model/Faculty';
import { LoginService } from '../service/login.service';
import { University } from '../model/University';
import { UniversityService } from '../service/university.service';

@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.css']
})
export class FacultyComponent implements OnInit {

  role;
  faculty: Faculty={ id: null, name:null, university:null, address:null, phone:null, email:null, description:null, deandata:null, teachers:null,majors:null};
  faculties: Faculty[]=[];
  university: University={notifications:null,id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null};
  universities: University[];
  constructor(private fs: FacultyService, private us: UniversityService, private activatedRoute: ActivatedRoute,  private ls:LoginService, private router: Router) { }

  ngOnInit(): void {
    this.fs.getOne(this.activatedRoute.snapshot.params['id']).subscribe(faculty => this.faculty = faculty);
    this.us.getOne(this.activatedRoute.snapshot.params['id']).subscribe(university => this.university = university);
    this.role = this.ls.getRole();
  }

  delete() {
    this.fs.delete(this.faculty.id).subscribe(()=>this.router.navigate(["/faculties"]))
  }

}
