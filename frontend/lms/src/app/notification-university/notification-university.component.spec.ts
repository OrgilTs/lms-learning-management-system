import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationUniversityComponent } from './notification-university.component';

describe('NotificationUniversityComponent', () => {
  let component: NotificationUniversityComponent;
  let fixture: ComponentFixture<NotificationUniversityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationUniversityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationUniversityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
