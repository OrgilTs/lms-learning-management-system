import { Component, OnInit } from '@angular/core';
import { Universitynotification } from '../model/Universitynotification';
import { UniversityNotificationService } from '../service/university-notification.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-notification-university',
  templateUrl: './notification-university.component.html',
  styleUrls: ['./notification-university.component.css']
})
export class NotificationUniversityComponent implements OnInit {

  universityNotification: Universitynotification = {id:null, university :null, created: null, title: null, message: null}
  universityNotifications: Universitynotification[]=[];

  constructor(private uns: UniversityNotificationService, private activatedRoute: ActivatedRoute, private router: Router) { }


  ngOnInit(): void {
    this.uns.getOne(this.activatedRoute.snapshot.params['id']).subscribe(universityNotification => this.universityNotification = universityNotification);
  }

  delete() {
    this.uns.delete(this.universityNotification.id).subscribe(()=>this.router.navigate(["/obavestenjau"]))
  }

}
