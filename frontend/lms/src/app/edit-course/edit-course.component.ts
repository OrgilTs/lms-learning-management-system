import { Component, OnInit } from '@angular/core';
import { Course } from '../model/course';
import { CourseService } from '../service/course.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.css']
})
export class EditCourseComponent implements OnInit {

  course: Course={ id: null, name: null, description: null,leaderdata:null};

  constructor(private cs: CourseService, private activatedRoute: ActivatedRoute,  private router: Router) { }

  ngOnInit(): void {
    this.cs.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(i => this.course = i);
  }

  submit() {
    this.cs.edit(this.course.id, this.course).subscribe(()=> this.router.navigate(["/course", this.course.id]));
  }


}
