import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColloquiumComponent } from './colloquium.component';

describe('ColloquiumComponent', () => {
  let component: ColloquiumComponent;
  let fixture: ComponentFixture<ColloquiumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColloquiumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColloquiumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
