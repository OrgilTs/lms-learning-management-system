import { Component, OnInit } from '@angular/core';
import { University } from '../model/University';
import { ActivatedRoute, Router } from '@angular/router';
import { UniversityService } from '../service/university.service';

@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.css']
})
export class UniversityComponent implements OnInit {

  university: University={ id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null};
  
  universities: University[];

  constructor(private us: UniversityService, private activatedRoute: ActivatedRoute,  private router: Router) { }

  ngOnInit(): void {
    this.us.getOne(this.activatedRoute.snapshot.params['id']).subscribe(university => this.university = university);
  }

  delete() {
    this.us.delete(this.university.id).subscribe(()=>this.router.navigate(["/universities"]))
  }


}
