import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColloquiumsScheduleComponent } from './colloquiums-schedule.component';

describe('ColloquiumsScheduleComponent', () => {
  let component: ColloquiumsScheduleComponent;
  let fixture: ComponentFixture<ColloquiumsScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColloquiumsScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColloquiumsScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
