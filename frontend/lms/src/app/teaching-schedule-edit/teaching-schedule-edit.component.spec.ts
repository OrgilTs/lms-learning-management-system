import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachingScheduleEditComponent } from './teaching-schedule-edit.component';

describe('TeachingScheduleEditComponent', () => {
  let component: TeachingScheduleEditComponent;
  let fixture: ComponentFixture<TeachingScheduleEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachingScheduleEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachingScheduleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
