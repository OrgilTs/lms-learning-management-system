import { Component, OnInit } from '@angular/core';
import { Faculty } from '../model/Faculty';
import { University } from '../model/University';
import { Administrativestaff } from '../model/Administrativestaff';
import { Teacher } from '../model/Teacher';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { AdministrativeStaffService } from '../service/administrative-staff.service';
import { FacultyService } from '../service/faculty.service';
import { TeacherService } from '../service/teacher.service';
import { UniversityService } from '../service/university.service';
import { MajorService } from '../service/major.service';
import { Major } from '../model/Major';

@Component({
  selector: 'app-teachers-add',
  templateUrl: './teachers-add.component.html',
  styleUrls: ['./teachers-add.component.css']
})
export class TeachersAddComponent implements OnInit {

  selectedFaculty = 0;
  selectedMajor = 0;
  major:Major={id:null,name:null,faculty:null,teachers:null,studyyears:null};
  majors: Major[]=[];
  faculty: Faculty={id: null, name:null, university:null, address:null, phone:null, email:null, description:null, deandata:null, teachers:null,majors:null};
  faculties: Faculty[]=[];
  university: University={ id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null};
  universities: University[]=[];
  administrativeStaff: Administrativestaff={ id: null, address: null, active: null, email: null, university:null, password: null, jmbg:null, name: null, lastname: null};
  teacher: Teacher={ id: null,active: true,university:{id: null, name:null, address: null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null}, address: null, email: null, password: null, jmbg:null, name: null, lastname: null, biography:null,faculty: null,major:null};
  teachers: Teacher[]=[];
  
  constructor(private ms: MajorService, private ts: TeacherService, private us: UniversityService, private fs: FacultyService, private as: AdministrativeStaffService, private ls: LoginService, private router:Router) { }

  ngOnInit(): void {
    this.us.getAll("").subscribe(response => this.universities = response["content"]);
  }
 
  submit() {
    this.ts.add(this.teacher).subscribe(
      result => {
        this.router.navigate(["teachers"]);
      },
      error => {
        console.log(error);
      }
    );
  }

}
