import { Component, OnInit } from '@angular/core';
import { Major } from '../model/Major';
import { Studyyear } from '../model/Studyyear';
import { MajorService } from '../service/major.service';
import { StudyyearService } from '../service/studyyear.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubjectService } from '../service/subject.service';
import { Subject } from '../model/Subject';


@Component({
  selector: 'app-schedules',
  templateUrl: './schedules.component.html',
  styleUrls: ['./schedules.component.css']
})
export class SchedulesComponent implements OnInit {
  subjects: Subject[] = [];
  majors: Major[];
  major: Major = { id: null, name: null, faculty: { id: null, name: null, university: null, phone: null, email: null, address: null, description: null, deandata: null, teachers: null, majors: null }, teachers: null, studyyears: null };
  studyyear: Studyyear = { id: null, year: null, major: null, subjects: null };
  constructor(private ms: MajorService, private activatedRoute: ActivatedRoute, private router: Router, private sy: StudyyearService, private ss: SubjectService) { }

  ngOnInit(): void {
    this.ms.getAll("").subscribe(
      result => {
        this.majors = result["content"];
        if (this.majors.length > 0) {
          this.getMajor(this.majors[0].id);
        }
      },
      error => {
        console.log(error);
      }
    );

  }


  getMajor(id) {
    this.subjects = [];
    this.ms.getOne(id).subscribe(
      result => {
        this.major = result;
        for (let i = 0; i < this.major.studyyears.length; i++) {
          this.sy.getOne(this.major.studyyears[i].id).subscribe(
            result => {
              if (result.subjects != null) {
                for (let m = 0; m < result.subjects.length; m++) {
                  this.ss.getOne(result.subjects[m].id).subscribe(res => this.subjects.push(res))
                }
              }
            },
            error => {
              console.log(error);
            }
          );
        }
      },
      error => {
        console.log(error);
      }
    );
  }


}
