import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColloquiumsComponent } from './colloquiums.component';

describe('ColloquiumsComponent', () => {
  let component: ColloquiumsComponent;
  let fixture: ComponentFixture<ColloquiumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColloquiumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColloquiumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
