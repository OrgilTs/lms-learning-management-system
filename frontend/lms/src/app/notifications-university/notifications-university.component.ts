import { Component, OnInit } from '@angular/core';
import { Administrativestaff } from '../model/Administrativestaff';
import { UniversityService } from '../service/university.service';
import { UniversityNotificationService } from '../service/university-notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Universitynotification } from '../model/Universitynotification';
import { University } from '../model/University';

@Component({
  selector: 'app-notifications-university',
  templateUrl: './notifications-university.component.html',
  styleUrls: ['./notifications-university.component.css']
})
export class NotificationsUniversityComponent implements OnInit {

  universityNotifications: Universitynotification[]=[];
  university: University= { id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null};
  

  constructor(private uns: UniversityNotificationService, private us: UniversityService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.us.getOne(1).subscribe(university => this.university = university);
    this.getAll();
  }

  getAll() {
    this.uns.getAll("").subscribe(response => this.universityNotifications = response["content"]);
  }


}
