import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsUniversityComponent } from './notifications-university.component';

describe('NotificationsUniversityComponent', () => {
  let component: NotificationsUniversityComponent;
  let fixture: ComponentFixture<NotificationsUniversityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsUniversityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsUniversityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
