import { Component, OnInit } from '@angular/core';
import { StudentService } from '../service/student.service';
import { Student } from '../model/Student';
import { Studentyear } from '../model/Studentyear';
import { Router } from '@angular/router';
import { StudentYearService } from '../service/student-year.service';
import { LoginService } from '../service/login.service';
import { Administrativestaff } from '../model/Administrativestaff';
import { AdministrativeStaffService } from '../service/administrative-staff.service';
import { UniversityService } from '../service/university.service';
import { University } from '../model/University';
import { FacultyService } from '../service/faculty.service';
import { Faculty } from '../model/Faculty';
import { MajorService } from '../service/major.service';
import { Major } from '../model/Major';

@Component({
  selector: 'app-student-enrollment',
  templateUrl: './student-enrollment.component.html',
  styleUrls: ['./student-enrollment.component.css']
})
export class StudentEnrollmentComponent implements OnInit {

  selectedFaculty = 0;
  selectedMajor = null;
  major: Major = { id: null, name: null, faculty: null, teachers: null, studyyears: null };
  faculty: Faculty={id: null, name:null, university:null, address:null, phone:null, email:null, description:null, deandata:null, teachers:null,majors:null};
  university: University={ notifications:null, id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null};
  administrativeStaff: Administrativestaff={ id: null,address: null, active: null, email: null, university:null, password: null, jmbg:null, name: null, lastname: null};
  student: Student={id:null,active: true, email: null, password: null,jmbg:null,name:null,lastname:null,birthdate:null,address:null,studentyear:null,attendingsubjects:null};
  studentYear: Studentyear = { id: null, indexnumber: null, dateofenrollment: null, students: null, student:null, studyyear:null };
  studentYears: Studentyear[]=[];
  constructor(private ms: MajorService, private us: UniversityService, private fs: FacultyService, private ss: StudentService, private as: AdministrativeStaffService, private sy: StudentYearService, private ls: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.sy.getAll("").subscribe(response => this.studentYears = response["content"]);
    this.as.getByEmail(this.ls.getPayload()["sub"]).subscribe(
      result => {
        this.administrativeStaff = result;
        this.us.getOne(this.administrativeStaff.university.id).subscribe(response => this.university = response);
      },
      error => {
        console.log(error);
      }
    );
  }

  submit() {
    this.ss.add(this.student).subscribe(

      result => {
        this.studentYear.student = result;
        this.sy.add(this.studentYear).subscribe(
          result => {
            this.router.navigate(["students"]);
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );
  }

  onFacultyChange() {
    this.selectedMajor = null;
    this.studentYear.studyyear = null;
    this.faculty = { id: null, name: null, university: null, address: null, phone: null, email: null, description: null, deandata: null, teachers: null, majors: null };
    if (this.selectedFaculty > 0) {
      this.fs.getOne(this.selectedFaculty).subscribe(response => this.faculty = response);
    }
  }

  onMajorChange() {
    this.studentYear.studyyear = null;
    this.major = { id: null, name: null, faculty: null, teachers: null, studyyears: null };
    console.log("rest");
    if (this.selectedMajor > 0) {
      this.ms.getOne(this.selectedMajor).subscribe(response => this.major = response);
    }
  }

}
