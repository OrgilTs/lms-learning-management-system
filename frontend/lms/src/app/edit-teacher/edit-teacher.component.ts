import { Component, OnInit } from '@angular/core';
import { TeacherService } from '../service/teacher.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Teacher } from '../model/Teacher';

@Component({
  selector: 'app-edit-teacher',
  templateUrl: './edit-teacher.component.html',
  styleUrls: ['./edit-teacher.component.css']
})
export class EditTeacherComponent implements OnInit {
  
  teacher: Teacher={ id: null,active: null, university:null,address: null, email: null, password: null, jmbg:null, name: null, lastname: null, biography:null,faculty: null,major:null};

  constructor(private ts: TeacherService, private activatedRoute: ActivatedRoute,  private router: Router) { }

  ngOnInit(): void {
    this.ts.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(i => this.teacher = i);
  }

  submit() {
    // this.ts.edit(this.teacher.id, this.teacher).subscribe(()=> this.router.navigate(["/teacher", this.teacher.id]));
    this.ts.edit(this.teacher.id, this.teacher).subscribe(() => this.router.navigate(["/teachers", "update", this.teacher.id]));
  }

}
