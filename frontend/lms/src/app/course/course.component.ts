import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Studyyear } from '../model/Studyyear';
import { StudyyearService } from '../service/studyyear.service';
import { Major } from '../model/Major';
import { MajorService } from '../service/major.service';
import { LoginService } from '../service/login.service';


@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  role;
  major: Major = { id: null, name: null, faculty: { id: null, name: null, university: null, phone: null, email: null, address: null, description: null, deandata: null, teachers: null, majors: null }, teachers: null, studyyears: null };
  studyyear: Studyyear = { id: null, year: null, major: null, subjects: null };
  constructor(private ms: MajorService, private activatedRoute: ActivatedRoute, private router: Router, private sy: StudyyearService, private ls:LoginService) { }

  ngOnInit(): void {
    this.ms.getOne(this.activatedRoute.snapshot.params['id']).subscribe(
      major => { 
        this.major = major;
        if(this.major.studyyears.length>0) {
          this.getStudyYear(this.major.studyyears[0].id);
        }
      }
    );
    this.role = this.ls.getRole();
  }

  getStudyYear(id) {
    this.sy.getOne(id).subscribe(result => this.studyyear = result);
  }

  delete() {
    this.ms.delete(this.major.id).subscribe(() => this.router.navigate(["/courses"]))
  }

}
