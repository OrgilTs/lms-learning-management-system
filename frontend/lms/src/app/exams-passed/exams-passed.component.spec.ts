import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPassedComponent } from './exams-passed.component';

describe('ExamsPassedComponent', () => {
  let component: ExamsPassedComponent;
  let fixture: ComponentFixture<ExamsPassedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPassedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPassedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
