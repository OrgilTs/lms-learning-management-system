import { Component, OnInit } from '@angular/core';
import { StudentService } from '../service/student.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Student } from '../model/Student';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {

  student: Student={id:null,active: true, email: null, password: null,jmbg:null,name:null,lastname:null,birthdate:null,address:null,studentyear:null,attendingsubjects:null};

  constructor(private ss: StudentService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.ss.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(i => this.student = i);
  }

  submit() {
    this.ss.edit(this.student.id, this.student).subscribe(() => this.router.navigate(["/students", 'update', this.student.id]));
  }


}
