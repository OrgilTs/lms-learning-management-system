import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationCoursesComponent } from './administration-courses.component';

describe('AdministrationCoursesComponent', () => {
  let component: AdministrationCoursesComponent;
  let fixture: ComponentFixture<AdministrationCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
