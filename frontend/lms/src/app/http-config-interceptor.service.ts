import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { LoginService } from './service/login.service';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

  private token: string = null;


  constructor(private loginService: LoginService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.token = this.loginService.getToken();
    if (this.token) {
      req = req.clone({
        headers: req.headers.set(TOKEN_HEADER_KEY, this.token)
      });
    }

    return next.handle(req);
  }

}
