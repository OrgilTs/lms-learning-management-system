import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { ProfilComponent } from './profil/profil.component';
import { CoursesComponent } from './courses/courses.component';
import { ContactComponent } from './contact/contact.component';
import { FacultiesComponent } from './faculties/faculties.component';
import { ExamApplicationComponent } from './exam-application/exam-application.component';
import { ExamsComponent } from './exams/exams.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { AdministrationComponent } from './administration/administration.component';
import { ExamsPassedComponent } from './exams-passed/exams-passed.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { SubjectComponent } from './subject/subject.component';
import { CourseComponent } from './course/course.component';
import { FacultyComponent } from './faculty/faculty.component';
import { SubjectsCurrentComponent } from './subjects-current/subjects-current.component';
import { ExamsScheduleComponent } from './exams-schedule/exams-schedule.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { TeachingScheduleComponent } from './teaching-schedule/teaching-schedule.component';
import { AdministrativeStaffComponent } from './administrative-staff/administrative-staff.component';
import { StudentEnrollmentComponent } from './student-enrollment/student-enrollment.component';
import { AcademicCalendarComponent } from './academic-calendar/academic-calendar.component';
import { ColloquiumsScheduleComponent } from './colloquiums-schedule/colloquiums-schedule.component';
import { RectorInformationComponent } from './rector-information/rector-information.component';
import { FinalWorkComponent } from './final-work/final-work.component';
import { SubjectInformationComponent } from './subject-information/subject-information.component';
import { SyllabusComponent } from './syllabus/syllabus.component';
import { ExamsResultsComponent } from './exams-results/exams-results.component';
import { ThemesComponent } from './themes/themes.component';
import { ThemeComponent } from './theme/theme.component';
import { SyllabusUpdateComponent } from './syllabus-update/syllabus-update.component';
import { AdministrativeStaffAddComponent } from './administrative-staff-add/administrative-staff-add.component';
import { NotificationsAddComponent } from './notifications-add/notifications-add.component';
import { TeachersAddComponent } from './teachers-add/teachers-add.component';
import { AuthRouteService } from './service/auth-route.service';
import { EntryExamComponent } from './entry-exam/entry-exam.component';
import { EntryExamApplicationComponent } from './entry-exam-application/entry-exam-application.component';
import { SubjectsTeacherComponent } from './subjects-teacher/subjects-teacher.component';
import { StudentListComponent } from './student-list/student-list.component';
import { AdministrationOrganizationComponent } from './administration-organization/administration-organization.component';
import { AdministrationCoursesComponent } from './administration-courses/administration-courses.component';
import { AdministrationRegisteredUsersComponent } from './administration-registered-users/administration-registered-users.component';
import { AcademicCalendarUpdateComponent } from './academic-calendar-update/academic-calendar-update.component';
import { AdministrationCodebookComponent } from './administration-codebook/administration-codebook.component';
import { StudentComponent } from './student/student.component';
import { EditStudentComponent } from './edit-student/edit-student.component';
import { StudentsComponent } from './students/students.component';
import { EditAdministrativeStaffComponent } from './edit-administrative-staff/edit-administrative-staff.component';
import { AdministrativeStaffsComponent } from './administrative-staffs/administrative-staffs.component';
import { EditTeacherComponent } from './edit-teacher/edit-teacher.component';
import { TeachersComponent } from './teachers/teachers.component';
import { TeacherComponent } from './teacher/teacher.component';
import { EditCourseComponent } from './edit-course/edit-course.component';
import { UniversitiesComponent } from './universities/universities.component';
import { UniversityComponent } from './university/university.component';
import { NotificationsEditComponent } from './notifications-edit/notifications-edit.component';
import { NotificationsUniversityComponent } from './notifications-university/notifications-university.component';
import { NotificationUniversityComponent } from './notification-university/notification-university.component';
import { SubjectsAdministrationComponent } from './subjects-administration/subjects-administration.component';
import { NotificationsUserAddComponent } from './notifications-user-add/notifications-user-add.component';
import { NotificationsUserEditComponent } from './notifications-user-edit/notifications-user-edit.component';
import { ScheduleAddComponent } from './schedule-add/schedule-add.component';

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "faculties", component: FacultiesComponent},
  {path: "faculties/:id", component: FacultyComponent},
  {path: "universities", component: UniversitiesComponent},
  {path: "universities/:id", component: UniversityComponent},
  {path: "prijava", component: LoginComponent
},
  {path: "registracija", component: RegistrationComponent},
  {path: "akademski-kalendar", component: AcademicCalendarComponent},
  {path: "akademski-kalendar-azuriranje", component: AcademicCalendarUpdateComponent,
  data: {
    allowedRoles: ["administrative-staff"]
  },
  canActivate: [AuthRouteService]
},
  {path: "rasporedi", component: SchedulesComponent},
  {path: "raspored-ispita", component: ExamsScheduleComponent
},
  {path: "raspored-nastave", component: SchedulesComponent
},
  {path: "raspored-kolokvijuma", component: ColloquiumsScheduleComponent,
  data: {
    allowedRoles: ["administrative-staff"]
  },
  canActivate: [AuthRouteService]
},
  {path: "profil", component: ProfilComponent
}, 
  {path: "korisnici", component: UsersComponent,

},
  {path: "korisnici/:id", component: UserComponent},
  {path: "korisnici-dodavanje", component: AddUserComponent
},
  {path: "nastavnici-dodavanje", component: TeachersAddComponent,
},
  {path: "studenti-dodavanje", component: StudentEnrollmentComponent
},
  {path: "korisnici-izmena", component: EditUserComponent,
  data: {
    allowedRoles: ["administrator"]
  },
  canActivate: [AuthRouteService]
},
  {path: "obavestenja", component: NotificationsComponent},
  {path: "obavestenja-dodavanje", component: NotificationsUserAddComponent},
  {path: "obavestenja/:id/update", component: NotificationsUserEditComponent},
  {path: "obavestenjau", component: NotificationsUniversityComponent},
  {path: "obavestenjau-dodavanje", component: NotificationsAddComponent},
  {path: "obavestenjau/:id", component: NotificationUniversityComponent},
  {path: "obavestenjau/:id/update", component: NotificationsEditComponent},
  {path: "prijava-ispita", component: ExamApplicationComponent
}, {path: "prijemni-ispit", component: EntryExamComponent
},
  {path: "prijemni-ispit-prijava", component: EntryExamApplicationComponent
},
  {path: "ispiti", component: ExamsComponent},
  {path: "ispiti-prijava", component: ExamApplicationComponent},
  {path: "ispiti-polozeni", component: ExamsPassedComponent}, 
  {path: "ispiti-rezultati", component: ExamsResultsComponent},
  {path: "predmeti", component: SubjectsComponent},
  {path: "predmeti/:id", component: SubjectComponent},
  {path: "predmeti-nastavnik", component: SubjectsTeacherComponent},
  {path: "spisak-studenata", component: StudentListComponent},
  {path: "predmeti-trenutni", component: SubjectsCurrentComponent},
  {path: "predmeti/:id/info", component: SubjectInformationComponent},
  {path: "silabus", component: SyllabusComponent}, 
  {path: "silabus-update", component: SyllabusUpdateComponent,
},
  {path: "zavrsni-rad", component: FinalWorkComponent},
  {path: "teme", component: ThemesComponent},
  {path: "tema", component: ThemeComponent},
  {path: "students", component: StudentsComponent},
  {path: "students/:id", component: StudentComponent},
  {path: "students/update/:id", component: EditStudentComponent},
  {path: "administracija", component: AdministrationComponent},
  {path: "administracija-sifarnika", component: AdministrationCodebookComponent,
  data: {
    allowedRoles: ["administrator"]
  },
  canActivate: [AuthRouteService]
},
  {path: "administracija-registrovanih-korisnika", component: AdministrationRegisteredUsersComponent
},
  {path: "administracija-kurseva", component: AdministrationCoursesComponent,
  data: {
    allowedRoles: ["administrator"]
  },
  canActivate: [AuthRouteService]
},
  {path: "administracija-organizacije", component: AdministrationOrganizationComponent,
  data: {
    allowedRoles: ["administrator"]
  },
  canActivate: [AuthRouteService]
},
{path: "administracija-predmeta", component: SubjectsAdministrationComponent
},
{path: "administracija-satnica", component: ScheduleAddComponent
},

  {path: "courses", component: CoursesComponent},
  {path: "courses/:id", component: CourseComponent},
  {path: "courses/update/:id", component: EditCourseComponent},
  {path: "rektor-podaci", component: RectorInformationComponent},
  {path: "kontakt", component: ContactComponent},
  {path: "administrativestaffs", component: AdministrativeStaffsComponent},
  {path: "administrativestaffs/:id", component: AdministrativeStaffComponent},
  {path: "administrativestaffs/update/:id", component: EditAdministrativeStaffComponent},
  {path: "administrativno-osoblje-dodavanje", component: AdministrativeStaffAddComponent
},
  {path: "teachers", component: TeachersComponent},
  {path: "teachers/:id", component: TeacherComponent},
  {path: "teachers/update/:id", component: EditTeacherComponent},
  {path: "upis-studenta", component: StudentEnrollmentComponent
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
