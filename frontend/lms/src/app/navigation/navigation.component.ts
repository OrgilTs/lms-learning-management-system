import { Component, OnInit } from '@angular/core';
import { User } from '../model/User';
import { LoginService } from '../service/login.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  email:string = "";
  constructor(private ls:LoginService) { }

  ngOnInit(): void {
    this.ls.loginEvent.subscribe(res => this.email=res)
    if(this.ls.token != "" && this.ls.token != null) {
      this.email=this.ls.getPayload()["sub"];
    }
  }

  logout() {
    this.email = "";
    this.ls.logout();
  }
}
