import { Component, OnInit } from '@angular/core';
import { StudentService } from '../service/student.service';
import { Student } from '../model/Student';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  student: Student={id:null,active: true, email: null, password: null,jmbg:null,name:null,lastname:null,birthdate:null,address:null,studentyear:null,attendingsubjects:null};
  students: Student[]=[];
  
  constructor(private ss: StudentService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.ss.getAll("").subscribe(response => this.students = response["content"]);
  }

  submit() {
    this.ss.add(this.student).subscribe(() => this.getAll());
  }

}
