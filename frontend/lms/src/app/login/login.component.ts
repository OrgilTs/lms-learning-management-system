import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';
import { User } from '../model/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user:User = {
    id:null,
    email: null,
    password: null,
    active:null,
    notifications:[]
  }


  constructor(private loginService: LoginService, private router:Router) { }

  ngOnInit(): void {
  }

  login() {
    this.loginService.login(this.user).subscribe(r=>{
      this.loginService.setToken(r);
      this.router.navigate(["/profil"]);
      
    }, r=> {
      console.log("failed");
    });
  }

}
