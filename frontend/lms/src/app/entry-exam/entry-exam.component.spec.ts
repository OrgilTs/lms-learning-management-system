import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryExamComponent } from './entry-exam.component';

describe('EntryExamComponent', () => {
  let component: EntryExamComponent;
  let fixture: ComponentFixture<EntryExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
