import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicCalendarUpdateComponent } from './academic-calendar-update.component';

describe('AcademicCalendarUpdateComponent', () => {
  let component: AcademicCalendarUpdateComponent;
  let fixture: ComponentFixture<AcademicCalendarUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcademicCalendarUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcademicCalendarUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
