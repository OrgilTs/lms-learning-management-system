import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.css']
})
export class ExamsComponent implements OnInit {
  role;
  constructor(private ls:LoginService) { }

  ngOnInit(): void {
    this.role = this.ls.getRole();
  }

}
