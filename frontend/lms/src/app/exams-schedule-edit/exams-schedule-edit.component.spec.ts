import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsScheduleEditComponent } from './exams-schedule-edit.component';

describe('ExamsScheduleEditComponent', () => {
  let component: ExamsScheduleEditComponent;
  let fixture: ComponentFixture<ExamsScheduleEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsScheduleEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsScheduleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
