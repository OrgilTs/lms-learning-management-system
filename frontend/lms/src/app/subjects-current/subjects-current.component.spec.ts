import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectsCurrentComponent } from './subjects-current.component';

describe('SubjectsCurrentComponent', () => {
  let component: SubjectsCurrentComponent;
  let fixture: ComponentFixture<SubjectsCurrentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectsCurrentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectsCurrentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
