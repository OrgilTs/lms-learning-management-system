import { Component, OnInit } from '@angular/core';
import { University } from '../model/University';
import { UniversityService } from '../service/university.service';

@Component({
  selector: 'app-universities',
  templateUrl: './universities.component.html',
  styleUrls: ['./universities.component.css']
})
export class UniversitiesComponent implements OnInit {

  university: University={ id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null};
  universities: University[]=[];

  constructor(private us: UniversityService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.us.getAll("").subscribe(response => this.universities = response["content"]);
  }

  submit() {
    this.us.add(this.university).subscribe(() => this.getAll());
  }
}
