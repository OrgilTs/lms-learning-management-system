import { Component, OnInit } from '@angular/core';
import { TeacherService } from '../service/teacher.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Teacher } from '../model/Teacher';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  teacher: Teacher={ id: null, university:null, address: null, active: null, email: null, password: null, jmbg:null, name: null, lastname: null, biography:null,faculty: null,major:null};
  teachers: Teacher[];

  constructor(private ts: TeacherService, private activatedRoute: ActivatedRoute,  private router: Router) { }

  ngOnInit(): void {
    this.ts.getOne(this.activatedRoute.snapshot.params['id']).subscribe(teacher => this.teacher = teacher);
    
  }

  delete() {
    this.ts.delete(this.teacher.id).subscribe(()=>this.router.navigate(["/teachers"]))
  }

}
