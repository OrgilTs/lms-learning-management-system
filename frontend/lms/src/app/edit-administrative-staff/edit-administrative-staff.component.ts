import { Component, OnInit } from '@angular/core';
import { AdministrativeStaffService } from '../service/administrative-staff.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Administrativestaff } from '../model/Administrativestaff';


@Component({
  selector: 'app-edit-administrative-staff',
  templateUrl: './edit-administrative-staff.component.html',
  styleUrls: ['./edit-administrative-staff.component.css']
})
export class EditAdministrativeStaffComponent implements OnInit {

  administrativeStaff: Administrativestaff={ id: null, address: null, active: true, email: null, university: null, password: null, jmbg: null, name: null, lastname: null };

  constructor(private as: AdministrativeStaffService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.as.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(i => this.administrativeStaff = i);
  }

  submit() {
    this.as.edit(this.administrativeStaff.id, this.administrativeStaff).subscribe(() => this.router.navigate(["/administrativestaffs", "update", this.administrativeStaff.id]));
  }

}
