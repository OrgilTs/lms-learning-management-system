import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAdministrativeStaffComponent } from './edit-administrative-staff.component';

describe('EditAdministrativeStaffComponent', () => {
  let component: EditAdministrativeStaffComponent;
  let fixture: ComponentFixture<EditAdministrativeStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAdministrativeStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAdministrativeStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
