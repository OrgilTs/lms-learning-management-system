import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { User } from '../model/User';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  user = {
    id:null,
    email: null,
    password: null,
    active:null,
    notifications:[]
  }
  users: User[]=[];

  payload: JSON;

  constructor(private us: UserService) { }

  ngOnInit(): void {
    this.us.getOne(this.getLoggedUser()).subscribe(user => this.user = user);
  }

  getLoggedUser(){
    var token = localStorage.getItem("TOKEN");
    this.payload = JSON.parse(atob(token.split('.')[1]));
    var id=this.payload["id"];
    return id;
    
  }

}
