import { Component, OnInit } from '@angular/core';

import { TeacherService } from '../service/teacher.service';
import { Teacher } from '../model/Teacher';
import { Faculty } from '../model/Faculty';
import { University } from '../model/University';
import { UniversityService } from '../service/university.service';
import { FacultyService } from '../service/faculty.service';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';
import { AdministrativeStaffService } from '../service/administrative-staff.service';
import { Administrativestaff } from '../model/Administrativestaff';
import { Major } from '../model/Major';
import { MajorService } from '../service/major.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  selectedFaculty = 0;
  selectedMajor = 0;
  major:Major={"id":null,"name":null,"faculty":null,"teachers":null,"studyyears":null};
  majors: Major[]=[];
  faculty: Faculty={id: null, name:null, university:null, address:null, phone:null, email:null, description:null, deandata:null, teachers:null,majors:null};
  faculties: Faculty[]=[];
  university: University={ notifications:null,id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null};
  administrativeStaff: Administrativestaff={ id: null, address: null,active: null, email: null, university:null, password: null, jmbg:null, name: null, lastname: null};
  teacher: Teacher={ id: null,active: null, university:null,address: null, email: null, password: null, jmbg:null, name: null, lastname: null, biography:null,faculty: null,major:null};
  teachers: Teacher[]=[];
  
  constructor(private ms: MajorService, private ts: TeacherService, private us: UniversityService, private fs: FacultyService, private as: AdministrativeStaffService, private ls: LoginService, private router:Router) { }

  ngOnInit(): void {
    this.us.getAll("").subscribe(response => this.faculties = response["content"]);
    this.as.getByEmail(this.ls.getPayload()["sub"]).subscribe(

      result => {
        this.administrativeStaff = result;
        this.us.getOne(this.administrativeStaff.university.id).subscribe(response => this.university = response);
      },
      error => {
        console.log(error);
      }
    );
  }

  submit() {
    this.ts.add(this.teacher).subscribe(() => null);
  }

  onFacultyChange(){
    this.fs.getOne(this.selectedFaculty).subscribe(response => this.faculty = response);
    }

    onMajorChange(){
      this.ms.getOne(this.selectedFaculty).subscribe(response => this.major = response);
      }
}
