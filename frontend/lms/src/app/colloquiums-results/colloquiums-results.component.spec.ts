import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColloquiumsResultsComponent } from './colloquiums-results.component';

describe('ColloquiumsResultsComponent', () => {
  let component: ColloquiumsResultsComponent;
  let fixture: ComponentFixture<ColloquiumsResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColloquiumsResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColloquiumsResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
