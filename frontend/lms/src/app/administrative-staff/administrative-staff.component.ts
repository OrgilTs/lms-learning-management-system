import { Component, OnInit } from '@angular/core';

import { StudentService } from '../service/student.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdministrativeStaffService } from '../service/administrative-staff.service';
import { Administrativestaff } from '../model/Administrativestaff';

@Component({
  selector: 'app-administrative-staff',
  templateUrl: './administrative-staff.component.html',
  styleUrls: ['./administrative-staff.component.css']
})
export class AdministrativeStaffComponent implements OnInit {

  administrativeStaff: Administrativestaff={ id: null, address: null, active: null, email: null, university:null, password: null, jmbg:null, name: null, lastname: null};
  administrativeStaffs: Administrativestaff[]=[];

  constructor(private as: AdministrativeStaffService, private activatedRoute: ActivatedRoute,  private router: Router) { }

  ngOnInit(): void {
    this.as.getOne(this.activatedRoute.snapshot.params['id']).subscribe(administrativeStaff => this.administrativeStaff = administrativeStaff);
  }

  delete() {
    this.as.delete(this.administrativeStaff.id).subscribe(()=>this.router.navigate(["/administrativestaffs"]))
  }

}
