import { Component, OnInit } from '@angular/core';
import { Major } from '../model/Major';
import { Faculty } from '../model/Faculty';
import { University } from '../model/University';
import { Student } from '../model/Student';
import { Administrativestaff } from '../model/Administrativestaff';
import { UniversityService } from '../service/university.service';
import { FacultyService } from '../service/faculty.service';
import { StudentService } from '../service/student.service';
import { AdministrativeStaffService } from '../service/administrative-staff.service';
import { MajorService } from '../service/major.service';
import { Studentyear } from '../model/Studentyear';
import { Entranceexam } from '../model/Entranceexam';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';
import { EntranceExamService } from '../service/entrance-exam.service';

@Component({
  selector: 'app-entry-exam-application',
  templateUrl: './entry-exam-application.component.html',
  styleUrls: ['./entry-exam-application.component.css']
})
export class EntryExamApplicationComponent implements OnInit {

  payload: JSON;
  selectedFaculty = 0;
  selectedUniversity = 0;
  selectedMajor = null;
  major: Major = { id: null, name: null, faculty: null, teachers: null, studyyears: null };
  majors: Major[]=[];
  faculty: Faculty={id: null, name:null, university:null, address:null, phone:null, email:null, description:null, deandata:null, teachers:null,majors:null};
  faculties: Faculty[]=[];
  university: University={ id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null};
  universities: University[]=[];
  administrativeStaff: Administrativestaff={ id: null, address: null, active: null, email: null, university:null, password: null, jmbg:null, name: null, lastname: null};
  student: Student={id:null,active: true, email: null, password: null,jmbg:null,name:null,lastname:null,birthdate:null,address:null,studentyear:null,attendingsubjects:null};
  studentYear: Studentyear = { id: null, indexnumber: null, dateofenrollment: null, students: null, student:null, studyyear:null };
  studentYears: Studentyear[]=[];
  entranceExam: Entranceexam = {id:null, student:{id:null,active: true, email: null, password: null,jmbg:null,name:null,lastname:null,birthdate:null,address:null,studentyear:null,attendingsubjects:null}, major:{ id: null, name: null, faculty: null, teachers: null, studyyears: null },requireddatetime: null, accepted: null};

  constructor(private es: EntranceExamService, private ms: MajorService, private us: UniversityService, private fs: FacultyService, private ss: StudentService, private as: AdministrativeStaffService, private ls: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.us.getAll("").subscribe(response => this.universities = response["content"]);
    this.fs.getAll("").subscribe(response => this.faculties = response["content"]);
    this.ms.getAll("").subscribe(response => this.majors = response["content"]);
  }

  submit() {
    console.log(this.entranceExam.major.id)
    this.entranceExam.student.id =this.getLoggedUser();
    // this.entranceExam.major.id =1;
    this.es.add(this.entranceExam).subscribe(
      result => {
        this.router.navigate(["prijemni-ispit"]);
      },
      error => {
        console.log(error);
      }
    );
  }

  onUniversityChange(){
    this.university= { id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null};
    if (this.selectedUniversity > 0) {
      this.us.getOne(this.selectedUniversity).subscribe(response => this.university = response);
    }
    }

  onFacultyChange() {
    this.selectedMajor = null;
    this.studentYear.studyyear = null;
    this.faculty = { id: null, name: null, university: null, address: null, phone: null, email: null, description: null, deandata: null, teachers: null, majors: null };
    if (this.selectedFaculty > 0) {
      this.fs.getOne(this.selectedFaculty).subscribe(response => this.faculty = response);
    }
  }

  onMajorChange() {
    this.studentYear.studyyear = null;
    this.major = { id: null, name: null, faculty: null, teachers: null, studyyears: null };
    console.log("rest");
    if (this.selectedMajor > 0) {
      this.ms.getOne(this.selectedMajor).subscribe(response => this.major = response);
    }
  }
  getLoggedUser(){
    var token = localStorage.getItem("TOKEN");
    this.payload = JSON.parse(atob(token.split('.')[1]));
    var id=this.payload["id"];
    return id;
  }

}
