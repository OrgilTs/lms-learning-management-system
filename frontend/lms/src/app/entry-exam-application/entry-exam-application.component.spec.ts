import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryExamApplicationComponent } from './entry-exam-application.component';

describe('EntryExamApplicationComponent', () => {
  let component: EntryExamApplicationComponent;
  let fixture: ComponentFixture<EntryExamApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryExamApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryExamApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
