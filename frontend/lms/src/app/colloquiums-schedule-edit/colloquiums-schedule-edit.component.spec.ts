import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColloquiumsScheduleEditComponent } from './colloquiums-schedule-edit.component';

describe('ColloquiumsScheduleEditComponent', () => {
  let component: ColloquiumsScheduleEditComponent;
  let fixture: ComponentFixture<ColloquiumsScheduleEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColloquiumsScheduleEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColloquiumsScheduleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
