import { TestBed } from '@angular/core/testing';

import { AdministrativeStaffService } from './administrative-staff.service';

describe('AdministrativeStaffService', () => {
  let service: AdministrativeStaffService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdministrativeStaffService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
