import { Injectable } from '@angular/core';
import { Place } from '../model/place';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {
  private address = "http://localhost:8080/api/place"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Place[]> {
    return this.http.get<Place[]>(this.address+params);
  }

  getOne(id: Number): Observable<Place> {
    return this.http.get<Place>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Place): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Place): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  }
}
