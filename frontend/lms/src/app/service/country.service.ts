import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../model/country';
@Injectable({
  providedIn: 'root'
})
export class CountryService {
  private address = "http://localhost:8080/api/country"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Country[]> {
    return this.http.get<Country[]>(this.address+params);
  }

  getOne(id: Number): Observable<Country> {
    return this.http.get<Country>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Country): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Country): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  }
}
