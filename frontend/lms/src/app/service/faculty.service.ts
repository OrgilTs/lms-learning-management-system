import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Faculty } from '../model/Faculty';


@Injectable({
  providedIn: 'root'
})
export class FacultyService {
  private address = "http://localhost:8080/api/faculty"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Faculty[]> {
    return this.http.get<Faculty[]>(this.address+params);
  }

  getOne(id: Number): Observable<Faculty> {
    return this.http.get<Faculty>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Faculty): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Faculty): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 
}
