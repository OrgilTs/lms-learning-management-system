import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Teacher } from '../model/Teacher';


@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  private address = "http://localhost:8080/api/teacher"; 
  
  constructor(private http: HttpClient) { }

  getAll(params:String): Observable<Teacher[]> {
    return this.http.get<Teacher[]>(this.address+params);
  }

  getOne(id: Number): Observable<Teacher> {
    return this.http.get<Teacher>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Teacher): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Teacher): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 
}
