import { TestBed } from '@angular/core/testing';

import { DaytitleService } from './daytitle.service';

describe('DaytitleService', () => {
  let service: DaytitleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DaytitleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
