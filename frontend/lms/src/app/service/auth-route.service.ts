import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { LoginService } from './login.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthRouteService implements CanActivate {
  
  private set: Set<string>;

  constructor(private router: Router, private loginService: LoginService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.set = new Set(route.data.allowedRoles);
    if(this.set.has(this.loginService.getRole())) { 
      return true;
    }
    this.router.navigateByUrl('/login');
    return false;
  }
  
}
