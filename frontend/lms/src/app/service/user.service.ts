import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/User';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private address = "http://localhost:8080/api/user"; // api address

  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<User[]> {
    return this.http.get<User[]>(this.address + params);
  }

  getOne(id: Number): Observable<User> {
    return this.http.get<User>(`${this.address}/${id}`);
  }

  getByEmail(id: Number): Observable<User> {
    return this.http.get<User>(`${this.address}/get-by-email/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: User): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: User): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  }

}
