import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Major } from '../model/Major';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MajorService {

  private address = "http://localhost:8080/api/major";

  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<{}> {
    return this.http.get<[]>(this.address+params);
  }

  getOne(id: Number): Observable<Major> {
    return this.http.get<Major>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Major): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Major): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  }
}
