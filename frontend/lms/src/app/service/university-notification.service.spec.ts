import { TestBed } from '@angular/core/testing';

import { UniversityNotificationService } from './university-notification.service';

describe('UniversityNotificationService', () => {
  let service: UniversityNotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UniversityNotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
