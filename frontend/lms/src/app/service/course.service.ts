import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course } from '../model/course';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  private address = "http://localhost:8080/api/major"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Course[]> {
    return this.http.get<Course[]>(this.address+params);
  }

  getOne(id: Number): Observable<Course> {
    return this.http.get<Course>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Course): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Course): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 
}
