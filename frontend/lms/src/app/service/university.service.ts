import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { University } from '../model/University';

@Injectable({
  providedIn: 'root'
})
export class UniversityService {
  private address = "http://localhost:8080/api/university"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<University[]> {
    return this.http.get<University[]>(this.address+params);
  }

  getOne(id: Number): Observable<University> {
    return this.http.get<University>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: University): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: University): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 
}
