import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from '../model/Subject';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  private address = "http://localhost:8080/api/subject"; 

  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Subject[]> {
    return this.http.get<Subject[]>(this.address+params);
  }

  getOne(id: Number): Observable<Subject> {
    return this.http.get<Subject>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Subject): Observable<Subject> {
    return this.http.post<Subject>(this.address, i);
  }

  edit(id: Number, i: Subject): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 
}
