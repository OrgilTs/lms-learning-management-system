import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Administrativestaff } from '../model/Administrativestaff';


@Injectable({
  providedIn: 'root'
})
export class AdministrativeStaffService {

  private address = "http://localhost:8080/api/administrativestaff"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Administrativestaff[]> {
    return this.http.get<Administrativestaff[]>(this.address+params);
  }

  getOne(id: Number): Observable<Administrativestaff> {
    return this.http.get<Administrativestaff>(`${this.address}/${id}`);
  }

  getByEmail(email: String): Observable<Administrativestaff> {
    return this.http.get<Administrativestaff>(`${this.address}/get-by-email/${email}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Administrativestaff): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Administrativestaff): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 
}
