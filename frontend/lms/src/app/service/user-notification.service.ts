import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usernotification } from '../model/Usernotification';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserNotificationService {

  private address = "http://localhost:8080/api/usernotification";

  constructor(private http:HttpClient) { }

  getAll(params: String): Observable<Usernotification[]> {
    return this.http.get<Usernotification[]>(this.address + params);
  }

  getOne(id: Number): Observable<Usernotification> {
    return this.http.get<Usernotification>(`${this.address}/${id}`);
  }

  getByEmail(id: Number): Observable<Usernotification> {
    return this.http.get<Usernotification>(`${this.address}/get-by-email/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Usernotification): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Usernotification): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  }
}
