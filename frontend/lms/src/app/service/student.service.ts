import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../model/Student';


@Injectable({
  providedIn: 'root'
})
export class StudentService {
  private address = "http://localhost:8080/api/student"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Student[]> {
    return this.http.get<Student[]>(this.address+params);
  }

  getOne(id: Number): Observable<Student> {
    return this.http.get<Student>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Student): Observable<Student> {
    return this.http.post<Student>(this.address, i);
  }

  registration(i: Student): Observable<Student> {
    return this.http.post<Student>(`${this.address}/registration`, i);
  }

  edit(id: Number, i: Student): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 

}
