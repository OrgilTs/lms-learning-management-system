import { TestBed } from '@angular/core/testing';

import { EntranceExamService } from './entrance-exam.service';

describe('EntranceExamService', () => {
  let service: EntranceExamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EntranceExamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
