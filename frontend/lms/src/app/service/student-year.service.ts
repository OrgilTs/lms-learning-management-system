import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Studentyear } from '../model/Studentyear';

@Injectable({
  providedIn: 'root'
})
export class StudentYearService {

  private address = "http://localhost:8080/api/studentyear"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Studentyear[]> {
    return this.http.get<Studentyear[]>(this.address+params);
  }

  getOne(id: Number): Observable<Studentyear> {
    return this.http.get<Studentyear>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Studentyear): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Studentyear): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 

}
