import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Universitynotification } from '../model/Universitynotification';

@Injectable({
  providedIn: 'root'
})
export class UniversityNotificationService {
  private address = "http://localhost:8080/api/universitynotification"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Universitynotification[]> {
    return this.http.get<Universitynotification[]>(this.address+params);
  }

  getOne(id: Number): Observable<Universitynotification> {
    return this.http.get<Universitynotification>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Universitynotification): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Universitynotification): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 
}
