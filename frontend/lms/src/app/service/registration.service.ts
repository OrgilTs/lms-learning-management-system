import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Student } from '../model/Student';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }


  add(student: Student): Observable<Student> {
    return this.http.post<Student>("http://localhost:8080/api/student/", student);
  }

  update(id,student): Observable<Student> {
    return this.http.put<Student>(`http://localhost:8080/api/student/${id}`, student);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/student" + id);
  }
  getAll(params: String): Observable<Student[]> {
    return this.http.get<Student[]>("http://localhost:8080/api/student"+params);
  }
}
