import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Schedule } from '../model/Schedule';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  private address = "http://localhost:8080/api/schedule"; 

  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Schedule[]> {
    return this.http.get<Schedule[]>(this.address+params);
  }

  getOne(id: Number): Observable<Schedule> {
    return this.http.get<Schedule>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Schedule): Observable<Schedule> {
    return this.http.post<Schedule>(this.address, i);
  }

  edit(id: Number, i: Schedule): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 
}
