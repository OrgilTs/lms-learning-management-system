import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Entranceexam } from '../model/Entranceexam';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EntranceExamService {
  private address = "http://localhost:8080/api/entranceexam"; 
  
  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<Entranceexam[]> {
    return this.http.get<Entranceexam[]>(this.address+params);
  }

  getOne(id: Number): Observable<Entranceexam> {
    return this.http.get<Entranceexam>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Entranceexam): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Entranceexam): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  }
}
