import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Daytitle } from '../model/DayTitle';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DaytitleService {

  private address = "http://localhost:8080/api/daytitle";  

  constructor(private http:HttpClient) { }


  getAll(params: String): Observable<Daytitle[]> {
    return this.http.get<Daytitle[]>(this.address+params);
  }

  getOne(id: Number): Observable<Daytitle> {
    return this.http.get<Daytitle>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Daytitle): Observable<Daytitle> {
    return this.http.post<Daytitle>(this.address, i);
  }

  edit(id: Number, i: Daytitle): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  } 
}
