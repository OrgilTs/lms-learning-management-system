import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Address } from '../model/address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  private address = "http://localhost:8080/api/address";

  constructor(private http: HttpClient) { }

  getAll(params: String): Observable<{}> {
    return this.http.get<{}>(this.address+params);
  }

  getOne(id: Number): Observable<Address> {
    return this.http.get<Address>(`${this.address}/${id}`);
  }

  delete(id: Number): Observable<{}> {
    return this.http.delete(`${this.address}/${id}`);
  }

  add(i: Address): Observable<{}> {
    return this.http.post(this.address, i);
  }

  edit(id: Number, i: Address): Observable<{}> {
    return this.http.put(`${this.address}/${id}`, i);
  }
}
