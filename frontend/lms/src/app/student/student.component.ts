import { Component, OnInit } from '@angular/core';
import { StudentService } from '../service/student.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Student } from '../model/Student';
import { Studentyear } from '../model/Studentyear';
import { StudentYearService } from '../service/student-year.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  student: Student={id:null,active: true, email: null, password: null,jmbg:null,name:null,lastname:null,birthdate:null,address:null,studentyear:null,attendingsubjects:null};
  studentYear: Studentyear={id:null,indexnumber :null,dateofenrollment :null, students :null, student: null, studyyear: null }
  students: Student[]=[];

  constructor(private ss: StudentService, private sy: StudentYearService,  private activatedRoute: ActivatedRoute,  private router: Router) { }

  ngOnInit(): void {
    this.ss.getOne(this.activatedRoute.snapshot.params['id']).subscribe(student => this.student = student);
    this.sy.getOne(this.activatedRoute.snapshot.params['id']).subscribe(studentYear => this.studentYear = studentYear);
    
  }

  delete() {
    this.ss.delete(this.student.id).subscribe(()=>this.router.navigate(["/students"]))
  }
  
}
