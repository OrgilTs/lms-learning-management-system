import { Component, OnInit } from '@angular/core';
import { University } from '../model/University';
import { UniversityService } from '../service/university.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  university: University={ id: null, name:null, address:null, phone:null, email:null, description:null, rector:null, dateofestablishment:null, faculties:null, notifications:null};
  universities: University[]=[];
  
  constructor(private us: UniversityService) { }

  ngOnInit(): void {
    this.us.getOne(1).subscribe(university => this.university = university);
  }

}
