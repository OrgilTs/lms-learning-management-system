import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RectorInformationComponent } from './rector-information.component';

describe('RectorInformationComponent', () => {
  let component: RectorInformationComponent;
  let fixture: ComponentFixture<RectorInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RectorInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RectorInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
