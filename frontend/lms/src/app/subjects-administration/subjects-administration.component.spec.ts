import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectsAdministrationComponent } from './subjects-administration.component';

describe('SubjectsAdministrationComponent', () => {
  let component: SubjectsAdministrationComponent;
  let fixture: ComponentFixture<SubjectsAdministrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectsAdministrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectsAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
