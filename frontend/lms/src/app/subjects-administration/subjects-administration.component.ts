import { Component, OnInit } from '@angular/core';
import { SubjectService } from '../service/subject.service';
import { Subject } from '../model/Subject';
import { Studyyear } from '../model/Studyyear';
import { StudyyearService } from '../service/studyyear.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-subjects-administration',
  templateUrl: './subjects-administration.component.html',
  styleUrls: ['./subjects-administration.component.css']
})
export class SubjectsAdministrationComponent implements OnInit {
  selectedStudyYear = 0;
  studyyears:Studyyear[]=[];
  subject:Subject={id:null,name:null,espb:null,required:true,numberoflectures:null,numberofpractices:null,otherFormsOfClass:1,researchwork:1,remainingclasses:40,studyyear:{ id: null, year: null, major: null, subjects:null},classrealizations:null,results:null,schedules:null};
  subjects:Subject[];

 

  constructor(private ss:SubjectService,private sys:StudyyearService,private router: Router) { }

  ngOnInit(): void {
    this.getAllSubjects();
    this.getAllStudyYears();
  }
  getAllSubjects(){
    this.ss.getAll("").subscribe(response => this.subjects = response["content"]);
  }
  getAllStudyYears(){
    this.sys.getAll("").subscribe(response => this.studyyears = response["content"]);
  }

  submit() {
    this.ss.add(this.subject).subscribe(
      result => {
        this.router.navigate(["administracija-predmeta"]);
      },
      error => {
        console.log(error);
        
      }
    );
  }

}
