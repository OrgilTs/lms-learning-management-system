import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsUserEditComponent } from './notifications-user-edit.component';

describe('NotificationsUserEditComponent', () => {
  let component: NotificationsUserEditComponent;
  let fixture: ComponentFixture<NotificationsUserEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsUserEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsUserEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
