import { Component, OnInit } from '@angular/core';
import { User } from '../model/User';
import { Usernotification } from '../model/Usernotification';
import { UserNotificationService } from '../service/user-notification.service';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-notifications-user-edit',
  templateUrl: './notifications-user-edit.component.html',
  styleUrls: ['./notifications-user-edit.component.css']
})
export class NotificationsUserEditComponent implements OnInit {

  user: User = { id:null, active :true,email :null, password :null, notifications:null}
  users: User[]=[];
  userNotification: Usernotification  = {id:null, created: null, title: null, user: { id:null, active :true,email :null, password :null, notifications:null}, message: null}
  userNotifications: Usernotification[]=[];

  constructor(private nus: UserNotificationService,  private us:UserService, private router: Router) { }

  ngOnInit(): void {
    this.us.getAll("").subscribe(response => this.users = response["content"]);
    this.nus.getAll("").subscribe(response => this.userNotifications = response["content"]);
  }

  submit(){
    this.nus.edit(this.userNotification.id, this.userNotification).subscribe(() => this.router.navigate(["/obavestenja", "update", this.userNotification.id]));
  }

}
