import subprocess
import os
class AppGenerator:
    def __init__(self, app, class_name, class_attributes):
        self.app = app
        self.class_name = class_name
        self.attributes = class_attributes
        self.template = ""
        self.generate()
        
    def get_template(self):
        return self.template
    
    def generate(self):
        attributes_tpl = ""
        constructor_attributes_tpl = ""
        constructor_body_tpl = ""
        gs_tpl = ""
        for a in self.attributes:
            name = a["name"]
            name1 = a["name"].lower()
            type = a["type"]
            if (type=="LocalDateTime") or (type=="Date"):
                pass
            else:
                type = a["type"].lower()
            
            attributes_tpl += "\n     {1} :{0};".format(name, a["name"]+"service", a["nullable"])

            # add attributes for constructor
            if len(constructor_attributes_tpl) < 1:
                constructor_attributes_tpl += "{} {}".format(a["type"], a["name"])
            else:
                constructor_attributes_tpl += ", {} {}".format(a["type"], a["name"])

            if len(constructor_attributes_tpl) < 1:
                constructor_attributes_tpl += "{} {}".format(a["type"], a["name"])
            else:
                constructor_attributes_tpl += ", {} {}".format(a["type"], a["name"])
            if len(constructor_body_tpl) < 1:
                constructor_body_tpl += "long id"
            else:
                constructor_body_tpl += ", long id"

        constructor_tpl = """
    public {0}() {{\n        super();\n    }}\n\n
    public {0}({1}) {{
        this.id=id;
    {2}
    }}\n
""".format(self.class_name.title(), constructor_attributes_tpl, constructor_body_tpl)
        class_header_tpl = ""
        
        class_header_tpl += "{}".format(self.class_name.title())
        self.template = """
import {{ Component }} from '@angular/core';

@Component({{
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
}})
export class AppComponent {{
  title = 'lms';
}}

""".format(class_header_tpl, attributes_tpl, constructor_tpl, gs_tpl, self.app.lower(), "")
    
    