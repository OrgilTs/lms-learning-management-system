import subprocess
import os
class CssGenerator:
    def __init__(self, app, class_name, class_attributes):
        self.app = app
        self.class_name = class_name
        self.attributes = class_attributes
        self.template = ""
        self.generate()
        
    def get_template(self):
        return self.template
    
    def generate(self):
      return self.template
