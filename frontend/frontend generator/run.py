import csv
import sys
from projectMake import genProject
import os
import shutil

def menu():
    print("************MAIN MENU**************")
    #time.sleep(1)
    print()

    choice = input("""
                      1: generate app
                      2: delete app
                      3: ...
                      4: ...
                      5: Quit/Log Out

                      Please enter your choice: """)

    if choice == 1 or choice =="1":
        genProject()
    elif choice == 2 or choice =="2":
        shutil.rmtree('./app', ignore_errors=True)
        menu()
    elif choice == 3 or choice =="3":
        pass
    elif choice==4 or choice=="4":
        pass
    elif choice==5 or choice=="5":
        sys.exit
    else:
        print("wrong choice")
        print("Please try again")
        menu()



menu()