import subprocess
import os
class ServiceGenerator:
    def __init__(self, app, class_name, class_attributes):
        self.app = app
        self.class_name = class_name
        self.attributes = class_attributes
        self.template = ""
        self.generate()
        
    def get_template(self):
        return self.template
    
    def generate(self):
        attributes_tpl = ""
        constructor_attributes_tpl = ""
        constructor_body_tpl = ""
        gs_tpl = ""
        for a in self.attributes:
            name = a["name"]+"service"
            name1 = a["name"].lower()
            type = a["type"]
            if (type=="LocalDateTime") or (type=="Date"):
                pass
            else:
                type = a["type"].lower()
            
            attributes_tpl += "\n     {1} :{0};".format(name, a["name"]+"service", a["nullable"])

            # add attributes for constructor
            if len(constructor_attributes_tpl) < 1:
                constructor_attributes_tpl += "{} {}".format(a["type"], a["name"]+"service")
            else:
                constructor_attributes_tpl += ", {} {}".format(a["type"], a["name"]+"service")

            if len(constructor_attributes_tpl) < 1:
                constructor_attributes_tpl += "{} {}".format(a["type"], a["name"]+"service")
            else:
                constructor_attributes_tpl += ", {} {}".format(a["type"], a["name"]+"service")
            if len(constructor_body_tpl) < 1:
                constructor_body_tpl += "long id"
            else:
                constructor_body_tpl += ", long id"

        constructor_tpl = """
    public {0}() {{\n        super();\n    }}\n\n
    public {0}({1}) {{
        this.id=id;
    {2}
    }}\n
""".format(self.class_name.title(), constructor_attributes_tpl, constructor_body_tpl)
        class_header_tpl = ""
        
        class_header_tpl += "{}".format(self.class_name.title())
        self.template = """

import {{ Injectable }} from '@angular/core';
import {{ HttpClient }} from '@angular/common/http';
import {{ Observable }} from 'rxjs';
import {{ {0} }} from '../model/{7}';

@Injectable({{
  providedIn: 'root'
}})
export class {0}Service {{

  constructor(private http: HttpClient) {{ }}

  getAll(): Observable<{0}[]> {{
    return this.http.get<{0}[]>("http://localhost:8080/api/{6}/");
  }}
  delete(id: Number): Observable<{{}}> {{
    return this.http.delete("http://localhost:8080/api/{6}/" + id);
  }}

  getOne(id): Observable<{0}> {{
    return this.http.get<{0}>(`http://localhost:8080/api/{6}/${{id}}`);
  }}

  add({7}: {0}): Observable<{0}> {{
    return this.http.post<{0}>("http://localhost:8080/api/{6}/", {7});
  }}

  update(id,{7}): Observable<{0}> {{
    return this.http.put<{0}>(`http://localhost:8080/api/{6}/${{id}}`, {7});
  }}


}}
    
}}
""".format(class_header_tpl, attributes_tpl, constructor_tpl, gs_tpl, self.app.lower(), "", class_header_tpl.lower()+"s", class_header_tpl.lower())
    
    