import subprocess
import os
class ComponentSpec2Generator:
    def __init__(self, app, class_name, class_attributes):
        self.app = app
        self.class_name = class_name
        self.attributes = class_attributes
        self.template = ""
        self.generate()
        
    def get_template(self):
        return self.template
    
    def generate(self):
        self.template = """
import {{ async, ComponentFixture, TestBed }} from '@angular/core/testing';

import {{ {0}Component }} from './{1}.component';

describe('{0}Component', () => {{
  let component: {0}Component;
  let fixture: ComponentFixture<{0}Component>;

  beforeEach(async(() => {{
    TestBed.configureTestingModule({{
      declarations: [ {0}Component ]
    }})
    .compileComponents();
  }}));

  beforeEach(() => {{
    fixture = TestBed.createComponent({0}Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }});

  it('should create', () => {{
    expect(component).toBeTruthy();
  }});
}});
""".format(self.class_name.title(), self.class_name.lower())
