import subprocess
import os
class ComponentGenerator:
    def __init__(self, app, class_name, class_attributes):
        self.app = app
        self.class_name = class_name
        self.attributes = class_attributes
        self.template = ""
        self.generate()
        
    def get_template(self):
        return self.template
    
    def generate(self):
        attributes_tpl = ""
        for a in self.attributes:
            if len(attributes_tpl) > 0:
                attributes_tpl += ", {}:null".format(a["name"])
            else:
                attributes_tpl += "{}:null".format(a["name"])
        
        new_object = "{0}:{1} = {{ {2} }};".format(self.class_name.lower(), self.class_name.title(), attributes_tpl)
            
  
        self.template = """
import {{ Component, OnInit }} from '@angular/core';
import {{{1}Service}} from '../service/{1}.service'
import {{{1}}} from '../model/{0}'

@Component({{
  selector: 'app-{0}',
  templateUrl: './{0}.component.html',
  styleUrls: ['./{0}.component.css']
}})
export class {1}Component implements OnInit {{
  {2}
  constructor(private service:{1}Service, private activatedRoute: ActivatedRoute) {{ }}

  ngOnInit(): void {{
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {{
        this.{0} = result;
      }},
      error => {{
        console.log(error);
      }}
    );

  }}
}}        

""".format(self.class_name.lower(), self.class_name.title(), new_object)
