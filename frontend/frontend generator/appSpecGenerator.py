import subprocess
import os
class AppSpecGenerator:
    def __init__(self, app, class_name, class_attributes):
        self.app = app
        self.class_name = class_name
        self.attributes = class_attributes
        self.template = ""
        self.generate()
        
    def get_template(self):
        return self.template
    
    def generate(self):
        attributes_tpl = ""
        constructor_attributes_tpl = ""
        constructor_body_tpl = ""
        gs_tpl = ""
        for a in self.attributes:
            name = a["name"]+"service"+"spec"
            
            attributes_tpl += "\n     {1} :{0};".format(name, a["name"], a["nullable"])

            # add attributes for constructor
            if len(constructor_attributes_tpl) < 1:
                constructor_attributes_tpl += "{}".format(a["name"])
            else:
                constructor_attributes_tpl += ", {}".format(a["name"])

            if len(constructor_attributes_tpl) < 1:
                constructor_attributes_tpl += "{}".format(a["name"])
            else:
                constructor_attributes_tpl += ", {}".format(a["name"])
            if len(constructor_body_tpl) < 1:
                constructor_body_tpl += "long id"
            else:
                constructor_body_tpl += ", long id"
  

        constructor_tpl = """
    public {0}() {{\n        super();\n    }}\n\n
    public {0}({1}) {{
        this.id=id;
    {2}
    }}\n
""".format(self.class_name.title(), constructor_attributes_tpl, constructor_body_tpl)
        class_header_tpl = ""
        
        class_header_tpl += "{}".format(self.class_name.title())
        self.template = """

import {{ TestBed, async }} from '@angular/core/testing';
import {{ RouterTestingModule }} from '@angular/router/testing';
import {{ AppComponent }} from './app.component';

describe('AppComponent', () => {{
  beforeEach(async(() => {{
    TestBed.configureTestingModule({{
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }}).compileComponents();
  }}));

  it('should create the app', () => {{
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  }});

  it(`should have as title 'lms'`, () => {{
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('lms');
  }});

  it('should render title', () => {{
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('lms app is running!');
  }});
}});



""".format(class_header_tpl, attributes_tpl, constructor_tpl, gs_tpl, self.app.lower(), "")
