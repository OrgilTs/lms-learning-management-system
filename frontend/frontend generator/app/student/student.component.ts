
import { Component, OnInit } from '@angular/core';
import {StudentService} from '../service/Student.service'
import {Student} from '../model/student'

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  student:Student = { jmbg:null, name:null, lastName:null, birthDate:null, residence:null, studentYear:null, attendingSubjects:null };
  constructor(private service:StudentService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.student = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

