
import { Component, OnInit } from '@angular/core';
import {ScientificfieldService} from '../service/Scientificfield.service'
import {Scientificfield} from '../model/scientificfield'

@Component({
  selector: 'app-scientificfield',
  templateUrl: './scientificfield.component.html',
  styleUrls: ['./scientificfield.component.css']
})
export class ScientificfieldComponent implements OnInit {
  scientificfield:Scientificfield = { name:null, titles:null };
  constructor(private service:ScientificfieldService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.scientificfield = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

