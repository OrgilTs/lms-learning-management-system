

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Place } from '../model/place';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Place[]> {
    return this.http.get<Place[]>("http://localhost:8080/api/places/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/places/" + id);
  }

  getOne(id): Observable<Place> {
    return this.http.get<Place>(`http://localhost:8080/api/places/${id}`);
  }

  add(place: Place): Observable<Place> {
    return this.http.post<Place>("http://localhost:8080/api/places/", place);
  }

  update(id,place): Observable<Place> {
    return this.http.put<Place>(`http://localhost:8080/api/places/${id}`, place);
  }


}
    
}
