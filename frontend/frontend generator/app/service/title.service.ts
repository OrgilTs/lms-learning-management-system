

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Title } from '../model/title';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Title[]> {
    return this.http.get<Title[]>("http://localhost:8080/api/titles/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/titles/" + id);
  }

  getOne(id): Observable<Title> {
    return this.http.get<Title>(`http://localhost:8080/api/titles/${id}`);
  }

  add(title: Title): Observable<Title> {
    return this.http.post<Title>("http://localhost:8080/api/titles/", title);
  }

  update(id,title): Observable<Title> {
    return this.http.put<Title>(`http://localhost:8080/api/titles/${id}`, title);
  }


}
    
}
