

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../model/country';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Country[]> {
    return this.http.get<Country[]>("http://localhost:8080/api/countrys/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/countrys/" + id);
  }

  getOne(id): Observable<Country> {
    return this.http.get<Country>(`http://localhost:8080/api/countrys/${id}`);
  }

  add(country: Country): Observable<Country> {
    return this.http.post<Country>("http://localhost:8080/api/countrys/", country);
  }

  update(id,country): Observable<Country> {
    return this.http.put<Country>(`http://localhost:8080/api/countrys/${id}`, country);
  }


}
    
}
