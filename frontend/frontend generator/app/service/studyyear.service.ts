

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Studyyear } from '../model/studyyear';

@Injectable({
  providedIn: 'root'
})
export class StudyyearService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Studyyear[]> {
    return this.http.get<Studyyear[]>("http://localhost:8080/api/studyyears/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/studyyears/" + id);
  }

  getOne(id): Observable<Studyyear> {
    return this.http.get<Studyyear>(`http://localhost:8080/api/studyyears/${id}`);
  }

  add(studyyear: Studyyear): Observable<Studyyear> {
    return this.http.post<Studyyear>("http://localhost:8080/api/studyyears/", studyyear);
  }

  update(id,studyyear): Observable<Studyyear> {
    return this.http.put<Studyyear>(`http://localhost:8080/api/studyyears/${id}`, studyyear);
  }


}
    
}
