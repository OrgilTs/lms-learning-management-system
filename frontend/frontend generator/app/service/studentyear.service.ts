

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Studentyear } from '../model/studentyear';

@Injectable({
  providedIn: 'root'
})
export class StudentyearService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Studentyear[]> {
    return this.http.get<Studentyear[]>("http://localhost:8080/api/studentyears/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/studentyears/" + id);
  }

  getOne(id): Observable<Studentyear> {
    return this.http.get<Studentyear>(`http://localhost:8080/api/studentyears/${id}`);
  }

  add(studentyear: Studentyear): Observable<Studentyear> {
    return this.http.post<Studentyear>("http://localhost:8080/api/studentyears/", studentyear);
  }

  update(id,studentyear): Observable<Studentyear> {
    return this.http.put<Studentyear>(`http://localhost:8080/api/studentyears/${id}`, studentyear);
  }


}
    
}
