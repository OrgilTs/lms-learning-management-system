

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../model/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Student[]> {
    return this.http.get<Student[]>("http://localhost:8080/api/students/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/students/" + id);
  }

  getOne(id): Observable<Student> {
    return this.http.get<Student>(`http://localhost:8080/api/students/${id}`);
  }

  add(student: Student): Observable<Student> {
    return this.http.post<Student>("http://localhost:8080/api/students/", student);
  }

  update(id,student): Observable<Student> {
    return this.http.put<Student>(`http://localhost:8080/api/students/${id}`, student);
  }


}
    
}
