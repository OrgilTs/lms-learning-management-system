

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Classrealization } from '../model/classrealization';

@Injectable({
  providedIn: 'root'
})
export class ClassrealizationService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Classrealization[]> {
    return this.http.get<Classrealization[]>("http://localhost:8080/api/classrealizations/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/classrealizations/" + id);
  }

  getOne(id): Observable<Classrealization> {
    return this.http.get<Classrealization>(`http://localhost:8080/api/classrealizations/${id}`);
  }

  add(classrealization: Classrealization): Observable<Classrealization> {
    return this.http.post<Classrealization>("http://localhost:8080/api/classrealizations/", classrealization);
  }

  update(id,classrealization): Observable<Classrealization> {
    return this.http.put<Classrealization>(`http://localhost:8080/api/classrealizations/${id}`, classrealization);
  }


}
    
}
