

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Teacher } from '../model/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Teacher[]> {
    return this.http.get<Teacher[]>("http://localhost:8080/api/teachers/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/teachers/" + id);
  }

  getOne(id): Observable<Teacher> {
    return this.http.get<Teacher>(`http://localhost:8080/api/teachers/${id}`);
  }

  add(teacher: Teacher): Observable<Teacher> {
    return this.http.post<Teacher>("http://localhost:8080/api/teachers/", teacher);
  }

  update(id,teacher): Observable<Teacher> {
    return this.http.put<Teacher>(`http://localhost:8080/api/teachers/${id}`, teacher);
  }


}
    
}
