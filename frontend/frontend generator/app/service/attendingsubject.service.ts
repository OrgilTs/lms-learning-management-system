

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Attendingsubject } from '../model/attendingsubject';

@Injectable({
  providedIn: 'root'
})
export class AttendingsubjectService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Attendingsubject[]> {
    return this.http.get<Attendingsubject[]>("http://localhost:8080/api/attendingsubjects/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/attendingsubjects/" + id);
  }

  getOne(id): Observable<Attendingsubject> {
    return this.http.get<Attendingsubject>(`http://localhost:8080/api/attendingsubjects/${id}`);
  }

  add(attendingsubject: Attendingsubject): Observable<Attendingsubject> {
    return this.http.post<Attendingsubject>("http://localhost:8080/api/attendingsubjects/", attendingsubject);
  }

  update(id,attendingsubject): Observable<Attendingsubject> {
    return this.http.put<Attendingsubject>(`http://localhost:8080/api/attendingsubjects/${id}`, attendingsubject);
  }


}
    
}
