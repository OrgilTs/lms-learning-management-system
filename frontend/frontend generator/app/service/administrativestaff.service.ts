

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Administrativestaff } from '../model/administrativestaff';

@Injectable({
  providedIn: 'root'
})
export class AdministrativestaffService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Administrativestaff[]> {
    return this.http.get<Administrativestaff[]>("http://localhost:8080/api/administrativestaffs/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/administrativestaffs/" + id);
  }

  getOne(id): Observable<Administrativestaff> {
    return this.http.get<Administrativestaff>(`http://localhost:8080/api/administrativestaffs/${id}`);
  }

  add(administrativestaff: Administrativestaff): Observable<Administrativestaff> {
    return this.http.post<Administrativestaff>("http://localhost:8080/api/administrativestaffs/", administrativestaff);
  }

  update(id,administrativestaff): Observable<Administrativestaff> {
    return this.http.put<Administrativestaff>(`http://localhost:8080/api/administrativestaffs/${id}`, administrativestaff);
  }


}
    
}
