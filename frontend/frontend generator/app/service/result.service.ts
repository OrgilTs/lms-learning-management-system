

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Result } from '../model/result';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Result[]> {
    return this.http.get<Result[]>("http://localhost:8080/api/results/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/results/" + id);
  }

  getOne(id): Observable<Result> {
    return this.http.get<Result>(`http://localhost:8080/api/results/${id}`);
  }

  add(result: Result): Observable<Result> {
    return this.http.post<Result>("http://localhost:8080/api/results/", result);
  }

  update(id,result): Observable<Result> {
    return this.http.put<Result>(`http://localhost:8080/api/results/${id}`, result);
  }


}
    
}
