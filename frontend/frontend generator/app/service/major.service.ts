

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Major } from '../model/major';

@Injectable({
  providedIn: 'root'
})
export class MajorService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Major[]> {
    return this.http.get<Major[]>("http://localhost:8080/api/majors/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/majors/" + id);
  }

  getOne(id): Observable<Major> {
    return this.http.get<Major>(`http://localhost:8080/api/majors/${id}`);
  }

  add(major: Major): Observable<Major> {
    return this.http.post<Major>("http://localhost:8080/api/majors/", major);
  }

  update(id,major): Observable<Major> {
    return this.http.put<Major>(`http://localhost:8080/api/majors/${id}`, major);
  }


}
    
}
