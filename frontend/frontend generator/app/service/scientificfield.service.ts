

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Scientificfield } from '../model/scientificfield';

@Injectable({
  providedIn: 'root'
})
export class ScientificfieldService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Scientificfield[]> {
    return this.http.get<Scientificfield[]>("http://localhost:8080/api/scientificfields/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/scientificfields/" + id);
  }

  getOne(id): Observable<Scientificfield> {
    return this.http.get<Scientificfield>(`http://localhost:8080/api/scientificfields/${id}`);
  }

  add(scientificfield: Scientificfield): Observable<Scientificfield> {
    return this.http.post<Scientificfield>("http://localhost:8080/api/scientificfields/", scientificfield);
  }

  update(id,scientificfield): Observable<Scientificfield> {
    return this.http.put<Scientificfield>(`http://localhost:8080/api/scientificfields/${id}`, scientificfield);
  }


}
    
}
