

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Administrator } from '../model/administrator';

@Injectable({
  providedIn: 'root'
})
export class AdministratorService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Administrator[]> {
    return this.http.get<Administrator[]>("http://localhost:8080/api/administrators/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/administrators/" + id);
  }

  getOne(id): Observable<Administrator> {
    return this.http.get<Administrator>(`http://localhost:8080/api/administrators/${id}`);
  }

  add(administrator: Administrator): Observable<Administrator> {
    return this.http.post<Administrator>("http://localhost:8080/api/administrators/", administrator);
  }

  update(id,administrator): Observable<Administrator> {
    return this.http.put<Administrator>(`http://localhost:8080/api/administrators/${id}`, administrator);
  }


}
    
}
