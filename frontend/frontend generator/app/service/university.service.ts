

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { University } from '../model/university';

@Injectable({
  providedIn: 'root'
})
export class UniversityService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<University[]> {
    return this.http.get<University[]>("http://localhost:8080/api/universitys/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/universitys/" + id);
  }

  getOne(id): Observable<University> {
    return this.http.get<University>(`http://localhost:8080/api/universitys/${id}`);
  }

  add(university: University): Observable<University> {
    return this.http.post<University>("http://localhost:8080/api/universitys/", university);
  }

  update(id,university): Observable<University> {
    return this.http.put<University>(`http://localhost:8080/api/universitys/${id}`, university);
  }


}
    
}
