

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Classtype } from '../model/classtype';

@Injectable({
  providedIn: 'root'
})
export class ClasstypeService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Classtype[]> {
    return this.http.get<Classtype[]>("http://localhost:8080/api/classtypes/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/classtypes/" + id);
  }

  getOne(id): Observable<Classtype> {
    return this.http.get<Classtype>(`http://localhost:8080/api/classtypes/${id}`);
  }

  add(classtype: Classtype): Observable<Classtype> {
    return this.http.post<Classtype>("http://localhost:8080/api/classtypes/", classtype);
  }

  update(id,classtype): Observable<Classtype> {
    return this.http.put<Classtype>(`http://localhost:8080/api/classtypes/${id}`, classtype);
  }


}
    
}
