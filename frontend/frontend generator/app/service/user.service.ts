

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>("http://localhost:8080/api/users/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/users/" + id);
  }

  getOne(id): Observable<User> {
    return this.http.get<User>(`http://localhost:8080/api/users/${id}`);
  }

  add(user: User): Observable<User> {
    return this.http.post<User>("http://localhost:8080/api/users/", user);
  }

  update(id,user): Observable<User> {
    return this.http.put<User>(`http://localhost:8080/api/users/${id}`, user);
  }


}
    
}
