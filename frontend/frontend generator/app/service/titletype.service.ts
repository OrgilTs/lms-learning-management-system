

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Titletype } from '../model/titletype';

@Injectable({
  providedIn: 'root'
})
export class TitletypeService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Titletype[]> {
    return this.http.get<Titletype[]>("http://localhost:8080/api/titletypes/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/titletypes/" + id);
  }

  getOne(id): Observable<Titletype> {
    return this.http.get<Titletype>(`http://localhost:8080/api/titletypes/${id}`);
  }

  add(titletype: Titletype): Observable<Titletype> {
    return this.http.post<Titletype>("http://localhost:8080/api/titletypes/", titletype);
  }

  update(id,titletype): Observable<Titletype> {
    return this.http.put<Titletype>(`http://localhost:8080/api/titletypes/${id}`, titletype);
  }


}
    
}
