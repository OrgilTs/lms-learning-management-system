

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Faculty } from '../model/faculty';

@Injectable({
  providedIn: 'root'
})
export class FacultyService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Faculty[]> {
    return this.http.get<Faculty[]>("http://localhost:8080/api/facultys/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/facultys/" + id);
  }

  getOne(id): Observable<Faculty> {
    return this.http.get<Faculty>(`http://localhost:8080/api/facultys/${id}`);
  }

  add(faculty: Faculty): Observable<Faculty> {
    return this.http.post<Faculty>("http://localhost:8080/api/facultys/", faculty);
  }

  update(id,faculty): Observable<Faculty> {
    return this.http.put<Faculty>(`http://localhost:8080/api/facultys/${id}`, faculty);
  }


}
    
}
