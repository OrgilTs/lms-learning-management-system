

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subject } from '../model/subject';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Subject[]> {
    return this.http.get<Subject[]>("http://localhost:8080/api/subjects/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/subjects/" + id);
  }

  getOne(id): Observable<Subject> {
    return this.http.get<Subject>(`http://localhost:8080/api/subjects/${id}`);
  }

  add(subject: Subject): Observable<Subject> {
    return this.http.post<Subject>("http://localhost:8080/api/subjects/", subject);
  }

  update(id,subject): Observable<Subject> {
    return this.http.put<Subject>(`http://localhost:8080/api/subjects/${id}`, subject);
  }


}
    
}
