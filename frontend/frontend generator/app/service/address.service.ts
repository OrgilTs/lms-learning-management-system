

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Address } from '../model/address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Address[]> {
    return this.http.get<Address[]>("http://localhost:8080/api/addresss/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/addresss/" + id);
  }

  getOne(id): Observable<Address> {
    return this.http.get<Address>(`http://localhost:8080/api/addresss/${id}`);
  }

  add(address: Address): Observable<Address> {
    return this.http.post<Address>("http://localhost:8080/api/addresss/", address);
  }

  update(id,address): Observable<Address> {
    return this.http.put<Address>(`http://localhost:8080/api/addresss/${id}`, address);
  }


}
    
}
