

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Teacheratrealization } from '../model/teacheratrealization';

@Injectable({
  providedIn: 'root'
})
export class TeacheratrealizationService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Teacheratrealization[]> {
    return this.http.get<Teacheratrealization[]>("http://localhost:8080/api/teacheratrealizations/");
  }
  delete(id: Number): Observable<{}> {
    return this.http.delete("http://localhost:8080/api/teacheratrealizations/" + id);
  }

  getOne(id): Observable<Teacheratrealization> {
    return this.http.get<Teacheratrealization>(`http://localhost:8080/api/teacheratrealizations/${id}`);
  }

  add(teacheratrealization: Teacheratrealization): Observable<Teacheratrealization> {
    return this.http.post<Teacheratrealization>("http://localhost:8080/api/teacheratrealizations/", teacheratrealization);
  }

  update(id,teacheratrealization): Observable<Teacheratrealization> {
    return this.http.put<Teacheratrealization>(`http://localhost:8080/api/teacheratrealizations/${id}`, teacheratrealization);
  }


}
    
}
