
import { Component, OnInit } from '@angular/core';
import {TitleService} from '../service/Title.service'
import {Title} from '../model/title'

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {
  title:Title = { dateWhenChosed:null, dateWhenStopped:null, majors:null, scientificField:null, titleType:null };
  constructor(private service:TitleService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.title = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

