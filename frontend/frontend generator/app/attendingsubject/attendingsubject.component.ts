
import { Component, OnInit } from '@angular/core';
import {AttendingsubjectService} from '../service/Attendingsubject.service'
import {Attendingsubject} from '../model/attendingsubject'

@Component({
  selector: 'app-attendingsubject',
  templateUrl: './attendingsubject.component.html',
  styleUrls: ['./attendingsubject.component.css']
})
export class AttendingsubjectComponent implements OnInit {
  attendingsubject:Attendingsubject = { finalGrade:null, classRealization:null, student:null };
  constructor(private service:AttendingsubjectService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.attendingsubject = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

