

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Subject } from '../model/Subject';
        import { SubjectService } from '../service/Subject.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-subjects',
            templateUrl: './subjects.component.html',
            styleUrls: ['./subjects.component.css']
        })
        export class SubjectsComponent implements OnInit {
            subject: Subject={"id":null,"username":null,"password":null,'roles':null};
            subjects:Subject[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: SubjectService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(subjects => this.subjects = subjects); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.subject).subscribe(() => this.getAll());
        }



}





