

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Major } from '../model/Major';
        import { MajorService } from '../service/Major.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-majors',
            templateUrl: './majors.component.html',
            styleUrls: ['./majors.component.css']
        })
        export class MajorsComponent implements OnInit {
            major: Major={"id":null,"username":null,"password":null,'roles':null};
            majors:Major[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: MajorService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(majors => this.majors = majors); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.major).subscribe(() => this.getAll());
        }



}





