

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Title } from '../model/Title';
        import { TitleService } from '../service/Title.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-titles',
            templateUrl: './titles.component.html',
            styleUrls: ['./titles.component.css']
        })
        export class TitlesComponent implements OnInit {
            title: Title={"id":null,"username":null,"password":null,'roles':null};
            titles:Title[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: TitleService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(titles => this.titles = titles); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.title).subscribe(() => this.getAll());
        }



}





