
import { Component, OnInit } from '@angular/core';
import {UniversityService} from '../service/University.service'
import {University} from '../model/university'

@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.css']
})
export class UniversityComponent implements OnInit {
  university:University = { name:null, dateOfEstablishment:null, address:null, faculties:null };
  constructor(private service:UniversityService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.university = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

