

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Studentyear } from '../model/Studentyear';
        import { StudentyearService } from '../service/Studentyear.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-studentyears',
            templateUrl: './studentyears.component.html',
            styleUrls: ['./studentyears.component.css']
        })
        export class StudentyearsComponent implements OnInit {
            studentyear: Studentyear={"id":null,"username":null,"password":null,'roles':null};
            studentyears:Studentyear[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: StudentyearService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(studentyears => this.studentyears = studentyears); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.studentyear).subscribe(() => this.getAll());
        }



}





