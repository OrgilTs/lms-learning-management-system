
import { Component, OnInit } from '@angular/core';
import {TeacherService} from '../service/Teacher.service'
import {Teacher} from '../model/teacher'

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
  teacher:Teacher = { jmbg:null, name:null, lastName:null, biography:null, faculty:null, major:null };
  constructor(private service:TeacherService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.teacher = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

