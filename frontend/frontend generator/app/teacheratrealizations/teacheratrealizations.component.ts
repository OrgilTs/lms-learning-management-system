

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Teacheratrealization } from '../model/Teacheratrealization';
        import { TeacheratrealizationService } from '../service/Teacheratrealization.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-teacheratrealizations',
            templateUrl: './teacheratrealizations.component.html',
            styleUrls: ['./teacheratrealizations.component.css']
        })
        export class TeacheratrealizationsComponent implements OnInit {
            teacheratrealization: Teacheratrealization={"id":null,"username":null,"password":null,'roles':null};
            teacheratrealizations:Teacheratrealization[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: TeacheratrealizationService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(teacheratrealizations => this.teacheratrealizations = teacheratrealizations); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.teacheratrealization).subscribe(() => this.getAll());
        }



}





