
import { Component, OnInit } from '@angular/core';
import {CountryService} from '../service/Country.service'
import {Country} from '../model/country'

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {
  country:Country = { name:null, places:null };
  constructor(private service:CountryService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.country = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

