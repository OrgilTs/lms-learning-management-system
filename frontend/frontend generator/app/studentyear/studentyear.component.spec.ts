
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentyearComponent } from './studentyear.component';

describe('StudentyearComponent', () => {
  let component: StudentyearComponent;
  let fixture: ComponentFixture<StudentyearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentyearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentyearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
