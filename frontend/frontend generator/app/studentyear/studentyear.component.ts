
import { Component, OnInit } from '@angular/core';
import {StudentyearService} from '../service/Studentyear.service'
import {Studentyear} from '../model/studentyear'

@Component({
  selector: 'app-studentyear',
  templateUrl: './studentyear.component.html',
  styleUrls: ['./studentyear.component.css']
})
export class StudentyearComponent implements OnInit {
  studentyear:Studentyear = { indexNumber:null, dateOfEnrollment:null, students:null };
  constructor(private service:StudentyearService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.studentyear = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

