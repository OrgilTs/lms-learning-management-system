

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Classrealization } from '../model/Classrealization';
        import { ClassrealizationService } from '../service/Classrealization.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-classrealizations',
            templateUrl: './classrealizations.component.html',
            styleUrls: ['./classrealizations.component.css']
        })
        export class ClassrealizationsComponent implements OnInit {
            classrealization: Classrealization={"id":null,"username":null,"password":null,'roles':null};
            classrealizations:Classrealization[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: ClassrealizationService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(classrealizations => this.classrealizations = classrealizations); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.classrealization).subscribe(() => this.getAll());
        }



}





