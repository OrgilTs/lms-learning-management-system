
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassrealizationComponent } from './classrealization.component';

describe('ClassrealizationComponent', () => {
  let component: ClassrealizationComponent;
  let fixture: ComponentFixture<ClassrealizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassrealizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassrealizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
