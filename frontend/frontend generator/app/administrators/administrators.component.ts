

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Administrator } from '../model/Administrator';
        import { AdministratorService } from '../service/Administrator.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-administrators',
            templateUrl: './administrators.component.html',
            styleUrls: ['./administrators.component.css']
        })
        export class AdministratorsComponent implements OnInit {
            administrator: Administrator={"id":null,"username":null,"password":null,'roles':null};
            administrators:Administrator[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: AdministratorService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(administrators => this.administrators = administrators); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.administrator).subscribe(() => this.getAll());
        }



}





