

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Classtype } from '../model/Classtype';
        import { ClasstypeService } from '../service/Classtype.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-classtypes',
            templateUrl: './classtypes.component.html',
            styleUrls: ['./classtypes.component.css']
        })
        export class ClasstypesComponent implements OnInit {
            classtype: Classtype={"id":null,"username":null,"password":null,'roles':null};
            classtypes:Classtype[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: ClasstypeService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(classtypes => this.classtypes = classtypes); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.classtype).subscribe(() => this.getAll());
        }



}





