
import { Component, OnInit } from '@angular/core';
import {AddressService} from '../service/Address.service'
import {Address} from '../model/address'

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
  address:Address = { street:null, number:null, place:null, universities:null, faculties:null };
  constructor(private service:AddressService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.address = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

