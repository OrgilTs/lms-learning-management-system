
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendingsubjectComponent } from './attendingsubject.component';

describe('AttendingsubjectComponent', () => {
  let component: AttendingsubjectComponent;
  let fixture: ComponentFixture<AttendingsubjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendingsubjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendingsubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
