

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Attendingsubject } from '../model/Attendingsubject';
        import { AttendingsubjectService } from '../service/Attendingsubject.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-attendingsubjects',
            templateUrl: './attendingsubjects.component.html',
            styleUrls: ['./attendingsubjects.component.css']
        })
        export class AttendingsubjectsComponent implements OnInit {
            attendingsubject: Attendingsubject={"id":null,"username":null,"password":null,'roles':null};
            attendingsubjects:Attendingsubject[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: AttendingsubjectService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(attendingsubjects => this.attendingsubjects = attendingsubjects); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.attendingsubject).subscribe(() => this.getAll());
        }



}





