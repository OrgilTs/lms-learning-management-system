

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { University } from '../model/University';
        import { UniversityService } from '../service/University.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-universitys',
            templateUrl: './universitys.component.html',
            styleUrls: ['./universitys.component.css']
        })
        export class UniversitysComponent implements OnInit {
            university: University={"id":null,"username":null,"password":null,'roles':null};
            universitys:University[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: UniversityService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(universitys => this.universitys = universitys); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.university).subscribe(() => this.getAll());
        }



}





