
import { Component, OnInit } from '@angular/core';
import {UserService} from '../service/User.service'
import {User} from '../model/user'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user:User = { password:null, email:null, active:null };
  constructor(private service:UserService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.user = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

