
import { Component, OnInit } from '@angular/core';
import {FacultyService} from '../service/Faculty.service'
import {Faculty} from '../model/faculty'

@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.css']
})
export class FacultyComponent implements OnInit {
  faculty:Faculty = { name:null, university:null, address:null, teachers:null, majors:null };
  constructor(private service:FacultyService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.faculty = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

