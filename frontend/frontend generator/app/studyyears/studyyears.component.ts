

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Studyyear } from '../model/Studyyear';
        import { StudyyearService } from '../service/Studyyear.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-studyyears',
            templateUrl: './studyyears.component.html',
            styleUrls: ['./studyyears.component.css']
        })
        export class StudyyearsComponent implements OnInit {
            studyyear: Studyyear={"id":null,"username":null,"password":null,'roles':null};
            studyyears:Studyyear[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: StudyyearService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(studyyears => this.studyyears = studyyears); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.studyyear).subscribe(() => this.getAll());
        }



}





