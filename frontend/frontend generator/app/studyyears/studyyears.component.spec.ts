
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyyearComponent } from './studyyear.component';

describe('StudyyearComponent', () => {
  let component: StudyyearComponent;
  let fixture: ComponentFixture<StudyyearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyyearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyyearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
