
import { Component, OnInit } from '@angular/core';
import {ClasstypeService} from '../service/Classtype.service'
import {Classtype} from '../model/classtype'

@Component({
  selector: 'app-classtype',
  templateUrl: './classtype.component.html',
  styleUrls: ['./classtype.component.css']
})
export class ClasstypeComponent implements OnInit {
  classtype:Classtype = { name:null, nastavniciNaRealizaciji:null };
  constructor(private service:ClasstypeService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.classtype = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

