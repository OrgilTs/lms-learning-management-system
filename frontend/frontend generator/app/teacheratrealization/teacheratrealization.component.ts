
import { Component, OnInit } from '@angular/core';
import {TeacheratrealizationService} from '../service/Teacheratrealization.service'
import {Teacheratrealization} from '../model/teacheratrealization'

@Component({
  selector: 'app-teacheratrealization',
  templateUrl: './teacheratrealization.component.html',
  styleUrls: ['./teacheratrealization.component.css']
})
export class TeacheratrealizationComponent implements OnInit {
  teacheratrealization:Teacheratrealization = { brojCasova:null, classRealizations:null, classType:null };
  constructor(private service:TeacheratrealizationService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.teacheratrealization = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

