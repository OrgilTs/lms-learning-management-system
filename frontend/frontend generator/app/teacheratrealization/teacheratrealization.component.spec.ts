
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacheratrealizationComponent } from './teacheratrealization.component';

describe('TeacheratrealizationComponent', () => {
  let component: TeacheratrealizationComponent;
  let fixture: ComponentFixture<TeacheratrealizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacheratrealizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacheratrealizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
