
import { Component, OnInit } from '@angular/core';
import {SubjectService} from '../service/Subject.service'
import {Subject} from '../model/subject'

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {
  subject:Subject = { name:null, espb:null, required:null, numberOfLectures:null, numberOfPractices:null, otherFormsOfClass:null, researchWork:null, remainingClasses:null, studyYear:null, classRealizations:null, results:null };
  constructor(private service:SubjectService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.subject = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

