




export interface Subject {
      
     id:number;
    
     name :string;
     espb :int;
     required :boolean;
     numberOfLectures :int;
     numberOfPractices :int;
     otherFormsOfClass :int;
     researchWork :int;
     remainingClasses :int;
     studyYear :studyyear;
     classRealizations :set<classrealization>;
     results :set<result>;
    
    
    

    
}
