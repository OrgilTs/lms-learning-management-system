

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Student } from '../model/Student';
        import { StudentService } from '../service/Student.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-students',
            templateUrl: './students.component.html',
            styleUrls: ['./students.component.css']
        })
        export class StudentsComponent implements OnInit {
            student: Student={"id":null,"username":null,"password":null,'roles':null};
            students:Student[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: StudentService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(students => this.students = students); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.student).subscribe(() => this.getAll());
        }



}





