
import { Component, OnInit } from '@angular/core';
import {ResultService} from '../service/Result.service'
import {Result} from '../model/result'

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  result:Result = { description:null, subject:null };
  constructor(private service:ResultService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.result = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

