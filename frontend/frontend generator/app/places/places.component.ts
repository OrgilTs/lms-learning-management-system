

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Place } from '../model/Place';
        import { PlaceService } from '../service/Place.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-places',
            templateUrl: './places.component.html',
            styleUrls: ['./places.component.css']
        })
        export class PlacesComponent implements OnInit {
            place: Place={"id":null,"username":null,"password":null,'roles':null};
            places:Place[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: PlaceService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(places => this.places = places); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.place).subscribe(() => this.getAll());
        }



}





