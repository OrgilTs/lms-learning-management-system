

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Country } from '../model/Country';
        import { CountryService } from '../service/Country.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-countrys',
            templateUrl: './countrys.component.html',
            styleUrls: ['./countrys.component.css']
        })
        export class CountrysComponent implements OnInit {
            country: Country={"id":null,"username":null,"password":null,'roles':null};
            countrys:Country[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: CountryService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(countrys => this.countrys = countrys); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.country).subscribe(() => this.getAll());
        }



}





