

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Faculty } from '../model/Faculty';
        import { FacultyService } from '../service/Faculty.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-facultys',
            templateUrl: './facultys.component.html',
            styleUrls: ['./facultys.component.css']
        })
        export class FacultysComponent implements OnInit {
            faculty: Faculty={"id":null,"username":null,"password":null,'roles':null};
            facultys:Faculty[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: FacultyService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(facultys => this.facultys = facultys); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.faculty).subscribe(() => this.getAll());
        }



}





