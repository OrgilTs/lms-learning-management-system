
import { Component, OnInit } from '@angular/core';
import {ClassrealizationService} from '../service/Classrealization.service'
import {Classrealization} from '../model/classrealization'

@Component({
  selector: 'app-classrealization',
  templateUrl: './classrealization.component.html',
  styleUrls: ['./classrealization.component.css']
})
export class ClassrealizationComponent implements OnInit {
  classrealization:Classrealization = { subject:null, teacherAtRealization:null, attendingSubjects:null };
  constructor(private service:ClassrealizationService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.classrealization = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

