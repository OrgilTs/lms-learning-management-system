

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Titletype } from '../model/Titletype';
        import { TitletypeService } from '../service/Titletype.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-titletypes',
            templateUrl: './titletypes.component.html',
            styleUrls: ['./titletypes.component.css']
        })
        export class TitletypesComponent implements OnInit {
            titletype: Titletype={"id":null,"username":null,"password":null,'roles':null};
            titletypes:Titletype[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: TitletypeService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(titletypes => this.titletypes = titletypes); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.titletype).subscribe(() => this.getAll());
        }



}





