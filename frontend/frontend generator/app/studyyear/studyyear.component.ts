
import { Component, OnInit } from '@angular/core';
import {StudyyearService} from '../service/Studyyear.service'
import {Studyyear} from '../model/studyyear'

@Component({
  selector: 'app-studyyear',
  templateUrl: './studyyear.component.html',
  styleUrls: ['./studyyear.component.css']
})
export class StudyyearComponent implements OnInit {
  studyyear:Studyyear = { year:null, major:null, subjects:null };
  constructor(private service:StudyyearService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.studyyear = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

