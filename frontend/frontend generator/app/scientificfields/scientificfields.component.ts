

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Scientificfield } from '../model/Scientificfield';
        import { ScientificfieldService } from '../service/Scientificfield.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-scientificfields',
            templateUrl: './scientificfields.component.html',
            styleUrls: ['./scientificfields.component.css']
        })
        export class ScientificfieldsComponent implements OnInit {
            scientificfield: Scientificfield={"id":null,"username":null,"password":null,'roles':null};
            scientificfields:Scientificfield[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: ScientificfieldService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(scientificfields => this.scientificfields = scientificfields); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.scientificfield).subscribe(() => this.getAll());
        }



}





