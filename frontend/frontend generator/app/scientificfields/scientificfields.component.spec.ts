
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScientificfieldComponent } from './scientificfield.component';

describe('ScientificfieldComponent', () => {
  let component: ScientificfieldComponent;
  let fixture: ComponentFixture<ScientificfieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScientificfieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScientificfieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
