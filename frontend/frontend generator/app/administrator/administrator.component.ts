
import { Component, OnInit } from '@angular/core';
import {AdministratorService} from '../service/Administrator.service'
import {Administrator} from '../model/administrator'

@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css']
})
export class AdministratorComponent implements OnInit {
  administrator:Administrator = {  };
  constructor(private service:AdministratorService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.administrator = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

