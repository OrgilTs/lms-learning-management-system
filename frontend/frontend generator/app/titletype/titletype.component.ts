
import { Component, OnInit } from '@angular/core';
import {TitletypeService} from '../service/Titletype.service'
import {Titletype} from '../model/titletype'

@Component({
  selector: 'app-titletype',
  templateUrl: './titletype.component.html',
  styleUrls: ['./titletype.component.css']
})
export class TitletypeComponent implements OnInit {
  titletype:Titletype = { indexNumber:null, titles:null };
  constructor(private service:TitletypeService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.titletype = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

