
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TitletypeComponent } from './titletype.component';

describe('TitletypeComponent', () => {
  let component: TitletypeComponent;
  let fixture: ComponentFixture<TitletypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TitletypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TitletypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
