
import { Component, OnInit } from '@angular/core';
import {PlaceService} from '../service/Place.service'
import {Place} from '../model/place'

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.css']
})
export class PlaceComponent implements OnInit {
  place:Place = { name:null, country:null, adresses:null };
  constructor(private service:PlaceService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.place = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

