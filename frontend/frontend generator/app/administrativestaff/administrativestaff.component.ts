
import { Component, OnInit } from '@angular/core';
import {AdministrativestaffService} from '../service/Administrativestaff.service'
import {Administrativestaff} from '../model/administrativestaff'

@Component({
  selector: 'app-administrativestaff',
  templateUrl: './administrativestaff.component.html',
  styleUrls: ['./administrativestaff.component.css']
})
export class AdministrativestaffComponent implements OnInit {
  administrativestaff:Administrativestaff = { jmbg:null, name:null, lastName:null };
  constructor(private service:AdministrativestaffService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.administrativestaff = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

