
import { Component, OnInit } from '@angular/core';
import {MajorService} from '../service/Major.service'
import {Major} from '../model/major'

@Component({
  selector: 'app-major',
  templateUrl: './major.component.html',
  styleUrls: ['./major.component.css']
})
export class MajorComponent implements OnInit {
  major:Major = { name:null, faculty:null, teachers:null, title:null, studyYears:null };
  constructor(private service:MajorService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      
      this.service.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(
      result => {
        this.major = result;
      },
      error => {
        console.log(error);
      }
    );

  }
}        

