

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Administrativestaff } from '../model/Administrativestaff';
        import { AdministrativestaffService } from '../service/Administrativestaff.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-administrativestaffs',
            templateUrl: './administrativestaffs.component.html',
            styleUrls: ['./administrativestaffs.component.css']
        })
        export class AdministrativestaffsComponent implements OnInit {
            administrativestaff: Administrativestaff={"id":null,"username":null,"password":null,'roles':null};
            administrativestaffs:Administrativestaff[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: AdministrativestaffService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(administrativestaffs => this.administrativestaffs = administrativestaffs); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.administrativestaff).subscribe(() => this.getAll());
        }



}





