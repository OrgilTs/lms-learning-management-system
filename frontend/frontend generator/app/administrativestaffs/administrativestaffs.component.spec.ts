
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrativestaffComponent } from './administrativestaff.component';

describe('AdministrativestaffComponent', () => {
  let component: AdministrativestaffComponent;
  let fixture: ComponentFixture<AdministrativestaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrativestaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrativestaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
