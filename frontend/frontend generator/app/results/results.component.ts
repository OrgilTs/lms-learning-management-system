

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Result } from '../model/Result';
        import { ResultService } from '../service/Result.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-results',
            templateUrl: './results.component.html',
            styleUrls: ['./results.component.css']
        })
        export class ResultsComponent implements OnInit {
            result: Result={"id":null,"username":null,"password":null,'roles':null};
            results:Result[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: ResultService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(results => this.results = results); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.result).subscribe(() => this.getAll());
        }



}





