

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { User } from '../model/User';
        import { UserService } from '../service/User.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-users',
            templateUrl: './users.component.html',
            styleUrls: ['./users.component.css']
        })
        export class UsersComponent implements OnInit {
            user: User={"id":null,"username":null,"password":null,'roles':null};
            users:User[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: UserService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(users => this.users = users); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.user).subscribe(() => this.getAll());
        }



}





