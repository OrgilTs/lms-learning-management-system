

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Teacher } from '../model/Teacher';
        import { TeacherService } from '../service/Teacher.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-teachers',
            templateUrl: './teachers.component.html',
            styleUrls: ['./teachers.component.css']
        })
        export class TeachersComponent implements OnInit {
            teacher: Teacher={"id":null,"username":null,"password":null,'roles':null};
            teachers:Teacher[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: TeacherService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(teachers => this.teachers = teachers); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.teacher).subscribe(() => this.getAll());
        }



}





