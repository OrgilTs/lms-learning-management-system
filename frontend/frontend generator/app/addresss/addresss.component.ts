

        import { Component, OnInit } from '@angular/core';
        import { HttpClient } from '@angular/common/http';
        import { LoginService } from '../service/login.service';
        import { Address } from '../model/Address';
        import { AddressService } from '../service/Address.service';
        import { Router } from '@angular/router';

        @Component({
            selector: 'app-addresss',
            templateUrl: './addresss.component.html',
            styleUrls: ['./addresss.component.css']
        })
        export class AddresssComponent implements OnInit {
            address: Address={"id":null,"username":null,"password":null,'roles':null};
            addresss:Address[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: AddressService,private router: Router) { }

        ngOnInit(): void {
            this.getAll();
        }

        getAll() { this.us.getAll().subscribe(addresss => this.addresss = addresss); }

        getOne(idUsera) {
            this.router.navigate(["/", idUsera])
        }

        delete(idUsera) {
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }
  
        addOne(){
            this.us.add(this.address).subscribe(() => this.getAll());
        }



}





