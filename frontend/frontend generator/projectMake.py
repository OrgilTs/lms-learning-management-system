import os, shutil, json
import platform
from modelGenerator import ModelGenerator
from serviceSpecGenerator import ServiceSpecGenerator
from serviceGenerator import ServiceGenerator
from appGenerator import AppGenerator
from appSpecGenerator import AppSpecGenerator
from componentGenerator import ComponentGenerator
from componentGenerator_s import ComponentGenerator_S
from cssGenerator import CssGenerator
from componentSpecGenerator import ComponentSpecGenerator

def saveFile(path, template, app_name, key):
    f = open(path, "w")
    f.write(template)
    f.close()
    print("[{0}]  {1} generated successfully...".format(app_name, key.title()))

def genProject():
    data = []
    try:
        with open("./project.json") as f:
            data = json.load(f)
    except Exception as e:
        print("[ERROR]: "+e)


    if os.path.exists('./app') is False:
        os.mkdir("./app")
    
    if os.path.exists('./app/model') is False:
        os.mkdir("./app/model")
    
    if os.path.exists('./app/service') is False:
        os.mkdir("./app/service")

    
    for project in data:
        for c in project["classes"]:
            model = ModelGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/model/{1}.ts".format(project["package"], c["name"].title()), model.get_template(), project["projectName"], c["name"].title())

            service = ServiceSpecGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/service/{1}.service.spec.ts".format(project["package"], c["name"].title().lower()), service.get_template(), project["projectName"], c["name"].title().lower())

            service = ServiceGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/service/{1}.service.ts".format(project["package"], c["name"].title().lower()), service.get_template(), project["projectName"], c["name"].title().lower())

            app = AppGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/app.component.ts".format(project["package"], c["name"].title().lower()), app.get_template(), project["projectName"], c["name"].title().lower())

            app = AppSpecGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/app.component.spec.ts".format(project["package"], c["name"].title().lower()), app.get_template(), project["projectName"], c["name"].title().lower())

            name = c["name"].lower()
            os.mkdir("./app/"+name)
            os.mkdir("./app/"+name+"s") 

            css = CssGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/{0}/{0}.css".format(c["name"].lower()), css.get_template(), project["projectName"], c["name"].title().lower())
            
            css2 = CssGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/"+name+"s/"+name+"s.css".format(project["package"], c["name"].title().lower()), css2.get_template(), project["projectName"], c["name"].title().lower())

            componentTs = ComponentGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/"+name+"/"+name+".component.ts".format(project["package"], c["name"].title().lower()), componentTs.get_template(), project["projectName"], c["name"].title().lower())
               
            componentTs2 = ComponentGenerator_S(project["package"], c["name"], c["attributes"])
            saveFile("./app/"+name+"s/"+name+"s.component.ts".format(project["package"], c["name"].title().lower()), componentTs2.get_template(), project["projectName"], c["name"].title().lower())

            componentSpec = ComponentSpecGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/"+name+"/"+name+".component.spec.ts".format(project["package"], c["name"].title().lower()), componentSpec.get_template(), project["projectName"], c["name"].title().lower())
               
            componentSpec2 = ComponentSpecGenerator(project["package"], c["name"], c["attributes"])
            saveFile("./app/"+name+"s/"+name+"s.component.spec.ts".format(project["package"], c["name"].title().lower()), componentSpec2.get_template(), project["projectName"], c["name"].title().lower())
 