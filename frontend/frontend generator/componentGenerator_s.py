import subprocess
import os
class ComponentGenerator_S:
    def __init__(self, app, class_name, class_attributes):
        self.app = app
        self.class_name = class_name
        self.attributes = class_attributes
        self.template = ""
        self.generate()
        
    def get_template(self):
        return self.template
    
    def generate(self):
        attributes_tpl = ""
        constructor_attributes_tpl = ""
        constructor_body_tpl = ""
        gs_tpl = ""
        for a in self.attributes:
            name = a["name"]+"service"+"spec"
            
            attributes_tpl += "\n     {1} :{0};".format(name, a["name"], a["nullable"])

            # add attributes for constructor
            if len(constructor_attributes_tpl) < 1:
                constructor_attributes_tpl += "{}".format(a["name"])
            else:
                constructor_attributes_tpl += ", {}".format(a["name"])

            if len(constructor_attributes_tpl) < 1:
                constructor_attributes_tpl += "{}".format(a["name"])
            else:
                constructor_attributes_tpl += ", {}".format(a["name"])
            if len(constructor_body_tpl) < 1:
                constructor_body_tpl += "long id"
            else:
                constructor_body_tpl += ", long id"
  

        constructor_tpl = """
    public {0}() {{\n        super();\n    }}\n\n
    public {0}({1}) {{
        this.id=id;
    {2}
    }}\n
""".format(self.class_name.title(), constructor_attributes_tpl, constructor_body_tpl)
        class_header_tpl = ""
        
        class_header_tpl += "{}".format(self.class_name.title())
        self.template = """

        import {{ Component, OnInit }} from '@angular/core';
        import {{ HttpClient }} from '@angular/common/http';
        import {{ LoginService }} from '../service/login.service';
        import {{ {0} }} from '../model/{0}';
        import {{ {8} }} from '../service/{0}.service';
        import {{ Router }} from '@angular/router';

        @Component({{
            selector: 'app-{7}s',
            templateUrl: './{7}s.component.html',
            styleUrls: ['./{7}s.component.css']
        }})
        export class {0}sComponent implements OnInit {{
            {7}: {0}={{"id":null,"username":null,"password":null,'roles':null}};
            {6}:{0}[];
  
        constructor(private http: HttpClient, public loginService: LoginService,private us: {8},private router: Router) {{ }}

        ngOnInit(): void {{
            this.getAll();
        }}

        getAll() {{ this.us.getAll().subscribe({6} => this.{6} = {6}); }}

        getOne(idUsera) {{
            this.router.navigate(["/", idUsera])
        }}

        delete(idUsera) {{
            this.us.delete(idUsera).subscribe(() => this.getAll());
        }}
  
        addOne(){{
            this.us.add(this.{7}).subscribe(() => this.getAll());
        }}



}}





""".format(class_header_tpl, attributes_tpl, constructor_tpl, gs_tpl, self.app.lower(), "",class_header_tpl.lower()+"s", class_header_tpl.lower(),class_header_tpl+"Service")
