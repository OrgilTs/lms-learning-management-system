import mysql.connector, json

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="root",
  database="lms"
)
mycursor = mydb.cursor(dictionary=True)

data = None
ogranicenje = 15
brojac = 0
print("Molimo sacekajte, ovo moze da potraje od 1 min do 100 dana...")
# with open('gradovi.json', "r", encoding="utf-8") as json_file:
#     data = json.load(json_file)
# for country in data:
#     mycursor.execute("INSERT INTO country (name) VALUES (%s)", (country["country"],))
#     mydb.commit()
#     mycursor.execute("SELECT * FROM country order by id desc limit 1")
#     last_id = mycursor.fetchone()
#     for city in country["cities"]:
#         brojac += 1
#         mycursor.execute("INSERT INTO place (name, country_id) VALUES (%s, %s)", (city, last_id["id"]))
#         mydb.commit()
#         if brojac == ogranicenje:
#             break
#     brojac = 0



# # lozinka korisnika je 123
# imena = ["Marko", "Milos", "Stefan", "Jelena", "Branislav", "Nikola", "Jovana", "Ana", "Uros", "Milica"]
# prezimena = ["Banjac", "Licina", "Ulemek", "Jovanovic", "Milanovic", "Ivanovic", "Jokic"]
# jmbg = 1000000000000
# for ime in imena:
#     jmbg += 1
#     for prezime in prezimena:
#         user = {
#             "name": ime,
#             "lastname": prezime,
#             "dtype": "Student",
#             "active": True,
#             "email": "{}.{}@lms.com".format(ime.lower(), prezime.lower()),
#             "password": "{bcrypt}$2a$10$wSfqZa94k.RvutGtu2y/c.LiCkhl2LfS/9ZMon8B6504kIN.cpN3u",
#             "jmbg": jmbg,
#             "birthdate": "1998-01-01",
#             "residence": 3
#         }
#         mycursor.execute("INSERT INTO user (dtype, active, email, password, jmbg, lastname, name, birthdate, address_id) VALUES (%(dtype)s,%(active)s, %(email)s, %(password)s, %(jmbg)s, %(lastname)s, %(name)s, %(birthdate)s, %(residence)s)", user)
#         mydb.commit()