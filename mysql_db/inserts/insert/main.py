import json
countries = []

def add(data):
    for country in countries:
        if country["country"] == data["country"]:
            country["cities"].append(data["city"])
            return None
    new_country = {
        "country" : data["country"],
        "cities" : [data["city"]]
    }
    countries.append(new_country)

with open("data.txt", "r", encoding="utf-8") as file:
    lines = file.readlines()
    for i in range(1, len(lines)):
        spl_lines = lines[i].split(",")
        data = {
            "country": spl_lines[4].replace('"', ""),
            "city":spl_lines[0].replace('"', "")
        }
        add(data)
    file.close()    

with open("result.json", "w", encoding="utf-8") as outfile:
    json.dump(countries, outfile, ensure_ascii=False)
    outfile.close()




