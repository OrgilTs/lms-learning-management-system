/*
-- Query: SELECT * FROM lms.subject
LIMIT 0, 1000

-- Date: 2020-07-15 01:25
*/
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (1,20,'matematika',20,20,1,20,'1',1,NULL,1);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (2,8,'engleski',20,20,1,20,'1',1,NULL,1);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (3,8,'Osnove Programiranja',20,20,1,20,'1',1,NULL,2);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (4,8,'Java',20,20,1,1,'1',1,NULL,1);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (5,8,'kva',20,20,1,20,'1',1,NULL,3);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (6,8,'isa',20,20,1,20,'1',1,NULL,1);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (7,8,'Statistika',20,20,1,20,'1',1,NULL,1);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (8,8,'Ekonomija',20,20,1,20,'1',1,NULL,5);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (9,8,'Menadzment',20,20,1,20,'1',1,NULL,6);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (10,8,'Ekonomija 4',20,20,1,20,'1',1,NULL,8);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (11,8,'Ekonomija 3',20,20,1,20,'1',1,NULL,7);
INSERT INTO `` (`id`,`espb`,`name`,`numberoflectures`,`numberofpractices`,`otherformsofclass`,`remainingclasses`,`required`,`researchwork`,`precondition_id`,`studyyear_id`) VALUES (12,8,'Sii 4 predmet',20,20,1,20,'1',1,NULL,4);
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('4', '9:00', '7:00', '2', '4');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('5', '7:00', '5:00', '3', '5');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('6', '3:00', '2:00', '4', '6');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('7', '16:00', '15:00', '5', '7');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('8', '19:00', '17:00', '1', '8');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('9', '20:00', '19:00', '2', '9');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('10', '16:00', '14:00', '3', '10');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('11', '16:00', '15:00', '4', '11');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('12', '18:00', '16:00', '5', '12');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('13', '18:00', '17:00', '1', '1');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('14', '20:00', '18:00', '2', '2');
INSERT INTO `lms`.`schedule` (`id`, `end`, `start`, `daytitle_id`, `subject_id`) VALUES ('15', '13:00', '11:00', '3', '3');
